module Presenter.Internal.Context
 ( Context
 , WithContext
 , runWithContext
 , setFilePath
 , getFilePath
 , setWindowSize
 , getWindowSize
 , getWindowRect
 ) where

import Control.Monad.State.Strict
import Data.Maybe
import Pipes.Core
import Pipes.Lift

import qualified Display as D
import Presenter.Input
import Presenter.Output
import UI


data Context
  = Context
    { filePathOf :: !FilePath
    , windowSizeOf :: !Dimensions
    }

type WithContext m = StateT Context m


runWithContext
  :: (Monad m)
  => Proxy a' a b' b (WithContext m) () -> Proxy a' a b' b m ()
runWithContext = evalStateP $
  Context
  { filePathOf = ""
  , windowSizeOf = Dimensions 0 0
  }

setFilePath :: (Monad m) => FilePath -> WithContext m ()
setFilePath path = do
  context <- get
  put (context { filePathOf = path })

getFilePath :: (Monad m) => WithContext m FilePath
getFilePath = gets filePathOf

setWindowSize :: (Monad m) => Dimensions -> WithContext m ()
setWindowSize size = do
  context <- get
  put (context { windowSizeOf = size })

getWindowSize :: (Monad m) => WithContext m Dimensions
getWindowSize = gets windowSizeOf

getWindowRect :: (Monad m) => WithContext m Rectangle
getWindowRect = Rectangle origin <$> getWindowSize
