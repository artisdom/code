{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MultiWayIf #-}

module Presenter.Internal.ViewPanel
 ( viewPanel
 ) where

import Control.Arrow
import Control.Monad.Fix
import Control.Wire hiding ((.),id)
import Data.Functor
import Data.Maybe

import Audio hiding (sampleRateOf)
import Display
import qualified Model
import Presenter.Internal.Cursors
import Presenter.Internal.TimeAxis
import Presenter.Internal.UITypes
import Presenter.Internal.ViewModeling as VM
import Presenter.Internal.Waveform
import UI


-- IMPORTANT NOTE:
-- The term "cursor" in this module refers to a reference within the audio,
-- as implemented in Audio.Cursor, not to the mouse pointer.

makeMarkingCursorData :: CursorDataMaker
makeMarkingCursorData status =
  if viewStart <= markingCursorPosition
     && markingCursorPosition < viewStart + viewSpan
  then Just CursorData
    { cursorTypeOf = MarkingCursor
    , cursorViewSpanOf = viewSpan
    , cursorViewStartOf = viewStart
    , cursorPositionOf = markingCursorPosition
    }
  else Nothing
  where
    viewStart = Model.viewCursorPositionOf status
    viewSpan = Model.viewSpanOf status
    markingCursorPosition = Model.markingCursorPositionOf status

makePlaybackCursorData :: CursorDataMaker
makePlaybackCursorData status =
  Model.playbackCursorPositionOf status <&> \p ->
    CursorData
    { cursorPositionOf = p
    , cursorTypeOf =
        PlaybackCursor
          if Model.isPlayingOf status
          then PlaybackCursorActive
          else PlaybackCursorIdle
    , cursorViewSpanOf = Model.viewSpanOf status
    , cursorViewStartOf = Model.viewCursorPositionOf status
    }

makeTimeAxisData :: TimeAxisDataMaker
makeTimeAxisData status =
  TimeAxisData
  { beginningSampleOf = start
  , endingSampleOf = start + numSamplesIn (Model.segmentsInViewOf status)
  , numSamplesInViewOf = Model.viewSpanOf status
  , sampleRateOf = Model.sampleRateOf status
  }
  where start = Model.viewCursorPositionOf status

makeWaveformData :: Interactivity -> WaveformDataMaker
makeWaveformData interactivity status =
  WaveformData
  { interactivityOf = interactivity
  , segmentsOf = Model.segmentsInViewOf status
  , viewSpanOf = Model.viewSpanOf status
  , viewStartOf = Model.viewCursorPositionOf status
  }


--------------------------------

{-# ANN module "HLint: ignore Redundant bracket" #-}
viewPanel :: (Monad m, MonadFix m) => EditScreenPanel m
viewPanel =
  -- Lisp mode!
  (integrateFeedback
    (zoomControl)
    (splitPanel (Fixed SizedBottom timeAxisHeight)
      (layeredPanel
        [ normalIdle
        , cursor makeMarkingCursorData
        , cursor makePlaybackCursorData ])
      (timeAxis makeTimeAxisData)))

zoomControl :: (Monad m) => EditScreenControl m
zoomControl = arr (\context ->
  case (mousePositionIn context, userInputIn context) of
    (Just c, Mouse (Scroll (Deltas sx sy))) ->
      let numSamplesInView = Model.viewSpanOf (feedforwardIn context)
      in mempty
      { modelUpdateOf =
          Model.moveViewCursorBy (samplesToScroll numSamplesInView sx) .
          Model.zoomView (c `ratioX` boundsIn context)
            if | sy  < 0 -> 10/9
               | sy == 0 -> 1.0
               | sy  > 0 -> 9/10
      }
    _ -> mempty
  )

viewSampleMovementFor = flip toSampleMovement Model.viewSpanOf

segmentUnderMouse context = do
  let segments = Model.segmentsInViewOf (feedforwardIn context)
  index <- positionXOf <$> viewSampleMovementFor context
  cursor <- advanceCursor index (cursorAtFirstSample segments) segments
  return (index, segmentAtCursor cursor segments)


--------------------------------
-- normal mode
--------------------------------

normalIdle :: Monad m => EditScreenPanel m
normalIdle = mealyState normalIdleOutput normalIdleTransition

normalIdleOutput context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = normalIdleGenerator context
  }

normalIdleGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData
    (maybe NoInteractivity (Hovering . positionXOf) (viewSampleMovementFor context))

normalIdleTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentIdle)
  `mergeL` (keyEvent MetaAlt Down context     $> precisionIdle)
  `mergeL` (buttonInBoundsEvent LeftButton Down context  $> normalPrimed)
  `mergeL` (buttonInBoundsEvent RightButton Down context $> normalOperationPrimed)

--

normalPrimed :: Monad m => EditScreenPanel m
normalPrimed = mealyState normalPrimedOutput normalPrimedTransition

normalPrimedOutput context =
  UIOutput
  { feedbackOf = normalPrimedFeedback context
  , generatorOf = normalPrimedGenerator context
  }

normalPrimedFeedback context =
  case segmentUnderMouse context of
    Nothing -> mempty
    Just (index, segment) ->
      if | inputIsButtonActionInBounds LeftButton Up context ->
             if isClip segment
             then mempty
               { audioCommandOf = Just Model.StartPlayback
               , modelUpdateOf = Model.setPlaybackCursorToMarkingCursor
                               . Model.setMarkingCursorToClipInViewAt index
               }
             else mempty
               { modelUpdateOf = Model.setMarkingCursorInViewTo index
               }
         | inputIsCombinedCtrlKey Down context ->
             mempty { modelUpdateOf = Model.startGapExpansion index }
         | otherwise -> mempty

normalPrimedGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData
    (maybe NoInteractivity Activating (positionXOf <$> viewSampleMovementFor context))

normalPrimedTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentDragging)
  `mergeL` (combinedCtrlKeyEvent Up context   $> normalPrimed)
  `mergeL` (keyEvent MetaAlt Down context     $> precisionPlayback)
  `mergeL` (keyEvent MetaAlt Up context       $> normalPrimed)
  `mergeL` (movementInBoundsEvent context     $> normalDragging)
  `mergeL` (inputInterruptionEvent context    $> normalIdle)

--

normalDragging :: Monad m => EditScreenPanel m
normalDragging = mealyState normalDraggingOutput normalDraggingTransition

normalDraggingOutput context =
  UIOutput
  { feedbackOf = normalDraggingFeedback context
  , generatorOf = normalDraggingGenerator context
  }

normalDraggingFeedback context =
  if inputIsCombinedCtrlKey Down context
  then mempty { modelUpdateOf = maybe id (Model.startGapExpansion . positionXOf) sm }
  else mempty { modelUpdateOf = maybe id (Model.moveViewCursorBy . (* (-1)) . VM.deltaXOf) sm }
  where
    sm = viewSampleMovementFor context

normalDraggingGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData NoInteractivity

normalDraggingTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentDragging)
  `mergeL` (combinedCtrlKeyEvent Up context   $> normalDragging)
  `mergeL` (keyEvent MetaAlt Down context     $> precisionPlayback)
  `mergeL` (keyEvent MetaAlt Up context       $> normalDragging)
  `mergeL` (inputInterruptionEvent context    $> normalIdle)

--

normalOperationPrimed :: Monad m => EditScreenPanel m
normalOperationPrimed =
  mealyState normalOperationPrimedOutput normalOperationPrimedTransition

normalOperationPrimedOutput context =
  UIOutput
  { feedbackOf = normalOperationPrimedFeedback context
  , generatorOf = normalOperationPrimedGenerator context
  }

normalOperationPrimedFeedback context =
  if inputIsButtonActionInBounds RightButton Up context
  then mempty { modelUpdateOf = Model.removeInvolving index }
  else mempty
  where
    index = positionXOf (fromJust (viewSampleMovementFor context))

normalOperationPrimedGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData
    (determineRemovalSelection context)

normalOperationPrimedTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentIdle)
  `mergeL` (combinedCtrlKeyEvent Up context   $> normalOperationPrimed)
  `mergeL` (keyEvent MetaAlt Down context     $> precisionOperation)
  `mergeL` (keyEvent MetaAlt Up context       $> normalOperationPrimed)
  `mergeL` (movementInBoundsEvent context     $> normalOperationDragging index)
  `mergeL` (inputInterruptionEvent context    $> normalIdle)
  where
    index = positionXOf (fromJust (viewSampleMovementFor context))

--

normalOperationDragging :: Monad m => SampleOffset -> EditScreenPanel m
normalOperationDragging samplesFromViewStart1 =
  mealyState
    (normalOperationDraggingOutput samplesFromViewStart1)
    (normalOperationDraggingTransition samplesFromViewStart1)

normalOperationDraggingOutput samplesFromViewStart1 context =
  UIOutput
  { feedbackOf = normalOperationDraggingFeedback samplesFromViewStart1 context
  , generatorOf = normalOperationDraggingGenerator samplesFromViewStart1 context
  }

normalOperationDraggingFeedback samplesFromViewStart1 context =
  if inputIsButtonActionInBounds RightButton Up context
  then mempty { modelUpdateOf = Model.removeInvolvingSpan samplesFromViewStart1 index }
  else mempty
  where
    index = positionXOf (fromJust (viewSampleMovementFor context))

normalOperationDraggingGenerator samplesFromViewStart1 context =
  Component (boundsIn context) . Waveform . makeWaveformData
    (getRemovalSelection samplesFromViewStart1 context)

normalOperationDraggingTransition samplesFromViewStart1 context =
           (combinedCtrlKeyEvent Down context $> adjustmentIdle)
  `mergeL` (combinedCtrlKeyEvent Up context   $> normalOperationDragging samplesFromViewStart1)
  `mergeL` (keyEvent MetaAlt Down context     $> precisionOperation)
  `mergeL` (keyEvent MetaAlt Up context       $> normalOperationDragging samplesFromViewStart1)
  `mergeL` (inputInterruptionEvent context    $> normalIdle)

--------

determineRemovalSelection context =
  case viewSampleMovementFor context of
    Nothing -> NoInteractivity
    Just sm ->
      let status = feedforwardIn context
          segments = Model.segmentsInViewOf status
          mark = Model.markingCursorPositionOf status
          view = Model.viewCursorPositionOf status
          x = positionXOf sm
          cursor = advanceCursorInBounds x (cursorAtFirstSample segments) segments
      in if isClip (segmentAtCursor cursor segments)
      then getRemovalSelection x context
      else getRemovalSelection (mark - view) context

getRemovalSelection samplesFromViewStart1 context =
  case viewSampleMovementFor context of
    Nothing -> NoInteractivity
    Just sm ->
      let segments = Model.segmentsInViewOf (feedforwardIn context)
          beginningCursor = cursorAtFirstSample segments
          cursor1 =
            if samplesFromViewStart1 < 0
            then beginningCursor
            else advanceCursorInBounds samplesFromViewStart1 beginningCursor segments
          cursor2 =
            advanceCursorInBounds (positionXOf sm) beginningCursor segments
      in case arrageForRemovalHighlight cursor1 cursor2 segments of
          Nothing -> NoInteractivity
          Just (c1, c2) ->
            RemovingBetween
              (numSamplesBetween beginningCursor c1 segments)
              (numSamplesBetween beginningCursor c2 segments)

arrageForRemovalHighlight cursor1 cursor2 segments =
  if pointToSameSegment cursor1 cursor2 && isGap (segmentAtCursor cursor1 segments)
  then Nothing
  else
    let (cursor1', cursor2') = order cursor1 cursor2
    in Just
      ( if isGap (segmentAtCursor cursor1' segments)
        then fromMaybe (cursorAtLastSample segments)
          (moveToBeginningOfNextSegment cursor1' segments)
        else cursor1'
      , if isClip (segmentAtCursor cursor2' segments)
        then
          -- special case where trailing gap is 0-length;
          -- it was highlighting the following clip erroneously
          -- (because of the use of `numSamplesBetween` in the
          -- function above)
          let c = fromMaybe cursor2' $ moveToBeginningOfNextSegment cursor2' segments
          in if lengthOf (segmentAtCursor c segments) == 0 then cursor2' else c
        else cursor2'
      )


--------------------------------
-- precision mode
--------------------------------

precisionIdle :: Monad m => EditScreenPanel m
precisionIdle = mealyState precisionIdleOutput precisionIdleTransition

precisionIdleOutput = precisionIdleWaveformOutput <> precisionIdleCursorOutput

precisionIdleCursorOutput context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = precisionIdleCursorGenerator context
  }

precisionIdleCursorGenerator = makePrecisionCursor PrecisionCursorIdle

precisionIdleWaveformOutput context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = precisionIdleWaveformGenerator context
  }

precisionIdleWaveformGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData NoInteractivity

precisionIdleTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentIdle)
  `mergeL` (keyEvent MetaAlt Up context       $> condIdle)
  `mergeL` (buttonInBoundsEvent LeftButton Down context  $> precisionPlayback)
  `mergeL` (buttonInBoundsEvent RightButton Down context $> precisionOperation)
  where
    condIdle =
      if combinedCtrlActiveIn context
      then adjustmentIdle
      else normalIdle

--

precisionPlayback :: Monad m => EditScreenPanel m
precisionPlayback = mealyState precisionPlaybackOutput precisionPlaybackTransition

precisionPlaybackOutput = precisionPlaybackWaveformOutput <> precisionPlaybackCursorOutput

precisionPlaybackCursorOutput context =
  UIOutput
  { feedbackOf = precisionPlaybackCursorFeedback context
  , generatorOf = precisionPlaybackCursorGenerator context
  }

precisionPlaybackCursorFeedback context
  | inputIsCombinedCtrlKey Down context
    || (inputIsKey MetaAlt Up context && combinedCtrlActiveIn context) =
      let index = positionXOf (fromJust (viewSampleMovementFor context))
      in mempty { modelUpdateOf = Model.startGapExpansion index }
  | inputIsButtonActionInBounds LeftButton Up context =
      case segmentUnderMouse context of -- this test accounts for the deadspace
        Just (index, _) ->
          mempty
          { audioCommandOf = Just Model.StartPlayback
          , modelUpdateOf = Model.setPlaybackCursorToMarkingCursor
                            . Model.setMarkingCursorInViewTo index
          }
        Nothing -> mempty
  | otherwise = mempty

precisionPlaybackCursorGenerator = makePrecisionCursor PrecisionCursorActivating

precisionPlaybackWaveformOutput context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = precisionPlaybackWaveformGenerator context
  }

precisionPlaybackWaveformGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData NoInteractivity

precisionPlaybackTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentDragging)
  `mergeL` (combinedCtrlKeyEvent Up context   $> precisionPlayback)
  `mergeL` (keyEvent MetaAlt Up context       $> condDragging)
  `mergeL` (inputInterruptionEvent context    $> precisionIdle)
  where
    condDragging =
      if combinedCtrlActiveIn context
      then adjustmentDragging
      else normalDragging

--

precisionOperation :: Monad m => EditScreenPanel m
precisionOperation = mealyState precisionOperationOutput precisionOperationTransition

precisionOperationOutput = precisionOperationWaveformOutput <> precisionOperationCursorOutput

precisionOperationCursorOutput context =
  UIOutput
  { feedbackOf = precisionOperationCursorFeedback context
  , generatorOf = precisionOperationCursorGenerator context
  }

precisionOperationCursorFeedback context =
  if inputIsButtonActionInBounds RightButton Up context
  then
    mempty
    { modelUpdateOf =
        case segmentUnderMouse context of
          Just (index, segment) ->
            if isClip segment then Model.splitClip index else Model.truncateGap index
          Nothing -> id
    }
  else mempty

precisionOperationCursorGenerator = makePrecisionCursor PrecisionCursorModifying

precisionOperationWaveformOutput context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = precisionOperationWaveformGenerator context
  }

precisionOperationWaveformGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData
    case segmentUnderMouse context of
      Just (index, segment) ->
        if isClip segment
        then NoInteractivity -- TODO
        else Truncating index
      Nothing -> NoInteractivity

precisionOperationTransition context =
           (combinedCtrlKeyEvent Down context $> adjustmentIdle)
  `mergeL` (combinedCtrlKeyEvent Up context   $> precisionOperation)
  `mergeL` (keyEvent MetaAlt Up context       $> condIdle)
  `mergeL` (inputInterruptionEvent context    $> precisionIdle)
  where
    condIdle =
      if combinedCtrlActiveIn context
      then adjustmentIdle
      else normalIdle

--------

makePrecisionCursor cursorType context =
  case viewSampleMovementFor context of
    Just index ->
      Component (boundsIn context) . Cursor . \status ->
        let viewStart = Model.viewCursorPositionOf status
        in  CursorData
            { cursorTypeOf = PrecisionCursor cursorType
            , cursorViewSpanOf = Model.viewSpanOf status
            , cursorViewStartOf = viewStart
            , cursorPositionOf = positionXOf index + viewStart
            }
    Nothing -> const Invisible


--------------------------------
-- adjustment mode
--------------------------------

adjustmentIdle :: Monad m => EditScreenPanel m
adjustmentIdle = mealyState adjustmentIdleOutput adjustmentIdleTransition

adjustmentIdleOutput context =
  UIOutput
  { feedbackOf = adjustmentIdleFeedback context
  , generatorOf = adjustmentIdleGenerator context
  }

adjustmentIdleFeedback context =
  if inputIsButtonActionInBounds LeftButton Down context
  then mempty { modelUpdateOf = maybe id (Model.startGapExpansion . positionXOf) sm }
  else mempty
  where
    sm = viewSampleMovementFor context

adjustmentIdleGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData NoInteractivity

adjustmentIdleTransition context =
           (combinedCtrlKeyEvent Up context $> condIdle)
  `mergeL` (keyEvent MetaAlt Down context   $> precisionIdle)
  `mergeL` (keyEvent MetaAlt Up context     $> adjustmentIdle)
  `mergeL` (buttonInBoundsEvent LeftButton Down context $> adjustmentDragging)
  where
    condIdle = if altActiveIn context then precisionIdle else normalIdle

--

adjustmentDragging :: Monad m => EditScreenPanel m
adjustmentDragging = mealyState adjustmentDraggingOutput adjustmentDraggingTransition

adjustmentDraggingOutput context =
  UIOutput
  { feedbackOf = adjustmentDraggingFeedback context
  , generatorOf = adjustmentDraggingGenerator context
  }

adjustmentDraggingFeedback context =
  if | inputIsButtonActionInBounds LeftButton Up context
       || inputIsCombinedCtrlKey Up context
       || inputIsKey MetaAlt Down context ->
         mempty { modelUpdateOf = Model.completeGapExpansion }
     | inputIsNonInterrupting context
       || inputIsKey MetaAlt Up context ->
         mempty { modelUpdateOf = Model.continueGapExpansion (maybe 0 VM.deltaXOf sm) }
     | otherwise ->
         mempty { modelUpdateOf = Model.abortGapExpansion }
  where
    sm = viewSampleMovementFor context

adjustmentDraggingGenerator context =
  Component (boundsIn context) . Waveform . makeWaveformData NoInteractivity

adjustmentDraggingTransition context =
           (combinedCtrlKeyEvent Up context $> cond)
  `mergeL` (keyEvent MetaAlt Down context   $> precisionPlayback)
  `mergeL` (keyEvent MetaAlt Up context     $> adjustmentDragging)
  `mergeL` (inputInterruptionEvent context  $> adjustmentIdle)
  where
    cond = if altActiveIn context then precisionPlayback else normalDragging
