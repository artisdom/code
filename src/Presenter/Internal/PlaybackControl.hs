{-# LANGUAGE MultiWayIf #-}

module Presenter.Internal.PlaybackControl
 ( playbackControl
 ) where

import Control.Arrow
import Control.Wire (mergeL)
import Data.Functor

import qualified Model
import Presenter.Internal.UITypes
import UI


playbackControl :: Monad m => EditScreenControl m
playbackControl = mediaControls &&& playbackControlRestart >>> combine


-- This is currently not working, seems we might have to register with the OS
-- in order to get media keys piped into the application.
mediaControls :: Monad m => EditScreenControl m
mediaControls = arr (\context ->
  if | inputIsKey MediaAudioNext Down context ->
        mempty -- TODO
     | inputIsKey MediaAudioPrev Down context ->
        mempty -- TODO
     | inputIsKey MediaAudioPlay Down context ->
        if Model.isPlayingOf (feedforwardIn context)
        then mempty { audioCommandOf = Just Model.StopPlayback }
        else mempty { audioCommandOf = Just Model.StartPlayback }
     | inputIsKey MediaAudioStop Down context ->
        mempty { audioCommandOf = Just Model.StopPlayback }
     | otherwise ->
        mempty
  )


--

playbackControlContinue :: Monad m => EditScreenControl m
playbackControlContinue =
  mealyState
    (const mempty)
    playbackControlContinueTransition

playbackControlContinueTransition context =
           (combinedCtrlKeyEvent Up context $> playbackControlRestart)
  `mergeL` (keyEvent KeySpace Down context  $> playbackControlContinuePrimed)

--

playbackControlContinuePrimed :: Monad m => EditScreenControl m
playbackControlContinuePrimed =
  mealyState
    (togglePlayback Model.maybeInitPlaybackCursor)
    playbackControlContinuePrimedTransition

playbackControlContinuePrimedTransition context =
           (combinedCtrlKeyEvent Up context $> playbackControlRestartPrimed)
  `mergeL` (        anyKeyEvent context
           `mergeL` anyButtonEvent context
           `mergeL` scrollEvent context       $> playbackControlContinue)

--

playbackControlRestart :: Monad m => EditScreenControl m
playbackControlRestart =
  mealyState
    (const mempty)
    playbackControlRestartTransition

playbackControlRestartTransition context =
           (combinedCtrlKeyEvent Down context $> playbackControlContinue)
  `mergeL` (keyEvent KeySpace Down context    $> playbackControlRestartPrimed)

--

playbackControlRestartPrimed :: Monad m => EditScreenControl m
playbackControlRestartPrimed =
  mealyState
    (togglePlayback Model.setPlaybackCursorToMarkingCursor)
    playbackControlRestartPrimedTransition

playbackControlRestartPrimedTransition context =
           (combinedCtrlKeyEvent Down context $> playbackControlContinuePrimed)
  `mergeL` (        anyKeyEvent context
           `mergeL` anyButtonEvent context
           `mergeL` scrollEvent context         $> playbackControlRestart)

--------

togglePlayback updateCursor context =
  case ( inputIsKey KeySpace Up context
       , Model.isPlayingOf (feedforwardIn context)) of
    (True, True)  ->
      mempty
      { audioCommandOf = Just Model.StopPlayback
      }
    (True, False) ->
      mempty
      { audioCommandOf = Just Model.StartPlayback
      , modelUpdateOf = updateCursor
      }
    _ -> mempty
