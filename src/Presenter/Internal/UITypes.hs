module Presenter.Internal.UITypes
 ( Feedback(..)
 , toModelInput
 , Control
 , Panel
 , Screen
 , EditScreenControl
 , EditScreenPanel
 , EditScreen
 , integrateFeedback
 ) where

import Control.Arrow

import Display
import Model
import UI


data Feedback
  = ModelFeedback
    { audioCommandOf :: !(Maybe AudioAction)
    , modelUpdateOf :: !(State -> State)
    }
  | QuitFeedback
  | SaveFeedback

instance Semigroup Feedback where
 SaveFeedback <> _ = SaveFeedback
 _ <> SaveFeedback = SaveFeedback
 QuitFeedback <> _ = QuitFeedback
 _ <> QuitFeedback = QuitFeedback
 (ModelFeedback la lm) <> (ModelFeedback ra rm) =
    ModelFeedback (la <> ra) (lm . rm)

instance Monoid Feedback where
  mempty = ModelFeedback Nothing id


toModelInput (ModelFeedback (Just a) u) = PerformActions u a
toModelInput (ModelFeedback Nothing u) = PerformAction u
toModelInput QuitFeedback = Quit
toModelInput SaveFeedback = Save


type Control m i = UIControl m i Feedback
type Panel m i = UIPanel m i Object Feedback
type Screen m i = UIScreen m i Object Feedback

type EditScreenControl m = Control m Status
type EditScreenPanel m = Panel m Status
type EditScreen m = Screen m Status


integrateFeedback :: Monad m => Control m i -> Panel m i -> Panel m i
integrateFeedback control panel =
  control &&& panel >>^ \(fb, o) -> o { feedbackOf = feedbackOf o <> fb }
