{-# LANGUAGE MultiWayIf #-}

module Presenter.Internal.ViewModeling where

import Audio
import Model
import Presenter.Input
import UI


data SampleMovement
  = SampleMovement
    { positionXOf :: !SampleOffset
    , deltaXOf :: !SampleOffset
    }


toSampleMovement :: PanelContext Status -> (Status -> SampleCount) -> Maybe SampleMovement
toSampleMovement context getNumSamples =
  let bounds@(Rectangle _ (Dimensions w _)) = boundsIn context
      numSamples = fromIntegral (getNumSamples (feedforwardIn context))
      makePosition = round . (* numSamples) . (`ratioX` bounds)
      makeMovement = round . (* numSamples) . (`ratio` w)
  in case (mousePositionIn context, userInputIn context) of
    (Just l, Mouse (Position (Movement _ (Deltas dx _)))) ->
      Just (SampleMovement (makePosition l) (makeMovement dx))
    (Just l, _) ->
      Just (SampleMovement (makePosition l) (makeMovement 0))
    _ -> Nothing

-- positive `direction` scrolls forward
-- negative `direction` scrolls backward
samplesToScroll :: SampleCount -> Int -> SampleCount
samplesToScroll numSamplesInView direction =
  let viewPercent = 0.04
      dir =
        if | direction > 0 ->  1.0
           | direction < 0 -> -1.0
           | otherwise     ->  0.0
      num = fromIntegral numSamplesInView
  in floor (num * dir * viewPercent)
