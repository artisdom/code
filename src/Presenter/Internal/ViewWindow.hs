{-# LANGUAGE BlockArguments #-}

module Presenter.Internal.ViewWindow where

import Control.Arrow
import Control.Monad.Fix
import Data.Functor

import Audio
import Display
import qualified Model
import Presenter.Internal.UITypes
import Presenter.Internal.ViewModeling
import UI hiding (boundsOf)


viewWindow :: Monad m => EditScreenPanel m
viewWindow = viewWindowIdle


--------------------------------
-- states
--------------------------------

viewWindowIdle :: Monad m => EditScreenPanel m
viewWindowIdle = mealyState viewWindowIdleOutput viewWindowIdleTransition

viewWindowIdleOutput context =
  case fullSampleMovementFor context of
    Nothing ->
      UIOutput
      { feedbackOf = mempty
      , generatorOf = viewWindowComponent b ViewWindowNeutral
      }
    Just (SampleMovement p _) ->
      UIOutput
      { feedbackOf = mempty { modelUpdateOf = scroll vs (userInputIn context) }
      , generatorOf = viewWindowComponent b
          if inViewWindow p then ViewWindowHover else ViewWindowNeutral
      }
  where
    b = boundsIn context
    s = feedforwardIn context
    vcp = Model.viewCursorPositionOf s
    vs = Model.viewSpanOf s
    inViewWindow p = vcp <= p && p < vcp+vs

viewWindowIdleTransition context =
  case fullSampleMovementFor context of
    Nothing -> noTransition
    Just (SampleMovement p _) ->
      buttonInBoundsEvent LeftButton Down context
      $> viewWindowDragging (if inViewWindow p then p - vcp else vs `div` 2)
  where
    s = feedforwardIn context
    vcp = Model.viewCursorPositionOf s
    vs = Model.viewSpanOf s
    inViewWindow p = vcp <= p && p < vcp+vs

--

viewWindowDragging :: Monad m => SampleOffset -> EditScreenPanel m
viewWindowDragging offset =
  mealyState (viewWindowDraggingOutput offset) viewWindowDraggingTransition

viewWindowDraggingOutput offset context =
  case fullSampleMovementFor context of
    Just (SampleMovement p _) ->
      UIOutput
      { feedbackOf = mempty { modelUpdateOf = Model.moveViewCursorTo (p - offset) }
      , generatorOf = viewWindowComponent b ViewWindowActive
      }
    Nothing ->
      UIOutput
      { feedbackOf = mempty
      , generatorOf = viewWindowComponent b ViewWindowNeutral
      }
  where
    b = boundsIn context

viewWindowDraggingTransition context =
  inputInterruptionEvent context $> viewWindowIdle

--------

viewWindowComponent :: Rectangle -> ViewWindowState -> Model.Status -> UILayout Object
viewWindowComponent b state status =
  Component b $ ViewWindow ViewWindowData
  { fullSpanOf = Model.sampleCountOf status
  , viewWindowStateOf = state
  , windowSpanOf = Model.viewSpanOf status
  , windowStartOf = Model.viewCursorPositionOf status
  }

fullSampleMovementFor = flip toSampleMovement Model.sampleCountOf

scroll numSamplesInView (Mouse (Scroll (Deltas sx sy))) =
  Model.moveViewCursorBy (samplesToScroll numSamplesInView (sx-sy))
scroll _ _ = id
