{-# LANGUAGE MultiWayIf #-}

module Presenter.Internal.AutoscrollControl
 ( autoscrollControl
 ) where

import Control.Arrow
import Control.Monad.Fix

import qualified Audio as A
import qualified Model
import Presenter.Internal.UITypes
import UI


--------------------------------
-- internal wire
--------------------------------

data InternalInput
  = InternalInput
    { shouldScrollOf :: !Bool
    , statusOf :: !Model.Status
    }

type InternalWire m = UIWire m InternalInput Feedback


makeInternalInput :: PanelContext Model.Status -> InternalInput
makeInternalInput context =
  -- TODO disable scrolling if end of file is on screen?
  -- might mess with zooming if we don't
  InternalInput shouldScroll status
  where
    status = feedforwardIn context
    shouldScroll = Model.isPlayingOf status && not (anyHeldInputIn context)


--------------------------------
-- control
--------------------------------

autoscrollControl :: (Monad m, MonadFix m) => EditScreenControl m
autoscrollControl = makeInternalInput ^>> idle

scrollViewTo n = mempty { modelUpdateOf = Model.moveViewCursorTo n }


--------------------------------
-- autoscroll states
--------------------------------

idle :: Monad m => InternalWire m
idle = mealyState idleOutput idleTransition

idleOutput _ = mempty

idleTransition internal =
  if shouldScrollOf internal
  then transitionTo delay
  else noTransition

--

delay :: Monad m => InternalWire m
delay = mealyState delayOutput delayTransition

delayOutput _ = mempty

delayTransition internal =
  if shouldScrollOf internal
  then transitionTo primed
  else transitionTo idle

--

primed :: Monad m => InternalWire m
primed = mealyState primedOutput primedTransition

primedOutput _ = mempty

primedTransition internal =
  let shouldScroll = shouldScrollOf internal
      status = statusOf internal
      sampleRate = Model.sampleRateOf status
      pcp = Model.playbackCursorPositionOf status
      vcp = Model.viewCursorPositionOf status
      half = Model.viewSpanOf status `div` 2
      sb = 2 -- number of seconds to take to find backwards
      sf = 3 -- number of seconts to take to match forwards
      transitionToScrolling p t i o =
        transitionTo . scrolling $ BezierConfig
        { playbackCursorPositionOf = p
        , sampleRateOf = Model.sampleRateOf status
        , scrollIntentOf = i
        , timeSpanOf = t
        , viewCursorPositionOf = Model.viewCursorPositionOf status
        , viewOffsetOf = o
        , viewSpanOf = Model.viewSpanOf status
        }
  in case (shouldScroll, pcp) of
    (True, Just p) ->
      if | p+sb*sampleRate < vcp -> transitionToScrolling p sb ToMeet 0
         | p < vcp               -> transitionTo waiting -- not worth scrolling yet
         | p < vcp + half        -> transitionTo waiting
         | otherwise             -> transitionToScrolling p sf ToMatch (-half)
    _ -> transitionTo idle

--

waiting :: Monad m => InternalWire m
waiting = mealyState waitingOutput waitingTransition

waitingOutput internal =
  let shouldScroll = shouldScrollOf internal
      status = statusOf internal
      pcp = Model.playbackCursorPositionOf status
      vcp = Model.viewCursorPositionOf status
      half = Model.viewSpanOf status `div` 2
  in case (shouldScroll, pcp) of
    (True, Just p) ->
      if p > vcp + half
      then scrollViewTo (p-half)
      else mempty
    _ -> mempty

waitingTransition internal =
  let shouldScroll = shouldScrollOf internal
      status = statusOf internal
      pcp = Model.playbackCursorPositionOf status
      vcp = Model.viewCursorPositionOf status
      half = Model.viewSpanOf status `div` 2
  in case (shouldScroll, pcp) of
    (False, _)     -> transitionTo idle
    (True, Just p) ->
      if p > vcp + half
      then transitionTo matching
      else noTransition
    _ -> noTransition

--

stateAfterIntent ToMeet  = waiting
stateAfterIntent ToMatch = matching

makeTimeF config p =
  fromIntegral (p - playbackCursorPositionOf config) /
  fromIntegral (timeSpanOf config * sampleRateOf config)

scrolling :: Monad m => BezierConfig -> InternalWire m
scrolling config =
  let bez = makeBezier config
      timeF = makeTimeF config
      nextState = stateAfterIntent (scrollIntentOf config)
      output = scrollingOutput bez timeF
      transition = scrollingTransition timeF nextState
  in mealyState output transition

scrollingOutput bez timeFor internal =
  let pcp = Model.playbackCursorPositionOf (statusOf internal)
  in case (shouldScrollOf internal, pcp) of
    (True, Just p) -> scrollViewTo (yOf (bez (timeFor p)))
    _ -> mempty

scrollingTransition timeFor nextState internal =
  let pcp = Model.playbackCursorPositionOf (statusOf internal)
  in case (shouldScrollOf internal, pcp) of
    (False, _)     -> transitionTo idle
    (True, Just p) ->
      if timeFor p > 1
      then transitionTo nextState
      else noTransition
    _ -> noTransition

--

matching :: Monad m => InternalWire m
matching = mealyState matchingOutput matchingTransition

matchingOutput internal =
  let shouldScroll = shouldScrollOf internal
      status = statusOf internal
      pcp = Model.playbackCursorPositionOf status
      half = Model.viewSpanOf status `div` 2
  in case (shouldScroll, pcp) of
    (True, Just p) -> scrollViewTo (p-half)
    _ -> mempty

matchingTransition internal =
  if shouldScrollOf internal
  then noTransition
  else transitionTo idle


--------------------------------
-- Bézier curve
--------------------------------

data ScrollIntent
  = ToMeet
  | ToMatch

slopeForIntent ToMeet  = 0
slopeForIntent ToMatch = 1


data BezierConfig
  = BezierConfig
    { playbackCursorPositionOf :: !A.SampleIndex
    , sampleRateOf :: !Int
    , scrollIntentOf :: !ScrollIntent
    , timeSpanOf :: !Int -- seconds
    , viewCursorPositionOf :: !A.SampleIndex
    , viewOffsetOf :: !A.SampleCount
    , viewSpanOf :: !A.SampleCount
    }


-- A Bézier curve is used to auto-scroll the view to the playback-cursor's
-- position, attaining the appropriate velocity when we reach it.
-- When going backwards, the velocity starts and ends at 0;
-- when going forwards, the velocity starts at 0 and ends matching the velcotiy
-- of the playback cursor.
-- The x-axis here is our time, which is relative to the starting position
-- of the playback cursor; time increments are created by adding multiples of
-- the sample rate to it.
-- The y-axis represents our desired position over time; in the beginning we
-- start at the current view position, and in the end we match the position of
-- the playback cursor, possibly minus some offset (so the cursor is located in
-- the middle of the screen after we scroll the view forward to it).
-- (When we're scrolling backwards, we just go to where the playback cursor will
-- be when it enters the screen on the left, and then we'll wait until it hits
-- the center of the screen to start scrolling with it again.)
-- The y-values of the two mid-points of the Bézier curve give us the starting
-- and ending velocities relative to the starting and ending points, resp. --
-- when the y-values of the mid-points are equal to those of the starting and
-- ending points, then the velocities will be zero.
-- In the case that we're matching the playback cursor's position when coming
-- from behind, the third point (2) will have a smaller y-value than the ending
-- point so that we'll match the velocity of the playback cursor when we reach
-- it as well.
--
-- For Bézier points 0-3: P_0 is the current view position, P_3 is the position
-- of the view when matcing the eventual position of the playback cursor:
--
-- Scrolling Forwards:
--
--    |
--    |         3
--    |      /
--    |    2
--    |    |
--    |    |
--    |    |
--   -0====1----------
--    |
--
--
-- Scrolling Backwards:
--
--    |
--   -0====1----------
--    |    |
--    |    |
--    |    2----3
--    |
--
makeBezier :: BezierConfig -> Float -> Coordinates
makeBezier config =
  let numSeconds = timeSpanOf config
      sampleRate = sampleRateOf config
      slopeToggle = slopeForIntent (scrollIntentOf config)
      initPCP = playbackCursorPositionOf config
      finalPCP = initPCP + sampleRate * numSeconds
      midPCP = initPCP + (finalPCP - initPCP) `div` 2
      initVCP = viewCursorPositionOf config
      finalVCP = finalPCP + viewOffsetOf config
      p0 = Coordinates initPCP initVCP
      p1 = Coordinates midPCP  initVCP
      -- a slopeToggle of 1 here gives us a matching velocity
      -- a slopeToggle of 0 gives us no velocity
      p2 = Coordinates midPCP  (finalVCP - slopeToggle * (finalPCP - midPCP))
      p3 = Coordinates finalPCP finalVCP
  in bezier [p0,p1,p2,p3]

plus :: Coordinates -> Coordinates -> Coordinates
(Coordinates xl yl) `plus` (Coordinates xr yr) = Coordinates (xl+xr) (yl+yr)
infixl 6 `plus`

times :: Float -> Coordinates -> Coordinates
c `times` (Coordinates x y) =
  Coordinates (round (c * fromIntegral x)) (round (c * fromIntegral y))
infixl 7 `times`

-- https://en.wikipedia.org/wiki/Bézier_curve
-- a nice visualization:
-- https://tholman.com/bezier-curve-simulation/
bezier :: [Coordinates] -> Float -> Coordinates
bezier [p0,p1,p2] t = -- quadratic
  (1-t)^2 `times` p0 `plus`
  2 * t * (1-t) `times` p1 `plus`
  t^2 `times` p2
bezier [p0,p1,p2,p3] t = -- cubic
  (1-t)^3 `times` p0 `plus`
  3 * t * (1-t)^2 `times` p1 `plus`
  3 * t^2 * (1-t) `times` p2 `plus`
  t^3 `times` p3
