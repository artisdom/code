module Presenter.Internal.TimeAxis where

import Control.Arrow

import Display
import Model
import Presenter.Internal.UITypes
import UI


type TimeAxisDataMaker = Status -> TimeAxisData


timeAxis :: Monad m => TimeAxisDataMaker -> EditScreenPanel m
timeAxis f = arr (\context ->
  UIOutput
  { feedbackOf = mempty
  , generatorOf = Component (boundsIn context) . TimeAxis . f
  })

timeAxisHeight = 20 :: Word
