module Presenter.Internal.Cursors where

import Control.Arrow

import Display
import Model
import Presenter.Internal.UITypes
import UI


type CursorDataMaker = Status -> Maybe CursorData


cursor :: Monad m => CursorDataMaker -> EditScreenPanel m
cursor f = arr (\context ->
  UIOutput
  { feedbackOf = mempty
  , generatorOf = maybe Invisible (Component (boundsIn context) . Cursor) . f
  })
