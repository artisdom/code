{-# LANGUAGE LambdaCase #-}

module Presenter.Internal.StatusPanel
 ( statusPanel
 , convertSaveStatus
 ) where

import Control.Arrow

import Audio
import qualified Display as D
import qualified Model
import Presenter.Internal.UITypes
import UI


statusPanel :: Monad m => EditScreenPanel m
statusPanel = arr (\context ->
  UIOutput
  { feedbackOf = mempty
  , generatorOf = Component (boundsIn context) . statusToStatus
  })

statusToStatus :: Model.Status -> D.Object
statusToStatus status =
  D.Status D.StatusData
  { D.playbackStatusOf = Model.isPlayingOf status
  , D.sampleRateValueOf = Model.sampleRateOf status
  , D.sampleTypeValueOf = Model.sampleTypeOf status
  , D.saveStatusOf = convertSaveStatus (Model.saveStatusOf status)
  }

convertSaveStatus = \case
  Model.Saved       -> D.Saved
  Model.SaveErrored -> D.SaveErrored
  Model.Saving p    -> D.Saving p
  Model.Unsaved     -> D.Unsaved
