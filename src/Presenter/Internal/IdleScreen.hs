module Presenter.Internal.IdleScreen
 ( idleScreen
 ) where

import Control.Arrow
import Control.Monad.Fix

import Display
import Presenter.Internal.QuitControl
import Presenter.Internal.UITypes
import UI


idleScreen :: (Monad m, MonadFix m) => PanelContext () -> Screen m ()
idleScreen context = trackPanelContext context >>>
  integrateFeedback quitControl instructions

instructions :: Monad m => Panel m ()
instructions = arr (\context ->
  UIOutput
  { feedbackOf = mempty
  , generatorOf = \_ -> Component (boundsIn context) Instructions
  })
