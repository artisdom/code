module Presenter.Internal.QuitControl where

import Control.Arrow
import Control.Monad.Fix

import Presenter.Internal.UITypes
import UI


quitControl :: (Monad m, MonadFix m) => Control m i
quitControl = arr (\context ->
  if combinedCtrlActiveIn context && inputIsKey KeyQ Down context
  then QuitFeedback
  else mempty)
