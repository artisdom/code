module Presenter.Internal.Toolbar
 ( ToolbarConfig(..)
 , toolbar
 , toolbarButton
 ) where

import Control.Arrow
import Control.Wire as W hiding ((.))
import Data.Functor

import Display hiding (paddingOf)
import Presenter.Internal.UITypes
import UI


data ToolbarConfig
  = ToolbarConfig
    { iconSizeOf :: !Dimensions
    , paddingOf :: !Word
    }

toolbar :: (Monad m) => ToolbarConfig -> [EditScreenPanel m] -> EditScreenPanel m
toolbar config [] = toolbar config [arr (\_ -> UIOutput mempty (const Invisible))]
toolbar config buttons = (W.id &&& fanThrough items) >>^ toOutput
  where
    items = map (positionInToolbar config) [0..] `zipW` buttons
    zipW = zipWith (^>>)

toOutput (context, o) =
  o { generatorOf = Node (Bar (boundsIn context) NoPattern) . generatorOf o }

positionInToolbar config index context =
  updateInput . updateBounds $ context
  where
    ff = feedforwardIn context
    Rectangle (Coordinates x y) _ = boundsIn context
    p = fromIntegral (paddingOf config)
    w = widthOf (iconSizeOf config)
    userInput = userInputIn context
    newCoords = Coordinates (x + p + (w+p) * index) (y+p)
    newBounds = Rectangle newCoords (iconSizeOf config)
    updateBounds = updatePanelContext (UIInput ff (Window (Resize newBounds)))
    updateInput =
      if inputIsResize context
      then updatePanelContext (UIInput ff None)
      else updatePanelContext (UIInput ff userInput)


data ToolbarButtonConfig ff fb
  = ToolbarButtonConfig
    { buttonEnabledOf :: !(ff -> Bool)
    , iconNameOf :: !IconName
    , releaseReactionOf :: !fb
    }

ifEnabled :: ButtonState -> ToolbarButtonConfig ff fb -> PanelContext ff -> ff -> UILayout Object
ifEnabled state config context ff =
  Component (boundsIn context) . Icon
  $ IconData (iconNameOf config)
  $ if buttonEnabledOf config ff then state else Disabled

buttonEnabled :: ToolbarButtonConfig ff fb -> PanelContext ff -> Bool
buttonEnabled config = buttonEnabledOf config . feedforwardIn


toolbarButton :: (Monad m, Monoid fb) => IconName -> (ff -> Bool) -> fb -> UIPanel m ff Object fb
toolbarButton icon enabled reaction = buttonIdle (ToolbarButtonConfig enabled icon reaction)

--

buttonIdle config = mealyState (buttonIdleOutput config) (buttonIdleTransition config)

buttonIdleOutput config context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = ifEnabled Enabled config context
  }

buttonIdleTransition config context = movementInBoundsEvent context $> buttonHover config

--

buttonHover config = mealyState (buttonHoverOutput config) (buttonHoverTransition config)

buttonHoverOutput config context =
  UIOutput
  { feedbackOf = mempty
  , generatorOf = ifEnabled Hover config context
  }

buttonHoverTransition config context =
  ((if not (buttonEnabled config context) then noTransition
    else buttonInBoundsEvent LeftButton Down context)
                                          $> buttonPressed config)
  `mergeL` (movedOutOfBoundsEvent context $> buttonIdle config)

--

buttonPressed config = mealyState (buttonPressedOutput config) (buttonPressedTransition config)

buttonPressedOutput config context =
  UIOutput
  { feedbackOf =
      if buttonEnabled config context
         && inputIsButtonActionInBounds LeftButton Up context
      then releaseReactionOf config
      else mempty
  , generatorOf = ifEnabled Active config context
  }

buttonPressedTransition config context =
  if not (buttonEnabled config context)
  then transitionTo (buttonHover config)
  else     (movedOutOfBoundsEvent context $> buttonIdle config)
  `mergeL` (inputInterruptionEvent context $> buttonHover config)
