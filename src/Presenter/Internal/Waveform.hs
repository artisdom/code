module Presenter.Internal.Waveform where

import Control.Arrow

import Display
import Model
import Presenter.Internal.UITypes
import UI


type WaveformDataMaker = Status -> WaveformData


waveform :: Monad m => WaveformDataMaker -> EditScreenPanel m
waveform f = arr (\context ->
  UIOutput
  { feedbackOf = mempty
  , generatorOf = Component (boundsIn context) . Waveform . f
  })
