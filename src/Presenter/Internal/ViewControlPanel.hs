{-# LANGUAGE BlockArguments #-}

module Presenter.Internal.ViewControlPanel
 ( viewControlPanel
 ) where

import Control.Arrow
import Control.Monad.Fix
import Data.Functor

import Display
import qualified Model
import Presenter.Internal.Cursors
import Presenter.Internal.TimeAxis
import Presenter.Internal.UITypes
import Presenter.Internal.ViewWindow
import Presenter.Internal.Waveform
import UI


makeMarkingCursorData :: CursorDataMaker
makeMarkingCursorData status =
  Just CursorData
  { cursorTypeOf = MarkingCursor
  , cursorViewSpanOf = Model.sampleCountOf status
  , cursorViewStartOf = 0
  , cursorPositionOf = Model.markingCursorPositionOf status
  }

makePlaybackCursorData :: CursorDataMaker
makePlaybackCursorData status =
  Model.playbackCursorPositionOf status <&> \p ->
    CursorData
    { cursorPositionOf = p
    , cursorTypeOf =
        PlaybackCursor
          if Model.isPlayingOf status
          then PlaybackCursorActive
          else PlaybackCursorIdle
    , cursorViewSpanOf = Model.sampleCountOf status
    , cursorViewStartOf = 0
    }

makeTimeAxisData :: TimeAxisDataMaker
makeTimeAxisData status =
  TimeAxisData
  { beginningSampleOf = 0
  , endingSampleOf = numSamples
  , numSamplesInViewOf = numSamples
  , sampleRateOf = Model.sampleRateOf status
  }
  where numSamples = Model.sampleCountOf status

makeWaveformData :: Interactivity -> WaveformDataMaker
makeWaveformData interactivity status =
  WaveformData
  { interactivityOf = interactivity
  , segmentsOf = Model.allSegmentsOf status
  , viewSpanOf = Model.sampleCountOf status
  , viewStartOf = 0
  }


--------------------------------

viewControlPanel :: (Monad m, MonadFix m) => EditScreenPanel m
viewControlPanel =
  splitPanel (Fixed SizedBottom timeAxisHeight)
    (layeredPanel
      [ waveform (makeWaveformData NoInteractivity)
      , cursor makeMarkingCursorData
      , cursor makePlaybackCursorData
      , viewWindow
      ])
    (timeAxis makeTimeAxisData)
