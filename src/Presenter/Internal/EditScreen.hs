{-# LANGUAGE MultiWayIf #-}

module Presenter.Internal.EditScreen
 ( EditScreenConfig(..)
 , editScreen
 , convertSaveStatus
 ) where

import Control.Arrow
import Control.Monad.Fix
import Control.Wire hiding ((.),id)
import Data.Maybe

import Display
import qualified Model
import Presenter.Internal.AutoscrollControl
import Presenter.Internal.PlaybackControl
import Presenter.Internal.Toolbar
import Presenter.Internal.QuitControl
import Presenter.Internal.StatusPanel
import Presenter.Internal.UITypes
import Presenter.Internal.ViewControlPanel
import Presenter.Internal.ViewPanel
import UI hiding (resizeBarSizeOf)


data EditScreenConfig
  = EditScreenConfig
    { resizeBarSizeOf :: Word
    , statusPanelHeightOf :: Word
    , toolbarHeightOf :: Word
    , toolbarPaddingOf :: Word
    , toolbarIconSizeOf :: Dimensions
    , viewControlHeightOf :: Word
    }


{-# ANN module "HLint: ignore Redundant bracket" #-}
editScreen
  :: (Monad m, MonadFix m)
  => EditScreenConfig -> PanelContext Model.Status -> EditScreen m
editScreen config context = trackPanelContext context >>>
  -- Lisp mode!
  (integrateFeedback
    (fanThrough
      [ pagingControl
      , playbackControl
      , quitControl
      , stateControl ])
    (splitPanel (Fixed SizedTop (toolbarHeightOf config))
      (toolbar (ToolbarConfig (toolbarIconSizeOf config) (toolbarPaddingOf config))
        [ (toolbarButton UndoIcon undoEnabled undoFeedback)
        , (toolbarButton RedoIcon redoEnabled redoFeedback)
        , (toolbarButton SaveIcon saveEnabled SaveFeedback) ])
      (splitPanel (Fixed SizedBottom (statusPanelHeightOf config))
        (integrateFeedback
          (autoscrollControl)
          (splitPanel
            (Expandable SizedBottom
              (resizeBarSizeOf config)
              (viewControlHeightOf config))
            (viewPanel)
            (viewControlPanel)))
        (statusPanel))))

redoFeedback = mempty { modelUpdateOf = Model.redo }
undoFeedback = mempty { modelUpdateOf = Model.undo }

redoEnabled :: Model.Status -> Bool
redoEnabled = Model.canRedoOf

saveEnabled :: Model.Status -> Bool
saveEnabled status =
  case Model.saveStatusOf status of
    Model.Saving _ -> False
    _ -> True

undoEnabled :: Model.Status -> Bool
undoEnabled = Model.canUndoOf

pagingControl :: (Monad m, MonadFix m) => EditScreenControl m
pagingControl = arr (\context ->
  mempty
  { modelUpdateOf =
      case (combinedCtrlActiveIn context, userInputIn context) of
        (False, Keyboard (Key Down KeyHome)) -> Model.seekBeginningView
        (False, Keyboard (Key Down KeyEnd )) -> Model.seekEndingView
        (False, Keyboard (Key Down KeyPageUp  )) -> Model.pageBackwardView
        (False, Keyboard (Key Down KeyPageDown)) -> Model.pageForwardView
        (True,  Keyboard (Key Down KeyHome)) ->
          Model.seekBeginningView . Model.setMarkingCursorToBeginning
        (True,  Keyboard (Key Down KeyEnd )) ->
          Model.seekEndingView    . Model.setMarkingCursorToEnding
        _ -> id
  })

stateControl :: (Monad m, MonadFix m) => EditScreenControl m
stateControl = arr (\context ->
  let meta = combinedCtrlActiveIn context
      metaShift = meta && shiftActiveIn context
  in if  | inputIsKey KeyS Down context && meta      -> SaveFeedback
         | inputIsKey KeyY Down context && meta      -> redoFeedback
         | inputIsKey KeyZ Down context && metaShift -> redoFeedback
         | inputIsKey KeyZ Down context && meta      -> undoFeedback
         | otherwise -> mempty
  )
