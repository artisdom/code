module Presenter.Internal.LoadScreen
 ( ProgressInfo(..)
 , loadScreen
 , makeLoadProgress
 ) where

import Control.Arrow
import Control.Monad.Fix

import qualified Display as D
import Presenter.Internal.QuitControl
import Presenter.Internal.UITypes
import UI


data ProgressInfo
  = ProgressInfo
    { clipsProgressOf :: !Float
    , filePathOf :: !FilePath
    , fileProgressOf :: !Float
    , visualsProgressOf :: !Float
    }


loadScreen
  :: (Monad m, MonadFix m)
  => PanelContext ProgressInfo -> Screen m ProgressInfo
loadScreen context = trackPanelContext context >>>
  integrateFeedback quitControl progress

progress :: Monad m => Panel m ProgressInfo
progress = arr (\context ->
  UIOutput
  { feedbackOf = mempty
  , generatorOf = Component (boundsIn context) . makeLoadProgress
  })

makeLoadProgress ff =
  D.LoadProgress D.LoadProgressData
  { D.clipsProgressOf = clipsProgressOf ff
  , D.filePathOf = filePathOf ff
  , D.fileProgressOf = fileProgressOf ff
  , D.visualsProgressOf = visualsProgressOf ff
  }
