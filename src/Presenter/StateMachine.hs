{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Presenter.StateMachine
 ( presenter
 ) where

import Control.Monad
import Control.Monad.Fix
import Control.Monad.Reader
import Data.Either
import Data.Foldable
import Pipes
import Pipes.Core
import System.FilePath
import Text.Casing

import Audio
import Config
import Control.Concurrent.Operation
import Display
import qualified Model
import Presenter.Internal.Context
import Presenter.Internal.EditScreen
import Presenter.Internal.IdleScreen
import Presenter.Internal.LoadScreen as Presenter
import Presenter.Internal.UITypes
import qualified Presenter.Input as Presenter
import qualified Presenter.Output as Presenter
import UI hiding (boundsOf, resizeBarSizeOf)


class
  ( HasSettings r
  , HasStyle r
  , HasProgramName r
  , MonadFix m
  , MonadReader r m
  , Operation m o
  ) => PresenterStateConstraint m r o where {}
instance
  ( HasSettings r
  , HasStyle r
  , HasProgramName r
  , MonadFix m
  , MonadReader r m
  , Operation m o
  ) => PresenterStateConstraint m r o where {}


type PresenterState = Proxy Model.Input Model.Output Presenter.Input Presenter.Output
type PresenterStateWithContext m = PresenterState (WithContext m)
type PresenterUIOutput i = UIOutput i Object Feedback
type PresenterUI m i = UIContext m (UIInput i) (PresenterUIOutput i)

type PresenterUIStepper m i =
  i -> PresenterStateWithContext m (Either () (PresenterUIOutput i) , PresenterUI m i)


stepPresenterUI
  :: PresenterStateConstraint m r o
  => PresenterUI m i -> UserInput -> PresenterUIStepper m i
stepPresenterUI uiContext userInput feedforward =
  lift . lift $ stepUI (UIInput feedforward userInput) uiContext


type IdleScreenUI m = PresenterUI m ()
type IdleScreenUIOutput = PresenterUIOutput ()
type LoadScreenUI m = PresenterUI m ProgressInfo
type LoadScreenUIOutput = PresenterUIOutput ProgressInfo
type EditScreenUI m = PresenterUI m Model.Status
type EditScreenUIOutput = PresenterUIOutput Model.Status


makeEditScreenContext
  :: PresenterStateConstraint m r o
  => Model.Status -> PresenterStateWithContext m (EditScreenUI m)
makeEditScreenContext status = do
  style <- asks getStyle
  let iconDim = 32
  let toolbarPadding = 4
  let config = EditScreenConfig
        { resizeBarSizeOf = editScreenResizeBarSizeOf style
        , statusPanelHeightOf = 25
        , toolbarHeightOf = 2*toolbarPadding + fromIntegral iconDim
        , toolbarPaddingOf = toolbarPadding
        , toolbarIconSizeOf = Dimensions iconDim iconDim
        , viewControlHeightOf = editScreenNavigationBarHeightOf style
        }
  rect <- lift getWindowRect
  let context = makePanelContext rect status
  return $ initUI (editScreen config context)


--------------------------------

presenter
  :: PresenterStateConstraint m r o
  => Presenter.Input -> PresenterState m ()
presenter input = runWithContext (uninitialized input)

uninitialized
  :: PresenterStateConstraint m r o
  => Presenter.Input -> PresenterStateWithContext m ()
uninitialized = \case
  Presenter.Initialize size -> do
    lift (setWindowSize size)
    goIdle Presenter.Render
  _ -> do
    input' <- respond Presenter.Uninitialized
    uninitialized input'


--------------------------------
-- idle state
--------------------------------

goIdle
  :: PresenterStateConstraint m r o
  => Presenter.Input -> PresenterStateWithContext m ()
goIdle input = do
  rect <- lift getWindowRect
  let context = makePanelContext rect ()
  let uiContext = initUI (idleScreen context)
  idle uiContext input

idle
  :: PresenterStateConstraint m r o
  => IdleScreenUI m -> Presenter.Input -> PresenterStateWithContext m ()
idle uiContext = \case
  Presenter.CloseRequest -> do
    _ <- request Model.Quit
    _ <- respond Presenter.Quit
    return ()
  Presenter.ContentDisplaying displaying ->
    idle uiContext (Presenter.Interactivity UI.None)
  Presenter.FileDrop path -> do
    lift (setFilePath path)
    response <- request (Model.Load path)
    rect <- lift getWindowRect
    let context = makePanelContext rect (ProgressInfo 0 path 0 0)
    let uiContext = initUI (loadScreen context)
    processLoadProgress (stepPresenterUI uiContext UI.None) response
  Presenter.Interactivity userInput -> do
    (output, uiContext') <- stepPresenterUI uiContext userInput ()
    processIdleScreenOutput uiContext' output
  Presenter.RebuildUI style ->
    useNewStyle style $ do
      rect <- lift getWindowRect
      let context = makePanelContext rect ()
      let uiContext' = initUI (idleScreen context)
      idle uiContext' (Presenter.Interactivity UI.None)
  Presenter.Render ->
    idle uiContext (Presenter.Interactivity UI.None)
  Presenter.Resize dims -> do
    lift (setWindowSize dims)
    let rect = Rectangle origin dims
    idle uiContext (Presenter.Interactivity (Window (Resize rect)))

processIdleScreenOutput
  :: PresenterStateConstraint m r o
  => IdleScreenUI m -> Either () IdleScreenUIOutput -> PresenterStateWithContext m ()
processIdleScreenOutput uiContext = \case
  Right (UIOutput QuitFeedback _) ->
    idle uiContext Presenter.CloseRequest
  Right (UIOutput _ g) -> do
    programName <- getNameOfProgram
    input' <- respond (Presenter.Display (g ()) programName)
    idle uiContext input'
  Left () -> do
    let message = "Something went wrong, go yell at the developer."
    input' <- respond (Presenter.Alert "Bad code!" message)
    goIdle input'


--------------------------------
-- file load state
--------------------------------

loading
  :: PresenterStateConstraint m r o
  => LoadScreenUI m -> Presenter.Input -> PresenterStateWithContext m ()
loading uiContext = \case
  Presenter.CloseRequest -> do
    -- TODO tell model to stop?
    _ <- respond Presenter.Quit
    return ()
  Presenter.ContentDisplaying displaying ->
    loading uiContext (Presenter.Interactivity UI.None)
  Presenter.FileDrop filePath ->
    goIdle (Presenter.FileDrop filePath)
  Presenter.Interactivity userInput -> do
    response <- request Model.Monitor
    processLoadProgress (stepPresenterUI uiContext userInput) response
  Presenter.RebuildUI style ->
    useNewStyle style $ do
      rect <- lift getWindowRect
      path <- lift getFilePath
      let context = makePanelContext rect (ProgressInfo 0 path 0 0)
      let uiContext' = initUI (loadScreen context)
      loading uiContext' (Presenter.Interactivity UI.None)
  Presenter.Render ->
    loading uiContext (Presenter.Interactivity UI.None)
  Presenter.Resize dims -> do
    lift (setWindowSize dims)
    let rect = Rectangle origin dims
    loading uiContext (Presenter.Interactivity (Window (Resize rect)))

processLoadProgress
  :: PresenterStateConstraint m r o
  => PresenterUIStepper m ProgressInfo -> Model.Output -> PresenterStateWithContext m ()
processLoadProgress genUI = \case
  Model.LoadProgressing path fileProgress clipsProgress -> do
    let progressInfo = ProgressInfo
          { Presenter.clipsProgressOf = clipsProgress
          , Presenter.filePathOf = path
          , Presenter.fileProgressOf = fileProgress
          , Presenter.visualsProgressOf = 0
          }
    (output, uiContext) <- genUI progressInfo
    processLoadScreenOutput loading uiContext progressInfo output
  Model.LoadFailed path error -> do
    let message = concat ["Could not load the file:\n", path, "\n\n", error]
    input' <- respond (Presenter.Alert "Error loading file" message)
    goIdle input'
  Model.LoadCompleted audio clips -> do
    threshold <- getThresholdIndBDown
    op <- lift . lift $ start (makeWaveformRepresentation audio clips threshold)
    let progressInfo = ProgressInfo
          { Presenter.clipsProgressOf = 1
          , Presenter.filePathOf = pathOf audio
          , Presenter.fileProgressOf = 1
          , Presenter.visualsProgressOf = 0
          }
    (output, uiContext) <- genUI progressInfo
    processLoadScreenOutput (makingVisuals op) uiContext progressInfo output
  _ -> do
    let message = "Something went wrong when loading the file."
    input' <- respond (Presenter.Alert "Bad code!" message)
    goIdle input'

processLoadScreenOutput
  :: PresenterStateConstraint m r o
  => (LoadScreenUI m -> Presenter.Input -> PresenterStateWithContext m ())
  -> LoadScreenUI m
  -> ProgressInfo
  -> Either () LoadScreenUIOutput
  -> PresenterStateWithContext m ()
processLoadScreenOutput nextState uiContext genArg = \case
  Right (UIOutput QuitFeedback _) ->
    nextState uiContext Presenter.CloseRequest
  Right (UIOutput _ gen) -> do
    programName <- getNameOfProgram
    path <- lift getFilePath
    let title = concat ["loading ", path, " - ", programName]
    input' <- respond (Presenter.Display
      { Presenter.toDrawOf = gen genArg
      , Presenter.windowTitleOf = title
      })
    nextState uiContext input'
  Left () -> do
    let message = "Something went wrong loading the file, go yell at the developer."
    input' <- respond (Presenter.Alert "Bad code!" message)
    goIdle input'


--------------------------------
-- visuals creation state
--------------------------------

makingVisuals
  :: PresenterStateConstraint m r o
  => o WaveformRepresentation -> LoadScreenUI m -> Presenter.Input
  -> PresenterStateWithContext m ()
makingVisuals op uiContext = \case
  Presenter.CloseRequest -> do
    lift . lift $ cancel op
    _ <- request Model.Quit
    _ <- respond Presenter.Quit
    return ()
  Presenter.ContentDisplaying displaying ->
    makingVisuals op uiContext (Presenter.Interactivity UI.None)
  Presenter.FileDrop filePath -> do
    lift . lift $ cancel op
    goIdle (Presenter.FileDrop filePath)
  Presenter.Interactivity userInput -> do
    state <- lift . lift $ poll op
    processVisualsProgress op (stepPresenterUI uiContext userInput) state
  Presenter.RebuildUI style ->
    useNewStyle style $ do
      rect <- lift getWindowRect
      path <- lift getFilePath
      let context = makePanelContext rect (ProgressInfo 0 path 0 0)
      let uiContext' = initUI (loadScreen context)
      makingVisuals op uiContext' (Presenter.Interactivity UI.None)
  Presenter.Render ->
    makingVisuals op uiContext (Presenter.Interactivity UI.None)
  Presenter.Resize dims -> do
    lift (setWindowSize dims)
    let rect = Rectangle origin dims
    makingVisuals op uiContext (Presenter.Interactivity (Window (Resize rect)))

processVisualsProgress
  :: PresenterStateConstraint m r o
  => o WaveformRepresentation
  -> PresenterUIStepper m ProgressInfo
  -> Status WaveformRepresentation
  -> PresenterStateWithContext m ()
processVisualsProgress op genUI = \case
  Completed waveformRepresentation -> do
    _ <- respond (Presenter.UseRepresentation waveformRepresentation)
    programName <- getNameOfProgram
    path <- lift getFilePath
    let title = concat ["loading ", path, " - ", programName]
    let progressInfo = ProgressInfo
          { Presenter.clipsProgressOf = 1
          , Presenter.filePathOf = path
          , Presenter.fileProgressOf = 1
          , Presenter.visualsProgressOf = 1
          }
    rect <- lift getWindowRect
    let output = Left $ Presenter.Display
          { Presenter.toDrawOf = UI.Component rect (makeLoadProgress progressInfo)
          , Presenter.windowTitleOf = title
          }
    response <- request Model.Monitor
    processModelResponse makeEditScreenContext output response
  Errored _ -> do
    let message = "Something went wrong when creating visuals for the file."
    input' <- respond (Presenter.Alert "Bad code!" message)
    goIdle input'
  Progressing p -> do
    path <- lift getFilePath
    let progressInfo = ProgressInfo
          { Presenter.clipsProgressOf = 1
          , Presenter.filePathOf = path
          , Presenter.fileProgressOf = 1
          , Presenter.visualsProgressOf = p
          }
    (output, uiContext) <- genUI progressInfo
    processLoadScreenOutput (makingVisuals op) uiContext progressInfo output


--------------------------------
-- running state
--------------------------------

running
  :: PresenterStateConstraint m r o
  => EditScreenUI m -> Model.Status -> Presenter.Input
  -> PresenterStateWithContext m ()
running uiContext status = \case
  Presenter.CloseRequest ->
    ifPermittedTo "quit" uiContext status quitOp
  Presenter.ContentDisplaying displaying ->
    running uiContext status (Presenter.Interactivity UI.None)
  Presenter.FileDrop filePath ->
    ifPermittedTo "load" uiContext status $ do
      _ <- request $ Model.PerformAudioAction Model.StopPlayback
      goIdle (Presenter.FileDrop filePath)
  Presenter.Interactivity userInput -> do
    (output, uiContext') <- stepPresenterUI uiContext userInput status
    processEditScreenOutput uiContext' status output
  Presenter.RebuildUI style ->
    useNewStyle style $ do
      uiContext' <- makeEditScreenContext status
      running uiContext' status (Presenter.Interactivity UI.None)
  Presenter.Render ->
    running uiContext status (Presenter.Interactivity UI.None)
  Presenter.Resize dims -> do
    lift (setWindowSize dims)
    let rect = Rectangle origin dims
    running uiContext status (Presenter.Interactivity (Window (Resize rect)))

ifPermittedTo
  :: PresenterStateConstraint m r o
  => String
  -> EditScreenUI m
  -> Model.Status
  -> PresenterStateWithContext m ()
  -> PresenterStateWithContext m ()
ifPermittedTo action uiContext status affirmativeAction =
  let okOption' = okOption { textOf = pascal action }
      options = [okOption', cancelOption]
      negativeAction = running uiContext status Presenter.Render
  in case Model.saveStatusOf status of
    Model.Saved -> affirmativeAction
    Model.SaveErrored -> do
      let title = "Save errored!"
      let message = concat ["Your last save errored, ", action, " anyway?"]
      input <- respond (Presenter.Prompt title message options)
      processOkCancelPromptResponse affirmativeAction negativeAction input
    Model.Saving _ -> do
      let title = "Still saving!!!"
      let message = concat ["Your file is STILL BEING SAVED, ", action, " anyway?"]
      input <- respond (Presenter.Prompt title message options)
      processOkCancelPromptResponse affirmativeAction negativeAction input
    Model.Unsaved -> do
      let title = "Work unsaved!"
      let message = concat ["You have unsaved work, ", action, " anyway?"]
      input <- respond (Presenter.Prompt title message options)
      processOkCancelPromptResponse affirmativeAction negativeAction input

processOkCancelPromptResponse
  :: PresenterStateConstraint m r o
  => PresenterStateWithContext m ()
  -> PresenterStateWithContext m ()
  -> Presenter.Input
  -> PresenterStateWithContext m ()
processOkCancelPromptResponse okAction cancelAction = \case
  Presenter.PromptResponse (Just r) ->
    case toEnum r of
      Ok -> okAction
      Cancel -> cancelAction
  _ -> cancelAction

quitOp
  :: PresenterStateConstraint m r o
  => PresenterStateWithContext m ()
quitOp = do
  _ <- request (Model.PerformAudioAction Model.StopPlayback)
  _ <- request Model.Quit
  _ <- respond Presenter.Quit
  return ()

processEditScreenOutput
  :: PresenterStateConstraint m r o
  => EditScreenUI m -> Model.Status -> Either () EditScreenUIOutput
  -> PresenterStateWithContext m ()
processEditScreenOutput uiContext status = \case
  Right (UIOutput feedback g) ->
    case feedback of
      ModelFeedback{} -> do
        response <- request (toModelInput feedback)
        processModelResponse produceUIContext (Right g) response
      QuitFeedback ->
        ifPermittedTo "quit" uiContext status quitOp
      SaveFeedback -> do
        response <-
            case Model.saveStatusOf status of
              Model.Saving _ -> do
                _ <- respond (Presenter.Alert "Already Saving" "Save already in progress")
                request Model.Monitor
              _ -> request (toModelInput feedback)
        processModelResponse produceUIContext (Right g) response
  Left () -> do
    let message = "Something went horribly wrong, go yell at the developer."
    input' <- respond (Presenter.Alert "Bad code!" message)
    goIdle input'
  where
    produceUIContext = const (return uiContext)

processModelResponse
  :: PresenterStateConstraint m r o
  => (Model.Status -> PresenterStateWithContext m (EditScreenUI m))
  -> Either Presenter.Output (Model.Status -> UILayout Object)
  -> Model.Output
  -> PresenterStateWithContext m ()
processModelResponse produceUIContext output = \case
  Model.Running status -> do
    input' <-
      case (Model.errorOf status, output) of
        (Just ex, _) ->
          respond (Presenter.Alert "Error!" (show ex))
        (_, Left o) ->
          respond o
        (_, Right g) -> do
          path <- lift getFilePath
          let saveStatus = convertSaveStatus (Model.saveStatusOf status)
          programName <- getNameOfProgram
          let (_, fileName) = splitFileName path
          let decorator = toDecorator saveStatus
          let title = concat [decorator, " ", fileName, " - ", programName, " - ", path]
          respond (Presenter.Display (g status) title)
    uiContext <- produceUIContext status
    running uiContext status input'
  _ -> do
    let message = "Something went wrong, go yell at the developer."
    input' <- respond (Presenter.Alert "Bad code!" message)
    goIdle input'

toDecorator = \case
  SaveErrored         -> "⚠" -- U+26A0
  Saved               -> "☰"
  Saving p | p > 0.83 -> "☱"
  Saving p | p > 0.67 -> "☲"
  Saving p | p > 0.50 -> "☳"
  Saving p | p > 0.33 -> "☴"
  Saving p | p > 0.17 -> "☵"
  Saving _            -> "☶"
  Unsaved             -> "☷"
