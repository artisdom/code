module Presenter.Output where

import Display
import UI


data Output
  = Uninitialized
  | Display
    { toDrawOf :: !(UILayout Object)
    , windowTitleOf :: !String
    }
  | Alert
    { summaryOf :: !String
    , detailsOf :: !String
    }
  | Notify
    { summaryOf :: !String
    , detailsOf :: !String
    }
  | Prompt
    { summaryOf :: !String
    , detailsOf :: !String
    , optionsOf :: !DialogOptions
    }
  | UseRepresentation !WaveformRepresentation
  | Quit
