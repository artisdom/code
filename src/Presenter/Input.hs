module Presenter.Input
 ( ButtonName(..)
 , KeyName(..)
 , Input(..)
 , Movement(..)
 ) where

import Config.Style
import UI


data Input
  = Initialize !Dimensions
  -- window
  | CloseRequest
  | ContentDisplaying !Bool
  | FileDrop !FilePath
  | Interactivity !UserInput
  | PromptResponse !(Maybe Int)
  | RebuildUI !Style
  | Resize !Dimensions
  -- no input
  | Render
