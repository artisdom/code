{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}

module Control.Concurrent.Operation where

import Control.Exception


data Status a
  = Completed !a
  | Errored !SomeException
  | Progressing !Float -- 0-1 of progress

instance Functor Status where
  fmap f (Completed a)   = Completed (f a)
  fmap _ (Errored e)     = Errored e
  fmap _ (Progressing p) = Progressing p

class Monad m => Operation m o | m -> o where
  start :: ((Float -> m ()) -> m r) -> m (o r)
  cancel :: o r -> m ()
  poll :: o r -> m (Status r)
