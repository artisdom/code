{-# LANGUAGE FunctionalDependencies #-}

module Control.Concurrent.Container where

import Control.Concurrent.MVar
import Control.Monad.ST
import Data.STRef


class Monad m => Container m a | m -> a where
  create :: b -> m (a b)
  read :: a b -> m b
  update :: a b -> (b -> b) -> m ()

instance Container IO MVar where
  create = newMVar
  read = readMVar
  update a f = do
    v <- takeMVar a
    putMVar a (f v)

instance Container (ST s) (STRef s) where
  create = newSTRef
  read = readSTRef
  update = modifySTRef
