{-# LANGUAGE Rank2Types #-}

module Config
 ( ConfigFilePaths(..)
 , Config
 , HasProgramName(..)
 , HasSettings(..)
 , HasStyle(..)
 , getAudioDriverName
 , getDefaultViewSpanInS
 , getMaxIntraclipSilenceInMs
 , getNameOfProgram
 , getPlaybackBufferSampleCount
 , getThresholdIndBDown
 , defaultConfig
 , establishConfigFiles
 , readConfigFiles
 --
 , getSetting
 , listSettings
 , setSetting
 , module Config.Style
 ) where

import Control.Lens
import Control.Monad.Except
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Maybe
import Data.Yaml
import System.Directory
import System.FilePath
import System.Info

import Config.Settings
import Config.Style
import Display


data ConfigFilePaths
  = ConfigFilePaths
    { settingsFilePathOf :: !FilePath
    , styleFilePathOf :: !FilePath
    }

data Config
  = Config
    { settingsOf :: !Settings
    , styleOf :: !Style
    }

instance HasSettings Config where
  getSettings = settingsOf

instance HasStyle Config where
  getStyle = styleOf
  updateStyle s c = c { styleOf = s }


class HasProgramName a where
  getProgramName :: a -> String

getNameOfProgram :: (MonadReader r m, HasProgramName r) => m String
getNameOfProgram = asks getProgramName


defaultConfig =
  Config
  { settingsOf = defaultSettings
  , styleOf = defaultStyle
  }

establishConfigFiles
  :: MonadIO m
  => Maybe FilePath -> Maybe FilePath -> m ConfigFilePaths
establishConfigFiles alternateSettingsFilePath alternateStyleFilePath = liftIO $ do
  defaultConfigDir <- liftIO (getXdgDirectory XdgConfig "osabe")
  liftIO (createDirectoryIfMissing True defaultConfigDir)
  let defaultSettingsPath = defaultConfigDir </> "settings.yaml"
  let settingsPath = fromMaybe defaultSettingsPath alternateSettingsFilePath
  establishSettingsFile settingsPath
  let defaultStylePath = defaultConfigDir </> "style.yaml"
  let stylePath = fromMaybe defaultStylePath alternateStyleFilePath
  establishStyleFile stylePath
  return (ConfigFilePaths settingsPath stylePath)

readConfigFiles
  :: (MonadIO m)
  => ConfigFilePaths -> m (Config, [String])
readConfigFiles configFilePaths = do
  (settings, settingsMessages) <- readSettingsFile (settingsFilePathOf configFilePaths)
  (style, styleMessages) <- readStyleFile (styleFilePathOf configFilePaths)
  return (Config settings style, settingsMessages ++ styleMessages)
