{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiWayIf #-}

module Model.State
 ( State
 , newState
 , MarkingCursorManipulation(..)
 , PlaybackManipulation(..)
 , ViewManipulation(..)
 , getAllSegments
 , getVisibleSegments
 , removeInvolving
 , removeInvolvingSpan
 , splitClip
 , truncateGap
 , startGapExpansion
 , continueGapExpansion
 , completeGapExpansion
 , abortGapExpansion
 , paste
 , getAudioFile
 , getSaveStatus
 , saved
 , AS.canUndo
 , undo
 , AS.canRedo
 , redo
 ) where

import qualified Control.Lens as L
import Data.Functor
import Data.Maybe

import Audio
import Model.Internal.Model
import Model.Internal.References
import qualified Model.Internal.AppState as AS


data Workspace
  = Workspace
    { cursorOf :: !Cursor
    , reversionOf :: !(Model -> Model)
    }


data SaveState
  = SaveState
    { lastSaveOf :: !(Maybe Segments)
    , savedOf :: !Bool
    }

update :: Segments -> SaveState -> SaveState
update segments state =
  state { savedOf = Just segments == lastSaveOf state }


type State = AS.AppState Model Segments SaveState Workspace

newState :: SampleCount -> SampleCount -> AudioFile -> Segments -> State
newState numPlaybackSamples numViewSamples audioFile segments =
  AS.newAppState (SaveState (Just segments) True) $
  newModel numPlaybackSamples numViewSamples audioFile segments


instance AudioData State where
  numberOfSamplesIn = AS.from numberOfSamplesIn
  sampleRateOf = AS.from sampleRateOf
  sampleTypeOf = AS.from sampleTypeOf
  fillWith = AS.from fillWith
  dBFSAt i = AS.from (dBFSAt i)
  belowThreshold = AS.from belowThreshold

instance PlaybackSource State where
  playbackSuspended = AS.from playbackSuspended
  getSegmentsToPlay n = AS.from (getSegmentsToPlay n)
  recordSamplesPlayed s = AS.over (recordSamplesPlayed s)

instance MarkingCursorManipulation State where
  getMarkingCursor = AS.from getMarkingCursor
  getMarkingCursorPosition = AS.from getMarkingCursorPosition
  setMarkingCursorInViewTo o = AS.over (setMarkingCursorInViewTo o)
  setMarkingCursorToClipInViewAt o = AS.over (setMarkingCursorToClipInViewAt o)
  setMarkingCursorToBeginning = AS.over setMarkingCursorToBeginning
  setMarkingCursorToEnding = AS.over setMarkingCursorToEnding

instance PlaybackManipulation State where
  getPlaybackCursorPosition = AS.from getPlaybackCursorPosition
  maybeInitPlaybackCursor = AS.over maybeInitPlaybackCursor
  pageForwardPlayback = AS.over pageForwardPlayback
  setPlaybackCursorToMarkingCursor = AS.over setPlaybackCursorToMarkingCursor

instance ViewManipulation State where
  cursorForSamplesFromViewStart s = AS.from (cursorForSamplesFromViewStart s)
  getViewCursorPosition = AS.from getViewCursorPosition
  getViewSpan = AS.from getViewSpan
  moveViewCursorBy o = AS.over (moveViewCursorBy o)
  moveViewCursorTo i = AS.over (moveViewCursorTo i)
  pageBackwardView = AS.over pageBackwardView
  pageForwardView = AS.over pageForwardView
  seekBeginningView = AS.over seekBeginningView
  seekEndingView = AS.over seekEndingView
  zoomView p i = AS.over (zoomView p i)

instance SegmentManipulation State where
  getAllSegments = AS.from getAllSegments
  getAudibleSegments = AS.from getAudibleSegments
  getSegmentsSpanning c1 c2 = AS.from (getSegmentsSpanning c1 c2)
  getVisibleSegments = AS.from getVisibleSegments
  insertSegmentsAfter c s ru =
    updateSaveState . AS.over (insertSegmentsAfter c s ru)
  removeSegmentsAfter c s ru =
    updateSaveState . AS.over (removeSegmentsAfter c s ru)
  replaceSegmentsSpanning c num s ru =
    updateSaveState . AS.over (replaceSegmentsSpanning c num s ru)


--------------------------------
-- removal
--------------------------------

removeInvolving :: SampleOffset -> State -> State
removeInvolving samplesFromViewCursor state =
  let markingCursor = getMarkingCursor state
  in case cursorForSamplesFromViewStart samplesFromViewCursor state of
    Just cursor ->
      let segments = AS.from getAllSegments state
      in if isClip (segmentAtCursor cursor segments)
      then
          let gapCursor = moveToBeginningOfNextSegment cursor segments
          in removeBetweenCursors (Just cursor) gapCursor state
      else removeBetweenCursors (Just markingCursor) (Just cursor) state
    Nothing ->
      removeBetweenCursors (Just markingCursor) Nothing state

removeInvolvingSpan :: SampleOffset -> SampleOffset -> State -> State
removeInvolvingSpan samplesFromViewStart1 samplesFromViewStart2 state =
  removeBetweenCursors
    (cursorForSamplesFromViewStart samplesFromViewStart1 state)
    (cursorForSamplesFromViewStart samplesFromViewStart2 state)
    state

removeBetweenCursors :: Maybe Cursor -> Maybe Cursor -> State -> State
removeBetweenCursors cursor1 cursor2 state =
  if pointToSameSegment cursor1' cursor2' && isGap (segmentAtCursor cursor1' segments)
  then state
  else
    updateSaveState $ AS.apply AS.Patch
    { AS.backwardOf = insertSegmentsAfter before removed insertionUpdater
    , AS.forwardOf = replaceSegmentsSpanning from num emptySegments removalUpdater
    }
    state { AS.clipboardOf = Just removed }
  where
    segments = AS.from getAllSegments state
    endCursor = cursorAtLastSample segments
    cursor1' = fromMaybe endCursor cursor1
    cursor2' = fromMaybe endCursor cursor2
    (from, to) = arrangeForRemoval cursor1' cursor2' segments
    removed = AS.from (getSegmentsSpanning from to) state
    num = numSegments removed
    before = fromJust (moveToEndOfPreviousSegment from segments)
    insertionUpdater = insertionReferencesUpdater before removed
    removalUpdater = removalReferencesUpdater from to

insertionReferencesUpdater
  :: Cursor -> Segments -> Segments -> Segments -> References -> References
insertionReferencesUpdater before toInsert old new references =
  case moveToBeginningOfNextSegment before old of
    Nothing -> references
    Just c ->
      L.over markingCursorReference relocate .
      L.over (playbackViewReference.cursor) (fmap relocate) .
      L.over (viewViewReference.cursor.identity) relocate $
      references
      where
        relocate cursor =
          if cursor < c then cursor
          else advanceCursorInBounds num c new
          where num = numSamplesIn toInsert + numSamplesBetween c cursor old


data Location
  = BeforeSpan
  | InSpan
  | AfterSpan !SampleCount

location :: Cursor -> Cursor -> Cursor -> Segments -> Location
location c begin end segments =
  if | c < begin'               -> BeforeSpan
     | begin' <= c && c <= end' -> InSpan
     | otherwise                -> AfterSpan num
  where
    begin' = moveToBeginningOfSegment begin segments
    end' = moveToEndOfSegment end segments
    num = numSamplesBetween (fromJust (moveToBeginningOfNextSegment end segments)) c segments

removalReferencesUpdater
  :: Cursor -> Cursor -> Segments -> Segments -> References -> References
removalReferencesUpdater c1 c2 old new references =
  let before = fromJust (moveToEndOfPreviousSegment c1 old)
      viewDiff =
        numSamplesBetween
          (references L.^.viewViewReference.cursor.identity)
          (fromMaybe (cursorAtLastSample old) (moveToBeginningOfNextSegment c2 old))
          old
      after = moveToBeginningOfNextSegment before new
      endOfNew = cursorAtLastSample new
  in
  L.over markingCursorReference (\c ->
    case location c c1 c2 old of
      BeforeSpan  -> c
      InSpan      -> fromMaybe endOfNew after
      AfterSpan n -> advanceCursorInBounds n (fromJust after) new
  ) .
  L.over (playbackViewReference.cursor) (\c ->
    case c of
      Nothing -> c
      Just c' ->
        case location c' c1 c2 old of
          BeforeSpan  -> c
          InSpan      -> after
          AfterSpan n -> fmap (\c' -> advanceCursorInBounds n c' new) after
  ) .
  L.over (viewViewReference.cursor.identity) (\c ->
    case location c c1 c2 old of
      BeforeSpan  -> c
      InSpan      -> retreatCursorInBounds viewDiff (fromMaybe endOfNew after) new
      AfterSpan n -> advanceCursorInBounds n (fromJust after) new
  ) $
  references


--------------------------------
-- splitting
--------------------------------

splitClip :: SampleOffset -> State -> State
splitClip samplesFromViewCursor state =
  case cursorForSamplesFromViewStart samplesFromViewCursor state of
    Nothing -> state
    Just tc ->
      updateSaveState $ AS.apply AS.Patch
      { AS.backwardOf = replaceSegmentsSpanning tc' 3 replaced updater
      , AS.forwardOf = replaceSegmentsSpanning tc' 1 replacements updater
      }
      state
      where
        segments = AS.from getAllSegments state
        segment = segmentAtCursor tc segments
        tc' = moveToBeginningOfSegment tc segments
        updater = splitReferencesUpdater tc'
        dist = numSamplesBetween tc tc' segments
        (pre, post) = splitSegment dist segment
        replaced = singletonSegments segment
        replacements = toSegments [pre, makeGap 0, post]

-- reference cursor should point to the beginning of the segment being un/split
splitReferencesUpdater
  :: Cursor -> Segments -> Segments -> References -> References
splitReferencesUpdater reference old new =
  L.over markingCursorReference relocate .
  L.over (playbackViewReference.cursor) (fmap relocate) .
  L.over (viewViewReference.cursor.identity) relocate
  where
    relocate cursor =
      if cursor < reference then cursor
      else advanceCursorInBounds num reference new
      where num = numSamplesBetween reference cursor old


--------------------------------
-- resizing
--------------------------------

truncateGap :: SampleOffset -> State -> State
truncateGap samplesFromViewCursor state =
  case cursorForSamplesFromViewStart samplesFromViewCursor state of
    Nothing -> state
    Just tc ->
      let segments = AS.from getAllSegments state
          dist = numSamplesBetween tc (moveToBeginningOfSegment tc segments) segments
      in sizeGap tc dist state

startGapExpansion :: SampleOffset -> State -> State
startGapExpansion samplesFromViewCursor state =
  let segments = AS.from getAllSegments state
      segment = segmentAtCursor cursor segments
      cursor =
        case cursorForSamplesFromViewStart samplesFromViewCursor state of
          Just tc ->
            if isClip (segmentAtCursor tc segments)
            then fromJust (moveToEndOfPreviousSegment tc segments)
            else tc
          Nothing -> cursorAtLastSample segments
      replaced = singletonSegments segment
      updater = resizeReferencesUpdater cursor
  in state
     { AS.workspaceOf = Just
         Workspace
         { Model.State.cursorOf = cursor
         , reversionOf = replaceSegmentsSpanning cursor 1 replaced updater
         }
     }

continueGapExpansion :: SampleCount -> State -> State
continueGapExpansion num state =
  case AS.workspaceOf state of
    Nothing -> state
    Just (Workspace c _) ->
      let segments = AS.from getAllSegments state
          segment = segmentAtCursor c segments
          segment' = segment { lengthOf = max 0 (lengthOf segment + num) }
          replacement = singletonSegments segment'
          updater = resizeReferencesUpdater c
          replaceSegments = replaceSegmentsSpanning c 1 replacement updater
      in state { AS.stateOf = replaceSegments (AS.stateOf state) }

completeGapExpansion :: State -> State
completeGapExpansion state =
  case AS.workspaceOf state of
    Nothing -> state
    Just (Workspace c r) ->
      let segments = AS.from getAllSegments state
          segment = segmentAtCursor c segments
          patch =
            AS.Patch
            { AS.backwardOf = r
            , AS.forwardOf = replaceSegmentsSpanning c 1 replacement updater
            }
          replacement = singletonSegments segment
          updater = resizeReferencesUpdater c
      in updateSaveState $ AS.apply patch state { AS.workspaceOf = Nothing }

abortGapExpansion :: State -> State
abortGapExpansion state =
  state
  { AS.stateOf = (maybe id reversionOf $ AS.workspaceOf state) (AS.stateOf state)
  , AS.workspaceOf = Nothing
  }

sizeGap :: Cursor -> SampleCount -> State -> State
sizeGap cursor size state =
  updateSaveState $ AS.apply AS.Patch
  { AS.backwardOf = replaceSegmentsSpanning cursor' 1 replaced updater
  , AS.forwardOf = replaceSegmentsSpanning cursor' 1 replacement updater
  }
  state
  where
    segments = AS.from getAllSegments state
    segment = segmentAtCursor cursor segments
    size' = max 0 size
    segment' = segment { lengthOf = size' }
    cursor' = moveToBeginningOfSegment cursor segments
    replaced = singletonSegments segment
    replacement = singletonSegments segment'
    updater = resizeReferencesUpdater cursor'

resizeReferencesUpdater
  :: Cursor -> Segments -> Segments -> References -> References
resizeReferencesUpdater reference old new references =
  let endOfNew = cursorAtLastSample new
      after = moveToBeginningOfNextSegment reference new
      reference' = moveToBeginningOfSegment reference old
      newLen = lengthOf $ segmentAtCursor reference' new
      -- this function is used polymorphically in the a,b arguments below:
      chooseNewPosition c a b =
        let num = numSamplesBetween c reference' old
        in if pointToSameSegment c reference' && newLen <= num
        then a
        else b
  in
  L.over markingCursorReference (\c ->
    -- BUGFIX: the cursor offset wasn't returning to 0 when the gap was expanded
    -- from having been 0-length (with Nothing offset) at the end of the audio,
    -- so the `reformSampleOffset` was added to make this change:
    let c' = reformSampleOffset c new
    in chooseNewPosition c (fromMaybe endOfNew after) c'
  ) .
  L.over (playbackViewReference.cursor) (\c ->
    case c of
      Nothing -> c
      Just c' -> chooseNewPosition c' after c
  ) .
  L.over (viewViewReference.cursor.identity) (\c ->
    let num = numSamplesBetween c reference' old
        resizeRelToGap =
          case after of
            Nothing -> pointToSameSegment c reference'
            Just a  -> pointToSameSegment c reference' || pointToSameSegment c a
    in if resizeRelToGap
    then advanceCursorInBounds num reference' new
    else c
  ) $
  references


--------------------------------
-- other ops
--------------------------------

paste :: SampleCount -> State -> State
paste samplesFromViewCursor state =
  case AS.clipboardOf state of
    Nothing -> state
    Just cd ->
      updateSaveState $ AS.apply AS.Patch
      { AS.backwardOf = removeSegmentsAfter before num removalUpdater
      , AS.forwardOf = insertSegmentsAfter before cd insertionUpdater
      }
      state
      where
        segments = AS.from getAllSegments state
        num = numSegments cd
        before =
          case cursorForSamplesFromViewStart samplesFromViewCursor state of
            Just c | isClip (segmentAtCursor c segments) ->
              fromJust (moveToEndOfPreviousSegment c segments)
            Just c  -> c
            Nothing -> cursorAtLastSample segments
        insertionUpdater = insertionReferencesUpdater before cd
        removalUpdater old new =
          let from = fromJust (moveToBeginningOfNextSegment before old)
              to = makeCursorSpanning num from old
          in removalReferencesUpdater from to old new

getAudioFile :: State -> AudioFile
getAudioFile = audioFileOf . AS.stateOf

getSaveStatus :: State -> Maybe Bool
getSaveStatus state =
  let saveState = AS.saveStateOf state
  in lastSaveOf saveState $> savedOf saveState

updateSaveState :: State -> State
updateSaveState state =
  let segments = AS.from getAllSegments state
  in state { AS.saveStateOf = update segments (AS.saveStateOf state) }

saved :: Maybe Segments -> State -> State
saved segments state =
  -- editing may have happened since the segments to be saved were captured,
  -- so don't set the save state to True but instead False and do a comparison
  -- using `updateSaveState` to set it to the correct value.
  updateSaveState $ state { AS.saveStateOf = SaveState segments False }

undo = updateSaveState . AS.undo
redo = updateSaveState . AS.redo
