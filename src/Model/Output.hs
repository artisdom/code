module Model.Output
 ( Output(..)
 , SaveStatus(..)
 , Status(..)
 ) where

import Control.Exception

import Audio


data Output
  = Idling
  | LoadProgressing
    { pathOf :: !FilePath
    , fileProgressOf :: !Float
    , segmentsProgressOf :: !Float
    }
  | LoadFailed
    { pathOf :: !FilePath
    , messageOf :: !String
    }
  | LoadCompleted
    { audioDataOf :: !AudioFile
    , segmentsOf :: !Segments
    }
  | Running !Status

data SaveStatus
  = Saved
  | SaveErrored
  | Saving !Float
  | Unsaved

data Status
  = Status
    { allSegmentsOf :: !Segments
    , canRedoOf :: !Bool
    , canUndoOf :: !Bool
    , errorOf :: !(Maybe SomeException)
    , isPlayingOf :: !Bool
    , markingCursorPositionOf :: !SampleIndex
    , playbackCursorPositionOf :: !(Maybe SampleIndex)
    , sampleCountOf :: !SampleCount
    , sampleRateOf :: !Int
    , sampleTypeOf :: !SampleType
    , saveStatusOf :: !SaveStatus
    , segmentsInViewOf :: !Segments
    , viewCursorPositionOf :: !SampleIndex
    , viewSpanOf :: !SampleCount
    }
