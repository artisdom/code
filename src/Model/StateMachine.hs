{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Model.StateMachine
 ( model
 ) where

import Prelude hiding (read)
import Control.Applicative
import Control.Exception
import Control.Monad
import Control.Monad.Reader
import Data.Functor
import Data.Maybe
import Pipes
import Pipes.Core

import qualified Audio
import Config
import Control.Concurrent.Container
import Control.Concurrent.Operation as Op
import Model.Input
import Model.Output
import Model.State


type ModelState = Server Input Output

class
  ( Audio.AudioLoader m
  , Audio.AudioPlayer m a State d
  , Container m a
  , HasSettings r
  , MonadReader r m
  , Operation m o
  ) => ModelStateConstraint m r a o d where {}
instance
  ( Audio.AudioLoader m
  , Audio.AudioPlayer m a State d
  , Container m a
  , HasSettings r
  , MonadReader r m
  , Operation m o
  ) => ModelStateConstraint m r a o d where {}


model :: ModelStateConstraint m r a o d => Input -> ModelState m ()
model = idle

idle :: ModelStateConstraint m r a o d => Input -> ModelState m ()
idle = \case
  Load path -> do
    op <- lift (start (Audio.readAudio path))
    input' <- respond (LoadProgressing path 0 0)
    loadingFile path op input'
  _ -> do
    input' <- respond Idling
    idle input'


--------------------------------
-- file load state
--------------------------------

loadingFile
  :: ModelStateConstraint m r a o d
  => FilePath -> o Audio.AudioFile -> Input -> ModelState m ()
loadingFile path op input =
  case input of
    Load path -> do
      lift (cancel op) -- stop loading thread
      idle input
    _ -> do
      loadState <- lift (poll op)
      processLoadProgress path op loadState

processLoadProgress
  :: ModelStateConstraint m r a o d
  => FilePath -> o Audio.AudioFile -> Op.Status Audio.AudioFile -> ModelState m ()
processLoadProgress path op = \case
  Progressing progress -> do
    input' <- respond (LoadProgressing path progress 0)
    loadingFile path op input'
  Errored e -> do
    input' <- respond (LoadFailed path (displayException e))
    idle input'
  Completed audio -> do
    threshold <- lift getThresholdIndBDown
    silence <- lift getMaxIntraclipSilenceInMs
    let context = Audio.makeSegmentsReadingContext threshold silence audio
    op' <- lift (start (Audio.constructSegments context))
    input' <- respond (LoadProgressing path 1 0)
    makingSegments audio op' input'


--------------------------------
-- segments creation state
--------------------------------

makingSegments
  :: ModelStateConstraint m r a o d
  => Audio.AudioFile -> o Audio.Segments -> Input -> ModelState m ()
makingSegments audio op input =
  case input of
    Load path -> do
      lift (cancel op) -- stop loading thread
      idle input
    _ -> do
      loadState <- lift (poll op)
      processSegmentsProgress audio op loadState

processSegmentsProgress
  :: ModelStateConstraint m r a o d
  => Audio.AudioFile -> o Audio.Segments -> Op.Status Audio.Segments -> ModelState m ()
processSegmentsProgress audio op = \case
  Progressing progress -> do
    let path = Audio.pathOf audio
    input' <- respond (LoadProgressing path 1 progress)
    makingSegments audio op input'
  Errored e -> do
    let path = Audio.pathOf audio
    input' <- respond (LoadFailed path (displayException e))
    idle input'
  Completed segments -> do
    numPlaybackSamples <- lift getPlaybackBufferSampleCount
    viewSpan <- lift getDefaultViewSpanInS <&> fmap (* Audio.sampleRateOf audio)
    let sampleCount = Audio.numSamplesIn segments
    let numViewSamples = maybe sampleCount (min sampleCount) viewSpan
    let model = newState numPlaybackSamples numViewSamples audio segments
    var' <- lift (create model)
    input' <- respond (LoadCompleted audio segments)
    audioDevice <- lift (Audio.openPlaybackDevice var')
    running audioDevice var' Nothing input'


--------------------------------
-- running state
--------------------------------

-- NOTE: all checks for save status will be done outside of the model;
-- the model assumes it is always safe to run an op when told to do so
running
  :: ModelStateConstraint m r a o d
  => d -> a State -> Maybe (o Audio.Segments) -> Input -> ModelState m ()
running audioDevice var saveOp input =
  case input of
    Load path -> do
      lift (Audio.closePlaybackDevice audioDevice)
      idle input
    Monitor -> continueRunning audioDevice var saveOp
    PerformAction modelAction -> do
      lift (update var modelAction)
      continueRunning audioDevice var saveOp
    PerformActions modelAction audioAction -> do
      lift (update var modelAction)
      lift (performAudioAction audioAction audioDevice)
      continueRunning audioDevice var saveOp
    PerformAudioAction audioAction -> do
      lift (performAudioAction audioAction audioDevice)
      continueRunning audioDevice var saveOp
    Quit -> lift (Audio.closePlaybackDevice audioDevice)
    Save -> do
      runState <- lift (read var)
      let audioFile = getAudioFile runState
      let segments = getAllSegments runState
      saveOp' <- lift (start (Audio.saveAudio audioFile segments))
      continueRunning audioDevice var (Just saveOp')

continueRunning
  :: ModelStateConstraint m r a o d
  => d -> a State -> Maybe (o Audio.Segments) -> ModelState m ()
continueRunning audioDevice var saveOp = do
  runState <- lift (read var)
  isPlaying <- lift (Audio.deviceIsPlaying audioDevice)
  saveOpStatus <- lift (getSaveOpStatus saveOp)
  let saveOp' = nextSaveOp saveOp saveOpStatus
  lift (updateSaveState var saveOpStatus)
  let error = getError saveOpStatus
  let saveStatus = fromMaybe SaveErrored $
        fromSaveOpStatus runState saveOpStatus <|> fromSaveStateStatus runState
  let output = Running (makeStatus runState isPlaying saveStatus error)
  input' <- respond output
  running audioDevice var saveOp' input'

performAudioAction StartPlayback = Audio.startPlaybackDevice
performAudioAction StopPlayback  = Audio.stopPlaybackDevice

getSaveOpStatus
  :: ModelStateConstraint m r a o d
  => Maybe (o Audio.Segments) -> m (Maybe (Op.Status Audio.Segments))
getSaveOpStatus Nothing   = return Nothing
getSaveOpStatus (Just op) = Just <$> poll op

nextSaveOp
  :: Maybe (o Audio.Segments)
  -> Maybe (Op.Status Audio.Segments)
  -> Maybe (o Audio.Segments)
nextSaveOp origSaveOp = \case
  (Just Progressing{}) -> origSaveOp
  _ -> Nothing

updateSaveState
  :: ModelStateConstraint m r a o d
  => a State -> Maybe (Op.Status Audio.Segments) -> m ()
updateSaveState var = \case
  Just (Completed s) -> update var (saved (Just s))
  Just (Errored ex) ->
    case fromException ex :: Maybe Audio.AudioFileException of
      Just Audio.ErrorRestoringBackup{}      -> update var (saved Nothing)
      Just Audio.ErrorWritingWithoutBackup{} -> update var (saved Nothing)
      _ -> return ()
  _ -> return ()

getError = \case
  Just (Errored ex) -> Just ex
  _ -> Nothing

fromSaveOpStatus state = \case
  Just (Completed s)   -> Just if getAllSegments state == s then Saved else Unsaved
  Just Errored{}       -> Just SaveErrored
  Just (Progressing p) -> Just (Saving p)
  _ -> Nothing

fromSaveStateStatus state =
  case getSaveStatus state of
    Just True  -> Just Saved
    Just False -> Just Unsaved
    Nothing    -> Just SaveErrored

makeStatus runState isPlaying saveStatus error =
  Status
  { allSegmentsOf = getAllSegments runState
  , canRedoOf = canRedo runState
  , canUndoOf = canUndo runState
  , errorOf = error
  , isPlayingOf = isPlaying
  , markingCursorPositionOf = getMarkingCursorPosition runState
  , playbackCursorPositionOf = getPlaybackCursorPosition runState
  , sampleCountOf = Audio.numSamplesIn (getAllSegments runState)
  , sampleRateOf = Audio.sampleRateOf runState
  , sampleTypeOf = Audio.sampleTypeOf runState
  , saveStatusOf = saveStatus
  , segmentsInViewOf = getVisibleSegments runState
  , viewCursorPositionOf = getViewCursorPosition runState
  , viewSpanOf = getViewSpan runState
  }
