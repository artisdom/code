module Model.Input
 ( Input(..)
 , AudioAction(..)
 ) where

import Model.State


data Input
  = Load !FilePath
  | Monitor
  | PerformAction !(State -> State)
  | PerformActions !(State -> State) !AudioAction
  | PerformAudioAction !AudioAction
  | Quit
  | Save

data AudioAction
  = StartPlayback
  | StopPlayback
  deriving (Show)

instance Semigroup AudioAction where
  StartPlayback <> _ = StartPlayback
  _ <> StartPlayback = StartPlayback
  _ <> _             = StopPlayback
