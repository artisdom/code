{-# LANGUAGE LambdaCase #-}

module Audio.Segment
 ( SampleCount
 , SampleIndex
 , SampleOffset
 , Segment(..)
 , makeGap
 , isClip
 , isGap
 , offsetOfFirstSampleIn
 , offsetOfLastSampleIn
 , trimBeginning
 , trimEnd
 , splitSegment
 , expandGap
 ) where


type SampleCount = Int
type SampleIndex = Int
type SampleOffset = Int

data Segment
  = Clip
    { startOf :: !SampleIndex
    , lengthOf :: !SampleCount
    }
  | InsertedGap
    { lengthOf :: !SampleCount
    }
  | ReferenceGap
    { startOf :: !SampleIndex
    , lengthOf :: !SampleCount
    , originalLengthOf :: !SampleCount
    }
  deriving (Read, Show, Eq, Ord)


makeGap = InsertedGap

isClip :: Segment -> Bool
isClip Clip{} = True
isClip _ = False

isGap :: Segment -> Bool
isGap = not . isClip

offsetOfFirstSampleIn :: Segment -> Maybe SampleOffset
offsetOfFirstSampleIn segment =
  if lengthOf segment == 0 then Nothing else Just 0

offsetOfLastSampleIn :: Segment -> Maybe SampleOffset
offsetOfLastSampleIn segment =
  if lengthOf segment == 0 then Nothing else Just (lengthOf segment - 1)

trimBeginning :: SampleCount -> Segment -> Segment
trimBeginning amount = \case
  Clip s l           -> Clip (s+amount) (l-amount)
  InsertedGap l      -> InsertedGap (l-amount)
  ReferenceGap s l o ->
    if o-amount <= 0
    then InsertedGap (l-amount)
    else ReferenceGap (s+amount) (l-amount) (o-amount)

trimEnd :: SampleCount -> Segment -> Segment
trimEnd amount = \case
  Clip s l           -> Clip s (l-amount)
  InsertedGap l      -> InsertedGap (l-amount)
  ReferenceGap s l o -> ReferenceGap s (l-amount) o

-- TODO TEST!!!
-- what to do when offset is 0 or == length?
-- what about 0-length segments?
splitSegment :: SampleOffset -> Segment -> (Segment, Segment)
splitSegment offset segment =
  ( trimEnd (lengthOf segment - offset) segment
  , trimBeginning offset segment
  ) where offset' = max 1 $ min (lengthOf segment - 1) offset

expandGap :: SampleCount -> Segment -> Segment
expandGap _ c@Clip{} = c
expandGap amount gap = gap { lengthOf = max 0 $ lengthOf gap + amount }
