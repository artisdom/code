module Audio.Wave.Sample where

import Data.Binary.Get
import Data.Binary.Put
import Data.Int
import Data.Word

import Audio.AudioSample


class (AudioSample a) => WaveSample a where
  getter :: Get a
  putter :: a -> Put


instance WaveSample Word8 where
  getter = getWord8
  putter = putWord8

instance WaveSample Int16 where
  getter = getInt16le
  putter = putInt16le

instance WaveSample Int24 where
  getter = getInt24le
  putter = putInt24le

instance WaveSample Int32 where
  getter = getInt32le
  putter = putInt32le

instance WaveSample Float where
  getter = getFloatle
  putter = putFloatle
