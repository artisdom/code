module Audio.Wave.Header
 ( WaveFileHeader(..)
 , WaveFileHeaderException
 , WaveSampleType(..)
 , getSampleRate
 , readWaveFileHeader
 , writeWithWaveHeader
 ) where

import qualified Codec.Audio.Wave as W
import Control.Exception
import Data.Maybe
import Data.Set
import Data.Word
import System.IO


data WaveSampleType
  = Word8NE
  | Int16LE
  | Int24LE
  | Int32LE
  | FloatLE

data WaveFileHeader
  = WaveFileHeader
    { fileSizeOf :: !Int
    , internalHeaderOf :: !W.Wave
    , offsetOfDataInFileOf :: !Integer
    , waveSampleTypeOf :: !WaveSampleType
    }


getSampleRate :: WaveFileHeader -> Int
getSampleRate = fromIntegral . W.waveSampleRate . internalHeaderOf


--------------------------------
-- Exceptions
--------------------------------

data WaveFileHeaderException
  = ErrorReadingHeader
  | FileTooBig
  | IOErrorReadingHeader !IOException
  | NotAWaveFile
  | OnlyMonoSupported
  | UnsupportedSampleType
  | UnsupportedWaveFormat
  deriving (Show)

instance Exception WaveFileHeaderException

fromWaveLibraryException :: SomeException -> Maybe WaveFileHeaderException
fromWaveLibraryException ex =
  case fromException ex :: Maybe W.WaveException of
    Just e ->
      case e of
        W.BadFileFormat message path ->
          Just NotAWaveFile
        W.NonDataChunkIsTooLong byteString path ->
          Just UnsupportedWaveFormat
        W.NonPcmFormatButMissingFact path ->
          Just UnsupportedWaveFormat
    Nothing ->
      Nothing

fromHeaderIOException :: SomeException -> Maybe WaveFileHeaderException
fromHeaderIOException ex =
  case fromException ex :: Maybe IOException of
    Just e  -> Just (IOErrorReadingHeader e)
    Nothing -> Nothing


--------------------------------
-- Header Methods
--------------------------------

readWaveFileHeader :: FilePath -> IO WaveFileHeader
readWaveFileHeader path = do
  result <- try (W.readWaveFile path)
  let header = case result of
        Right h -> h
        Left ex -> throw . head $
          catMaybes
            [ fromWaveLibraryException ex
            , fromHeaderIOException ex
            , Just ErrorReadingHeader
            ]
  return WaveFileHeader
    { fileSizeOf = numberOfSamples header
    , internalHeaderOf = header
    , offsetOfDataInFileOf = dataOffsetInFile header
    , waveSampleTypeOf = getSampleType header
    }

writeWithWaveHeader :: WaveFileHeader -> FilePath -> (Handle -> IO ()) -> IO ()
writeWithWaveHeader = flip W.writeWaveFile . internalHeaderOf

getSampleType :: W.Wave -> WaveSampleType
getSampleType header =
  case W.waveSampleFormat header of
    W.SampleFormatPcmInt  8 -> Word8NE
    W.SampleFormatPcmInt 16 -> Int16LE
    W.SampleFormatPcmInt 24 -> Int24LE
    W.SampleFormatPcmInt 32 -> Int32LE
    W.SampleFormatIeeeFloat32Bit -> FloatLE
    _ -> throw UnsupportedSampleType

numberOfSamples :: W.Wave -> Int
numberOfSamples header =
  if numSamplesInFile > maxVectorSize
  then throw FileTooBig
  else fromIntegral numSamplesInFile
  where
    numSamplesInFile = W.waveSamplesTotal header -- Word64
    maxVectorSize = fromIntegral (maxBound::Int) :: Word64

dataOffsetInFile :: W.Wave -> Integer
dataOffsetInFile header =
  if size (W.waveChannelMask header) /= 1
  then throw OnlyMonoSupported
  else toInteger (W.waveDataOffset header)
