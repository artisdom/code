{-# LANGUAGE FlexibleContexts #-}

module Audio.AudioFile where

import Control.Exception
import Control.Monad
import Data.Int
import System.Directory

import Audio.AudioData
import Audio.Segments
import Audio.WaveFile


data AudioFile
  = AudioFile
    { pathOf :: !FilePath
    , dataOf :: !WaveFile
    }

instance AudioData AudioFile where
  numberOfSamplesIn = numberOfSamplesIn . dataOf
  sampleRateOf = sampleRateOf . dataOf
  sampleTypeOf = sampleTypeOf . dataOf
  fillWith = fillWith . dataOf
  dBFSAt i = dBFSAt i . dataOf
  belowThreshold = belowThreshold . dataOf


data AudioFileException
  = ErrorCreatingBackup !IOException
  | ErrorPerformingOperation !WaveFileException -- will need to vary when more file types
  | ErrorRestoringBackup
    { ioExceptionOf :: !IOException
    , operationExceptionOf :: !WaveFileException -- will also need to vary
    }
  | ErrorWritingWithoutBackup !WaveFileException -- will also need to vary
  | FileSizeError !String
  deriving (Show)

instance Exception AudioFileException


readAudioFile :: FilePath -> (Float -> IO ()) -> IO AudioFile
readAudioFile path report = do
  r <- try (readWaveFile path report) :: IO (Either WaveFileException WaveFile)
  case r of
    Left ex -> throwIO (ErrorPerformingOperation ex)
    Right w -> return (AudioFile path w)

-- From https://en.wikipedia.org/wiki/WAV#Limitations
-- > The WAV format is limited to files that are less than 4 GiB, because of its
-- > use of a 32-bit unsigned integer to record the file size header.
maxWaveFileSize :: Int
maxWaveFileSize = fromIntegral (maxBound::Int32)

saveAudioFile :: AudioFile -> Segments -> (Float -> IO ()) -> IO Segments
saveAudioFile (AudioFile path waveFile) segments report = do
  -- TODO update with correct size for file type when more types are added
  when (numSamplesExceedsMaxAllowed maxWaveFileSize segments) $
    throwIO (FileSizeError "Audio too long for file format")
  exists <- doesFileExist path
  if exists
  then do
    r <- try $ do
      let temp = path ++ ['~']
      copyFile path temp
      return temp
    case r :: Either IOException FilePath of
      Left ex -> throwIO (ErrorCreatingBackup ex)
      Right temp -> do
        r <- try (writeWaveFile path waveFile segments report)
        case r :: Either WaveFileException () of
          Left ex -> do
            -- remove the file so we don't try to use a broken copy as a backup
            _ <- try (removeFile path) :: IO (Either IOException ())
            r <- try (copyFile temp path)
            case r :: Either IOException () of
              Left ex' -> throwIO (ErrorRestoringBackup ex' ex)
              Right () -> do
                -- ignore backup removal errors
                _ <- try (removeFile temp) :: IO (Either IOException ())
                throwIO (ErrorPerformingOperation ex)
          Right _ -> do
            -- ignore backup removal errors
            _ <- try (removeFile temp) :: IO (Either IOException ())
            return segments
  else do
    -- working without a net!!
    r <- try (writeWaveFile path waveFile segments report)
    case r :: Either WaveFileException () of
      Left ex -> do
        -- remove the file so we don't try to use a broken copy as a backup
        _ <- try (removeFile path) :: IO (Either IOException ())
        throwIO (ErrorWritingWithoutBackup ex)
      Right _ -> return segments
