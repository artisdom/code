{-# OPTIONS_GHC -Wno-overflowed-literals #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE NegativeLiterals #-}

module Audio.AudioSample
 ( SampleType(..)
 , AudioSample(..)
 , belowdBFS
 , module Audio.Int24
 , int24ToFloatSample
 ) where

import Data.Int
import Data.Word

import Audio.Decibel
import Audio.Int24


data SampleType
  = Word8
  | Int16
  | Int24
  | Int32
  | Float
  deriving (Eq, Ord, Show)


class (Num a, Ord a) => AudioSample a where
  sizeInBytes :: Word
  minValue  :: a
  zeroValue :: a
  maxValue  :: a
  dBFS :: a -> Decibel
  dBFSToSample :: Float -> a
  negateSample :: a -> a
  negateSample s =
    if s < zeroValue
    then zeroValue + (zeroValue - s)
    else zeroValue - (s - zeroValue)
  {-# INLINE negateSample #-}


belowdBFS :: (AudioSample a) => Float -> a -> Bool
belowdBFS threshold s =
  nth <= s && s <= th
  where
    th = dBFSToSample threshold
    nth = negateSample th
{-# INLINE belowdBFS #-}


dBFSToScale :: Float -> Float
dBFSToScale d = 10**(d/20)
{-# INLINE dBFSToScale #-}


--------------------------------
-- Word8 Instance
--------------------------------

instance AudioSample Word8 where
  sizeInBytes = 1
  minValue  = 0x00
  zeroValue = 0x80
  maxValue  = 0xFF
  dBFS a = dBDownFrom (word8MaxF - word8ZeroF) (fromIntegral a - word8ZeroF)
  dBFSToSample d = floor (dBFSToScale d * (word8MaxF-word8ZeroF) + word8ZeroF)

word8ZeroF :: Float
word8ZeroF = fromIntegral (zeroValue :: Word8)

word8MaxF :: Float
word8MaxF = fromIntegral (maxValue :: Word8)


--------------------------------
-- Int16 Instance
--------------------------------

instance AudioSample Int16 where
  sizeInBytes = 2
  minValue  = 0x8000::Int16
  zeroValue = 0x0000::Int16
  maxValue  = 0x7FFF::Int16
  dBFS a = dBDownFrom int16MaxF (fromIntegral a)
  dBFSToSample d = floor (dBFSToScale d * int16MaxF)

int16MaxF :: Float
int16MaxF = fromIntegral (maxValue :: Int16)


--------------------------------
-- Int24 Instance
--------------------------------

instance AudioSample Int24 where
  sizeInBytes = 3
  minValue  = I24 (0xFF800000::Int32)
  zeroValue = I24 (0x00000000::Int32)
  maxValue  = I24 (0x007FFFFF::Int32)
  dBFS a = dBDownFrom int24MaxF (fromIntegral a)
  dBFSToSample d = floor (dBFSToScale d * int24MaxF)

int24MaxF :: Float
int24MaxF = fromIntegral (unI24 (maxValue :: Int24))

int24ToFloatSample (I24 i24) = fromIntegral i24 / int24MaxF


--------------------------------
-- Int32 Instance
--------------------------------

instance AudioSample Int32 where
  sizeInBytes = 4
  minValue  = 0x80000000::Int32
  zeroValue = 0x00000000::Int32
  maxValue  = 0x7FFFFFFF::Int32
  dBFS a = dBDownFrom int32MaxF (fromIntegral a)
  dBFSToSample d = floor (dBFSToScale d * int32MaxF)

int32MaxF :: Float
int32MaxF = fromIntegral (maxValue :: Int32)


--------------------------------
-- Float Instance
--------------------------------

instance AudioSample Float where
  sizeInBytes = 4
  minValue  = -1.0
  zeroValue =  0.0
  maxValue  =  1.0
  dBFS = dBDownFrom maxValue
  dBFSToSample = dBFSToScale
