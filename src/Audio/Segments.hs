module Audio.Segments
 ( Segments
 , SegmentCount
 , SegmentIndex
 , SegmentsReadingContext
 , makeSegmentsReadingContext
 , constructSegments
 , emptySegments
 , singletonSegments
 , toSegments
 , toList
 , concatSegments
 , sliceSegments
 , takeSegments
 , dropSegments
 , sliceIndicesFor
 , forEachWithAccum
 , numSegments
 , numSamplesIn
 , numSamplesExceedsMaxAllowed
 , segmentAt
 , modifyFirstAndLast
 ) where

import Control.Monad.Reader
import Control.Monad.ST
import Data.List
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM

import Audio.AudioData
import Audio.AudioSample
import Audio.Segment
import Audio.Decibel


newtype Segments
  = Segments
    { segmentsOf :: V.Vector Segment
    }
  deriving (Eq)

type SegmentCount = Int
type SegmentIndex = Int


data SegmentsReadingContext
  = SegmentsReadingContext
    { audioSampleRateOf :: !Int
    , belowThresholdOf :: !(Int -> Bool)
    , maxIntraclipSilenceInMsOf :: !Int
    , numberOfSamplesOf :: !Int
    }

makeSegmentsReadingContext :: (AudioData a) => Float -> Int -> a -> SegmentsReadingContext
makeSegmentsReadingContext threshold silence audio
  = SegmentsReadingContext
    { audioSampleRateOf = sampleRateOf audio
    , belowThresholdOf = audio `belowThreshold` threshold
    , maxIntraclipSilenceInMsOf = silence
    , numberOfSamplesOf = numberOfSamplesIn audio
    }


constructSegments :: (Monad m) => SegmentsReadingContext -> (Float -> m ()) -> m Segments
constructSegments context report =
  toSegments <$> constructFrom context report

constructFrom :: (Monad m) => SegmentsReadingContext -> (Float -> m ()) -> m [Segment]
constructFrom context report
  | numberOfSamplesOf context == 0 =
      return [InsertedGap 0]
  | belowThresholdOf context 0 =
      continueInGap context report 0 1
  | otherwise =
      (InsertedGap 0 :) <$> continueInClip context report 0 1

continueInGap
  :: (Monad m)
  => SegmentsReadingContext -> (Float -> m ())
  -> SampleIndex -> SampleIndex -> m [Segment]
continueInGap context report start index
  | index == total = do
      report progress
      return [ReferenceGap start l l]
  | belowThresholdOf context index = do
      report progress
      continueInGap context report start (index+1)
  | otherwise = do
      report progress
      (ReferenceGap start l l :) <$> continueInClip context report index (index+1)
  where
    l = index - start
    total = numberOfSamplesOf context
    progress = fromIntegral index / fromIntegral total

continueInClip
  :: (Monad m)
  => SegmentsReadingContext -> (Float -> m ())
  -> SampleIndex -> SampleIndex -> m [Segment]
continueInClip context report start index
  | index == total = do
      report progress
      return [Clip start (index - start), InsertedGap 0]
  | belowThresholdOf context index = do
      report progress
      continueInAmbiguous context report start index (index+1)
  | otherwise = do
      report progress
      continueInClip context report start (index+1)
  where
    total = numberOfSamplesOf context
    progress = fromIntegral index / fromIntegral total

-- might be intra-clip silence, might be a gap, not sure yet, it's ambiguous
continueInAmbiguous
  :: (Monad m)
  => SegmentsReadingContext -> (Float -> m ())
  -> SampleIndex -> SampleIndex -> SampleIndex -> m [Segment]
continueInAmbiguous context report start ambiguous index
  | index == total = do
      report progress
      return [Clip start cl, ReferenceGap ambiguous gl gl]
  | belowThresholdOf context index = do
      report progress
      if gl < maxIntraclipSilence
      then continueInAmbiguous context report start ambiguous (index+1)
      else (Clip start cl :) <$> continueInGap context report ambiguous (index+1)
  | otherwise = do
      report progress
      continueInClip context report start (index+1)
  where
    cl = ambiguous - start
    gl = index - ambiguous
    total = numberOfSamplesOf context
    progress = fromIntegral index / fromIntegral total
    maxIntraclipSilence =
      (audioSampleRateOf context * maxIntraclipSilenceInMsOf context) `div` 1000

emptySegments :: Segments
emptySegments = Segments V.empty

singletonSegments :: Segment -> Segments
singletonSegments = Segments . V.singleton

toSegments :: [Segment] -> Segments
toSegments = Segments . V.fromList

toList :: Segments -> [Segment]
toList = V.toList . segmentsOf

concatSegments :: [Segments] -> Segments
concatSegments = Segments . V.concat . map segmentsOf

sliceSegments :: SegmentIndex -> SegmentCount -> Segments -> Segments
sliceSegments index num = Segments . V.slice index num . segmentsOf

takeSegments :: SegmentCount -> Segments -> Segments
takeSegments num = Segments . V.take num . segmentsOf

dropSegments :: SegmentCount -> Segments -> Segments
dropSegments num = Segments . V.drop num . segmentsOf

sliceIndicesFor :: Segments -> [(SampleOffset, SampleCount)]
sliceIndicesFor segments = zip indices lengths
  where
    clipList = toList segments
    lengths = map lengthOf clipList
    indices = scanl' (+) 0 lengths

forEachWithAccum :: Monad m => a -> Segments -> (a -> Segment -> m a) -> m ()
forEachWithAccum a (Segments segments) op = foldM_ op a segments

numSegments :: Segments -> SegmentCount
numSegments = V.length . segmentsOf

numSamplesIn :: Segments -> SampleCount
numSamplesIn = foldr (\a b -> lengthOf a + b) 0 . segmentsOf

-- the number of samples encompassed by all the segments might exceed the max
-- that we can write to a file (i.e. exceed max Int count) so we check here
-- to see if that is the case
numSamplesExceedsMaxAllowed :: Int -> Segments -> Bool
numSamplesExceedsMaxAllowed maxSize = go 0 . segmentsOf
  where
    go sum remaining =
      not (V.null remaining) &&
      (let l = lengthOf (V.head remaining)
       in (maxSize - l < sum) || go (sum+l) (V.tail remaining))

segmentAt :: SegmentIndex -> Segments -> Segment
segmentAt index segments = segmentsOf segments V.! index

modifyFirstAndLast :: Segments -> (Segment -> Segment) -> (Segment -> Segment) -> Segments
modifyFirstAndLast segments modFirstSegment modLastSegment = runST $ do
  v <- V.thaw (segmentsOf segments)
  VM.modify v modFirstSegment 0
  VM.modify v modLastSegment (VM.length v - 1)
  Segments <$> V.unsafeFreeze v
