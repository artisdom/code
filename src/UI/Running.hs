module UI.Running
 ( UIWire
 , UIContext
 , initUI
 , stepUI
 ) where

import Control.Wire hiding ((.),id)


type UISessionStep = Timed Int ()
type UIWire = Wire UISessionStep ()

data UIContext m i o
  = UIContext
    { sessionOf :: !(Session m UISessionStep)
    , uiOf :: !(UIWire m i o)
    }


initUI :: Monad m => UIWire m i o -> UIContext m i o
initUI ui =
  UIContext
  { sessionOf = countSession_ 0
  , uiOf = ui
  }

-- Thanks a bunch to https://stackoverflow.com/users/994206/mokosha
-- for their answer on how to get started with netwire:
-- https://stackoverflow.com/a/25340254/225292
stepUI :: Monad m => i -> UIContext m i o -> m (Either () o, UIContext m i o)
stepUI input uiContext = do
  (step, session') <- stepSession (sessionOf uiContext)
  (output, ui') <- stepWire (uiOf uiContext) step (Right input)
  return (output, UIContext session' ui')
