module UI.LayeredPanel where

import Control.Arrow

import UI.Panel
import UI.Wires


layeredPanel :: (Monad m, Monoid f) => [UIPanel m i o f] -> UIPanel m i o f
layeredPanel [] = arr (const
  UIOutput
  { feedbackOf = mempty
  , generatorOf = const Invisible
  })
layeredPanel [a] = a
layeredPanel as = fanThrough as
