module UI.Panel
 ( UIInput(..)
 , BarPattern(..)
 , UILayout(..)
 , UIOutput(..)
 --
 , PanelContext
 , makePanelContext
 , trackPanelContext
 , updatePanelContext
 --
 , UIControl
 , UIPanel
 , UIScreen
 --
 , inputIsCombinedCtrlKey
 , inputIsKey
 , anyKeyEvent
 , combinedCtrlKeyEvent
 , keyEvent
 , altActiveIn
 , combinedCtrlActiveIn
 , shiftActiveIn
 --
 , inputIsButtonActionInBounds
 , anyButtonEvent
 , buttonInBoundsEvent
 , buttonInSpecificBoundsEvent
 , movementInBoundsEvent
 , movedOutOfBoundsEvent
 , scrollEvent
 , mousePositionIn
 --
 , feedforwardIn
 , userInputIn
 --
 , inputIsInterrupting
 , inputIsNonInterrupting
 , inputInterruptionEvent
 , anyHeldInputIn
 --
 , inputIsResize
 , boundsIn
 ) where

import Control.Monad.Fix
import Control.Wire hiding ((.))
import Control.Wire.Unsafe.Event

import UI.Input
import UI.Primitive
import UI.Running
import UI.Wires


--------------------------------
-- input and output
--------------------------------

data UIInput ff
  = UIInput
    { feedforwardOf :: !ff
    , userInputOf :: !UserInput
    }


data BarPattern
  = Grip
  | NoPattern

data UILayout o
  = Bar
    { boundsOf :: !Rectangle
    , patternOf :: !BarPattern
    }
  | Component
    { boundsOf :: !Rectangle
    , componentOf :: !o
    }
  | Invisible
  | Node
    { leftOf :: !(UILayout o)
    , rightOf :: !(UILayout o)
    }


data UIOutput ff o fb
  = UIOutput
    { feedbackOf :: !fb
    , generatorOf :: !(ff -> UILayout o)
    }

instance Semigroup fb => Semigroup (UIOutput ff o fb) where
  (<>) l r =
    UIOutput
    { feedbackOf = feedbackOf l <> feedbackOf r
    , generatorOf = \ff -> Node (generatorOf l ff) (generatorOf r ff)
    }


--------------------------------
-- state tracking
--------------------------------

data BoundsState
  = BoundsState
    { rectOf :: !Rectangle
    }
  deriving (Eq, Show)


data MetaKeyStates
  = MetaKeyStates
    { altOf :: !Bool
    , cmdOf :: !Bool
    , ctrlOf :: !Bool
    , shiftOf :: !Bool
    }
  deriving (Eq, Show)

defaultMetaKeyStates
  = MetaKeyStates
    { altOf = False
    , cmdOf = False
    , ctrlOf = False
    , shiftOf = False
    }


data MouseState
  = MouseState
    { locationStateOf :: !(Maybe Coordinates)
    , leftButtonOf :: !Bool
    , rightButtonOf :: !Bool
    }
  deriving (Eq, Show)

defaultMouseState
  = MouseState
    { locationStateOf = Nothing
    , leftButtonOf = False
    , rightButtonOf = False
    }


data PanelContext ff
  = PanelContext
    { boundsStateOf :: !BoundsState
    , keyboardStateOf :: !MetaKeyStates
    , mouseStateOf :: !MouseState
    , uiInputOf :: !(UIInput ff)
    }

makePanelContext :: Rectangle -> ff -> PanelContext ff
makePanelContext bounds ff =
  PanelContext
  { boundsStateOf = BoundsState bounds
  , keyboardStateOf = defaultMetaKeyStates
  , mouseStateOf = defaultMouseState
  , uiInputOf = UIInput ff None
  }

trackPanelContext
  :: (Monad m, MonadFix m)
  => PanelContext ff -> Wire s e m (UIInput ff) (PanelContext ff)
trackPanelContext = loopify updatePanelContext

updatePanelContext :: UIInput ff -> PanelContext ff -> PanelContext ff
updatePanelContext input state@(PanelContext b k m _) =
  case userInputOf input of
    Keyboard (Key d MetaAlt) ->
      state' { keyboardStateOf = k { altOf = toBool d } }
    Keyboard (Key d MetaCmd) ->
      state' { keyboardStateOf = k { cmdOf = toBool d } }
    Keyboard (Key d MetaCtrl) ->
      state' { keyboardStateOf = k { ctrlOf = toBool d } }
    Keyboard (Key d MetaShift) ->
      state' { keyboardStateOf = k { shiftOf = toBool d } }
    Mouse (Button d LeftButton l) ->
      state'
      { mouseStateOf = m
        { locationStateOf =
            if inBounds (boundsIn state) l
            then Just l else Nothing
        , leftButtonOf = toBool d
        }
      }
    Mouse (Button d RightButton l) ->
      state'
      { mouseStateOf = m
        { locationStateOf =
            if inBounds (boundsIn state) l
            then Just l else Nothing
        , rightButtonOf = toBool d
        }
      }
    Mouse (Position p) ->
      state'
      { mouseStateOf = m
        { locationStateOf =
            if inBounds (boundsIn state) (positionOf p)
            then Just (positionOf p) else Nothing
        }
      }
    Mouse Lost ->
      state' { mouseStateOf = m { locationStateOf = Nothing } }
    Window (Resize r) ->
      state'
      { boundsStateOf = BoundsState r
      , mouseStateOf = m
        { locationStateOf = locationStateOf m >>= \l ->
            if inBounds r l then Just l else Nothing
        }
      }
    _ -> state'
  where
    state' = state { uiInputOf = input }


--------------------------------
-- panel types
--------------------------------

type UIControl m ff fb = UIWire m (PanelContext ff) fb
type UIPanel m ff o fb = UIWire m (PanelContext ff) (UIOutput ff o fb)
type UIScreen m ff o fb = UIWire m (UIInput ff) (UIOutput ff o fb)



-- NOTES for the conveience methods:
-- Anything with the `inputIs` prefix tests against the input
-- Anything with the `Event` suffix emits an `Event ()` when the input matches
-- Anything with the `In` suffix is a conveience field accessor or aggregator

--------------------------------
-- keyboard convenience
--------------------------------

inputIsCombinedCtrlKey :: Direction -> PanelContext ff -> Bool
inputIsCombinedCtrlKey dir context =
  case userInputOf (uiInputOf context) of
    Keyboard (Key d MetaCmd)  -> d == dir
    Keyboard (Key d MetaCtrl) -> d == dir
    _ -> False

inputIsKey :: KeyName -> Direction -> PanelContext ff -> Bool
inputIsKey key dir context =
  case userInputOf (uiInputOf context) of
    Keyboard (Key d k) -> d == dir && k == key
    _ -> False

--

anyKeyEvent :: PanelContext ff -> Event ()
anyKeyEvent context =
  case userInputOf (uiInputOf context) of
    Keyboard Key{} -> Event ()
    UnspecifiedInput -> Event ()
    _ -> NoEvent

combinedCtrlKeyEvent :: Direction -> PanelContext ff -> Event ()
combinedCtrlKeyEvent dir context =
  case userInputOf (uiInputOf context) of
    Keyboard (Key d MetaCmd)  | d == dir -> Event ()
    Keyboard (Key d MetaCtrl) | d == dir -> Event ()
    _ -> NoEvent

keyEvent :: KeyName -> Direction -> PanelContext ff -> Event ()
keyEvent key dir context =
  case userInputOf (uiInputOf context) of
    Keyboard (Key d k) | d == dir && k == key -> Event ()
    _ -> NoEvent

--

altActiveIn :: PanelContext ff -> Bool
altActiveIn = altOf . keyboardStateOf

combinedCtrlActiveIn :: PanelContext ff -> Bool
combinedCtrlActiveIn context = cmdOf ks || ctrlOf ks
  where ks = keyboardStateOf context

shiftActiveIn :: PanelContext ff -> Bool
shiftActiveIn = shiftOf . keyboardStateOf


--------------------------------
-- mouse convenience
--------------------------------

inputIsButtonActionInBounds :: ButtonName -> Direction -> PanelContext ff -> Bool
inputIsButtonActionInBounds btn dir context =
  case userInputOf (uiInputOf context) of
    Mouse (Button d b c) | d == dir && b == btn -> inBounds (boundsIn context) c
    _ -> False

--

anyButtonEvent :: PanelContext ff -> Event ()
anyButtonEvent context =
  case userInputOf (uiInputOf context) of
    Mouse Button{} -> Event ()
    _ -> NoEvent

buttonInBoundsEvent :: ButtonName -> Direction -> PanelContext ff -> Event ()
buttonInBoundsEvent btn dir context =
  case userInputOf (uiInputOf context) of
    Mouse (Button d b c) | b == btn && d == dir && inBounds bounds c -> Event ()
    _ -> NoEvent
  where
    bounds = boundsIn context

buttonInSpecificBoundsEvent
  :: Rectangle -> ButtonName -> Direction -> PanelContext ff -> Event ()
buttonInSpecificBoundsEvent bnds btn dir context =
  case userInputOf (uiInputOf context) of
    Mouse (Button d b c) | b == btn && d == dir && inBounds bnds c -> Event ()
    _ -> NoEvent

movementInBoundsEvent :: PanelContext ff -> Event ()
movementInBoundsEvent context =
  case userInputOf (uiInputOf context) of
    Mouse (Position (Movement c _)) | inBounds (boundsIn context) c -> Event ()
    _ -> NoEvent

movedOutOfBoundsEvent :: PanelContext ff -> Event ()
movedOutOfBoundsEvent context =
  case userInputOf (uiInputOf context) of
    Mouse (Position (Movement c d)) | inBounds b (c `subOffset` d) && not (inBounds b c) -> Event ()
    _ -> NoEvent
  where
    b = boundsIn context

scrollEvent :: PanelContext ff -> Event ()
scrollEvent context =
  case userInputOf (uiInputOf context) of
    Mouse Scroll{} -> Event ()
    _ -> NoEvent

--

mousePositionIn :: PanelContext ff -> Maybe Coordinates
mousePositionIn = locationStateOf . mouseStateOf


--------------------------------
-- ui input convenience
--------------------------------

feedforwardIn :: PanelContext ff -> ff
feedforwardIn = feedforwardOf . uiInputOf

userInputIn :: PanelContext ff -> UserInput
userInputIn = userInputOf . uiInputOf


--------------------------------
-- user input convenience
--------------------------------

inputIsInterrupting :: PanelContext ff -> Bool
inputIsInterrupting context =
  case userInputOf (uiInputOf context) of
    Keyboard (Key _ KeySpace)  -> False
    Keyboard (Key _ MetaShift) -> False
    Keyboard Key{}   -> True
    UnspecifiedInput -> True
    Mouse Button{}   -> True
    Mouse Lost       -> True
    Window Resize{}  -> True
    _                -> False

inputIsNonInterrupting :: PanelContext ff -> Bool
inputIsNonInterrupting = not . inputIsInterrupting

--

inputInterruptionEvent :: PanelContext ff -> Event ()
inputInterruptionEvent context =
  if inputIsInterrupting context
  then Event ()
  else NoEvent

--

anyHeldInputIn :: PanelContext ff -> Bool
anyHeldInputIn context =
  altOf kb || cmdOf kb || ctrlOf kb || shiftOf kb
  || leftButtonOf m || rightButtonOf m
  where
    kb = keyboardStateOf context
    m = mouseStateOf context


--------------------------------
-- window convenience
--------------------------------

inputIsResize :: PanelContext ff -> Bool
inputIsResize context =
  case userInputOf (uiInputOf context) of
    Window Resize{} -> True
    _ -> False

--

boundsIn :: PanelContext ff -> Rectangle
boundsIn = rectOf . boundsStateOf
