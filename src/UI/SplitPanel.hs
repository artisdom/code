{-# LANGUAGE LambdaCase #-}

module UI.SplitPanel
 ( ResizeBehavior(..)
 , PanelSizeSpec(..)
 , splitPanel
 ) where

import Control.Arrow
import Control.Monad.Fix
import Control.Wire as W hiding ((.))
import Data.Functor

import UI.Input
import UI.Panel
import UI.Primitive
import UI.Running
import UI.Wires


--------------------------------
-- resize bar spec
--------------------------------

data ResizeBehavior
  = SizedBottom
  -- | SizedLeft
  -- | SizedRight
  | SizedTop
  deriving (Eq, Show)

data PanelSizeSpec
  = Fixed
    { behaviorOf :: !ResizeBehavior
    , panelSizeOf :: !Word
    }
  | Expandable
    { behaviorOf :: !ResizeBehavior
    , resizeBarSizeOf :: !Word
    , panelSizeOf :: !Word
    }
  deriving (Eq, Show)


boundsFor :: PanelSizeSpec -> Rectangle -> Rectangle
boundsFor spec (Rectangle (Coordinates x y) (Dimensions w h)) =
  case spec of
    Fixed{} -> Rectangle origin (Dimensions 0 0)
    Expandable{} ->
      case behaviorOf spec of
        SizedBottom ->
          let rbs = fromIntegral (resizeBarSizeOf spec)
              ps = fromIntegral (panelSizeOf spec)
          in Rectangle
              (Coordinates x (y+h-rbs-ps))
              (Dimensions w rbs)
        SizedTop ->
          let rbs = fromIntegral (resizeBarSizeOf spec)
              ps = fromIntegral (panelSizeOf spec)
          in Rectangle
              (Coordinates x (y+ps))
              (Dimensions w rbs)

changeSizeIn :: PanelSizeSpec -> Deltas -> PanelSizeSpec
changeSizeIn spec deltas =
  case behaviorOf spec of
    SizedBottom ->
      spec
      { panelSizeOf =
          fromIntegral (max 0 (fromIntegral (panelSizeOf spec) - deltaYOf deltas))
      }
    SizedTop ->
      spec
      { panelSizeOf =
          fromIntegral (max 0 (fromIntegral (panelSizeOf spec) + deltaYOf deltas))
      }

sizesFor :: PanelSizeSpec -> Rectangle -> ((Rectangle, Rectangle), UILayout o)
sizesFor spec (Rectangle c@(Coordinates x y) (Dimensions w h)) =
  case spec of
    Fixed{} ->
      case behaviorOf spec of
        SizedBottom ->
          ( ( Rectangle c (Dimensions w (h-ps))
            , Rectangle (Coordinates x (y+h-ps)) (Dimensions w ps)
            )
          , Invisible
          )
        SizedTop ->
          ( ( Rectangle c (Dimensions w ps)
            , Rectangle (Coordinates x (y+ps)) (Dimensions w (h-ps))
            )
          , Invisible
          )
    Expandable{} ->
      case behaviorOf spec of
        SizedBottom ->
          ( ( Rectangle c (Dimensions w (h-ps-rbs))
            , Rectangle (Coordinates x (y+h-ps)) (Dimensions w ps)
            )
          , Bar (Rectangle (Coordinates x (y+h-ps-rbs)) (Dimensions w rbs)) Grip
          )
        SizedTop ->
          ( ( Rectangle c (Dimensions w ps)
            , Rectangle (Coordinates x (y+ps+rbs)) (Dimensions w (h-ps-rbs))
            )
          , Bar (Rectangle (Coordinates x (y+ps)) (Dimensions w rbs)) Grip
          )
  where
    ps = fromIntegral (panelSizeOf spec)
    rbs = fromIntegral (resizeBarSizeOf spec)


--------------------------------
-- split panel
--------------------------------

splitPanel
  :: (Monad m, MonadFix m, Semigroup fb)
  => PanelSizeSpec -> UIPanel m ff o fb -> UIPanel m ff o fb
  -> UIPanel m ff o fb
splitPanel spec panel1 panel2 =
  (W.id &&& resizeBar (Idle spec) >>^ adjust)
  >>> (first (panel1 *** panel2 >>> combine) >>^ toOutput)

resizeBar spec = loop (resizeBarIdle >>> delay spec >>^ \a -> (a, a))
-- need `delay` here to give the loop an initial value for the feedback

adjust (context, currentSpec) = ((context1, context2), sb)
  where
    ((r1, r2), sb) = sizesFor (specOf currentSpec) (boundsIn context)
    ff = feedforwardIn context
    userInput = userInputIn context
    resizedContext1 = updatePanelContext (UIInput ff (Window (Resize r1))) context
    resizedContext2 = updatePanelContext (UIInput ff (Window (Resize r2))) context
    (context1, context2) =
      if not (barIsResizing currentSpec || inputIsResize context)
      then ( updatePanelContext (UIInput ff userInput) resizedContext1
           , updatePanelContext (UIInput ff userInput) resizedContext2
           )
      else (resizedContext1, resizedContext2)

toOutput (o, sb) =
  UIOutput
  { feedbackOf = feedbackOf o
  , generatorOf = \ff -> Node (generatorOf o ff) sb
  }


--------------------------------
-- resize bar states
--------------------------------

data Interactivity
  = Idle
    { specOf :: !PanelSizeSpec
    }
  | Resizing
    { specOf :: !PanelSizeSpec
    }

barIsResizing = \case
  Resizing{} -> True
  _ -> False

--

resizeBarIdle :: Monad m => UIWire m (PanelContext ff, Interactivity) Interactivity
resizeBarIdle = mealyState resizeBarIdleOutput resizeBarIdleTransition

resizeBarIdleOutput = Idle . specOf . snd

resizeBarIdleTransition (context, currentSpec) =
  buttonInSpecificBoundsEvent bounds LeftButton Down context $> resizeBarResizing
  where
    bounds = boundsFor (specOf currentSpec) (boundsIn context)

--

resizeBarResizing :: Monad m => UIWire m (PanelContext ff, Interactivity) Interactivity
resizeBarResizing = mealyState resizeBarResizingOutput resizeBarResizingTransition

resizeBarResizingOutput (context, currentSpec) =
  case userInputIn context of
    Mouse (Position (Movement _ deltas)) ->
      Resizing (changeSizeIn (specOf currentSpec) deltas)
    _ -> currentSpec

resizeBarResizingTransition (context, _) =
  inputInterruptionEvent context $> resizeBarIdle
