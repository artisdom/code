module UI.Input where

import UI.Primitive


--------------------------------
-- direction
--------------------------------

data Direction = Down | Up
  deriving (Eq, Show)

toBool Down = True
toBool Up   = False


--------------------------------
-- keyboard
--------------------------------

data KeyName
  = KeyQ
  | KeyS
  | KeyY
  | KeyZ
  | KeyF4
  | KeyEnd
  | KeyHome
  | KeyPageDown
  | KeyPageUp
  | KeySpace
  | MediaAudioNext
  | MediaAudioPrev
  | MediaAudioPlay
  | MediaAudioStop
  | MetaAlt -- AKA Option (on Mac)
  | MetaCmd -- Mac ('Windows' key on Windows)
  | MetaCtrl
  | MetaShift
  deriving (Eq, Show)

data KeyboardInput
  = Key
    { keyDirectionOf :: !Direction
    , keyNameOf :: !KeyName
    }
  deriving (Eq, Show)


--------------------------------
-- mouse
--------------------------------

data ButtonName
  = LeftButton
  | RightButton
  deriving (Eq, Show)

data Movement
  = Movement
    { positionOf :: !Coordinates
    , movementOf :: !Deltas
    }
  deriving (Eq, Show)

data MouseInput
  = Button
    { buttonDirectionOf :: !Direction
    , buttonNameOf :: !ButtonName
    , locationOf :: !Coordinates
    }
  | Position !Movement
  | Scroll
    { amountOf :: !Deltas
    }
  | Lost
  deriving (Eq, Show)


--------------------------------
-- window
--------------------------------

data WindowInput
  -- = CloseRequest
  -- | ContentDisplaying !Bool
  = Resize !Rectangle
  deriving (Eq, Show)


--------------------------------
-- user input
--------------------------------

data UserInput
  = Keyboard !KeyboardInput
  | Mouse !MouseInput
  | Window !WindowInput
  --
  | UnspecifiedInput -- nothing that we're processing but which might interrupt
  | None
  deriving (Eq, Show)
