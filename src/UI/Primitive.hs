{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module UI.Primitive where

import Foreign.Ptr
import Foreign.Storable

import Data.Vector.Unboxed
import Data.Vector.Unboxed.Deriving
import GHC.Generics


--------------------------------
-- Location & Size
--------------------------------

data Coordinates
  = Coordinates
    { xOf :: !Int
    , yOf :: !Int
    }
  deriving (Eq, Generic, Ord, Show)

{-# ANN module "HLint: ignore Use uncurry" #-}
derivingUnbox "Coordinates"
  [t| Coordinates -> (Int, Int)   |]
  [| \(Coordinates x y) -> (x, y) |]
  [| \(x, y) -> Coordinates x y   |]

intAlignment = alignment (0::Int)
intSize = sizeOf (0::Int)

instance Storable Coordinates where
  sizeOf _ = 2 * intSize
  alignment _ = intAlignment
  peek ptr = do
    let xptr = castPtr ptr :: Ptr Int
    x <- peek xptr
    let yptr = plusPtr xptr (sizeOf x)
    y <- peek yptr
    return (Coordinates x y)
  poke ptr (Coordinates x y) = do
    let xptr = castPtr ptr :: Ptr Int
    poke xptr x
    let yptr = plusPtr xptr (sizeOf x)
    poke yptr y


origin = Coordinates 0 0


data Deltas
  = Deltas
    { deltaXOf :: !Int
    , deltaYOf :: !Int
    }
  deriving (Eq, Ord, Show)


zeroDeltas = Deltas 0 0

addOffset :: Coordinates -> Deltas -> Coordinates
addOffset (Coordinates x y) (Deltas dx dy) = Coordinates (x+dx) (y+dy)

subOffset :: Coordinates -> Deltas -> Coordinates
subOffset (Coordinates x y) (Deltas dx dy) = Coordinates (x-dx) (y-dy)


data Dimensions
  = Dimensions
    { widthOf :: !Int
    , heightOf :: !Int
    }
  deriving (Eq, Ord, Show)


--------------------------------
-- Lines
--------------------------------

data Line
  = Line
    { pointAOf :: !Coordinates
    , pointBOf :: !Coordinates
    }
  deriving (Eq, Show)
derivingUnbox "Line"
  [t| Line -> (Int, Int, Int, Int) |]
  [| \(Line (Coordinates x1 y1) (Coordinates x2 y2)) -> (x1, y1, x2, y2) |]
  [| \(x1, y1, x2, y2) -> Line (Coordinates x1 y1) (Coordinates x2 y2) |]

newtype Polyline
  = Polyline
    { pointsOf :: Vector Coordinates
    }
  deriving (Eq, Show)


--------------------------------
-- Rectangle
--------------------------------

data Rectangle
  = Rectangle
    { coordinatesOf :: !Coordinates
    , dimensionsOf :: !Dimensions
    }
  deriving (Eq, Show)


constrictBy :: Int -> Rectangle -> Rectangle
constrictBy n (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates (x+n) (y+n)) (Dimensions (w-2*n) (h-2*n))

doubleRect :: Rectangle -> Rectangle
doubleRect (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates (x*2) (y*2)) (Dimensions (w*2) (h*2))

inBounds :: Rectangle -> Coordinates -> Bool
inBounds (Rectangle (Coordinates bx by) (Dimensions bw bh)) (Coordinates cx cy) =
  bx <= cx && cx < bx+bw && by <= cy && cy < by+bh

toXIn :: Rectangle -> Int -> Int -> Int
toXIn (Rectangle (Coordinates x _) (Dimensions w _)) offset span =
  round (fromIntegral offset / fromIntegral span * fromIntegral w) + x
{-# INLINE toXIn #-}

ratio :: Integral a => a -> a -> Float
ratio num denom = fromIntegral num / fromIntegral denom

ratioX :: Coordinates -> Rectangle -> Float
ratioX (Coordinates cx _) (Rectangle (Coordinates bx _) (Dimensions bw _)) =
  fromIntegral (cx - bx) / fromIntegral bw

rectToPoly :: Rectangle -> Polyline
rectToPoly (Rectangle (Coordinates x y) (Dimensions w h)) =
  Polyline $ fromList
    [ Coordinates x y
    , Coordinates (x+w-1) y
    , Coordinates (x+w-1) (y+h-1)
    , Coordinates x (y+h-1)
    , Coordinates x y
    ]
