module UI.Wires where

import Control.Arrow
import Control.Monad.Fix
import Control.Wire
import Control.Wire.Unsafe.Event
import Data.List


combine :: (Arrow a, Semigroup o) => a (o, o) o
combine = arr (uncurry (<>))

fanThrough :: (Arrow a, Semigroup o) => [a i o] -> a i o
fanThrough = foldr1 (\a b -> a &&& b >>> combine)

loopify :: (Monad m, MonadFix m) => (i -> o -> o) -> o -> Wire s e m i o
loopify f d = loop (uncurry f ^>> delay d >>^ \a -> (a, a))
-- need `delay` here to give the loop an initial value for the feedback

mealyState :: (Monad m) => (i -> o) -> (i -> Event (Wire s e m i o)) -> Wire s e m i o
mealyState outf transf = dSwitch (arr (\i -> (outf i, transf i)))

transitionTo = Event
noTransition = NoEvent

chooseTransition :: [Event (Wire s e m i o)] -> Event (Wire s e m i o)
chooseTransition = foldl' mergeL NoEvent
