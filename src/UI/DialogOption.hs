module UI.DialogOption where


data DialogBinding = AcceptBinding | NoBinding | RejectBinding
  deriving (Show)

data DialogOption
  = DialogOption
    { bindingOf :: !DialogBinding
    , idOf :: !Int
    , textOf :: !String
    }
  deriving (Show)
type DialogOptions = [DialogOption]


data YesNo = Yes | No
  deriving (Eq, Enum, Show)

yesOption =
  DialogOption
  { bindingOf = AcceptBinding
  , idOf = fromEnum Yes
  , textOf = show Yes
  }

noOption =
  DialogOption
  { bindingOf = RejectBinding
  , idOf = fromEnum No
  , textOf = show No
  }

yesNo = [yesOption, noOption]


data OkCancel = Ok | Cancel
  deriving (Eq, Enum, Show)

okOption =
  DialogOption
  { bindingOf = AcceptBinding
  , idOf = fromEnum Ok
  , textOf = show Ok
  }

cancelOption =
  DialogOption
  { bindingOf = RejectBinding
  , idOf = fromEnum Cancel
  , textOf = show Cancel
  }

okCancel = [okOption, cancelOption]
