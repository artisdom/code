{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module View.StateMachine
 ( ViewArgs(..)
 , view
 ) where

import Control.Monad.Reader
import Data.Char
import Data.List
import qualified Data.Text as Text
import qualified Data.Vector as Vector
import Pipes
import Pipes.Core
import Pipes.Lift
import SDL hiding (get)
import qualified SDL.Font as Font
import qualified SDL.Image as Image
import System.FilePath

import Config
import Presenter.Input as Presenter
import Presenter.Output as Presenter
import View.Internal.Assets
import View.Internal.Context
import View.Internal.Dialog
import View.Internal.Event
import View.Internal.Layout
import View.Internal.SDLShim


type ViewState = Client Presenter.Input Presenter.Output
type ViewStateWithContext m = ViewState (WithContext m)

class
  ( HasSettings r
  , HasStyle r
  , HasProgramName r
  , MonadIO m
  , MonadReader r m
  ) => ViewStateConstraint m r where {}
instance
  ( HasSettings r
  , HasStyle r
  , HasProgramName r
  , MonadIO m
  , MonadReader r m
  ) => ViewStateConstraint m r where {}


data ViewArgs
  = ViewArgs
    { filePathOf :: !(Maybe FilePath)
    , messagesOf :: ![String]
    }


view
  :: ViewStateConstraint m r
  => ViewArgs -> ViewState m ()
view args = do
  initialize [InitAudio,InitVideo,InitEvents]
  Font.initialize
  Image.initialize [Image.InitPNG]
  --
  res <- initializeAudioDriver
  let messages =
        case res of
          Left m -> m : messagesOf args
          Right _ -> messagesOf args
  --
  context <- makeContext
  -- initialize lower layer(s)
  windowSize <- getDimensions (windowOf context)
  _ <- request (Initialize windowSize)
  --
  mapM_ (notify "Issue during initialization") messages
  event <- txGuiEvent =<< toGuiDropEvent (filePathOf args)
  runWithContext context (running event)

initializeAudioDriver
  :: ViewStateConstraint m r
  => ViewState m (Either String ())
initializeAudioDriver = do
  driverName <- getAudioDriverName
  case driverName of
    Just name -> do
      let configuredDriver = Text.pack name
      driver <- Vector.find ((== configuredDriver) . audioDriverName) <$> getAudioDrivers
      case driver of
        Just d -> do
          audioInit d
          return $ Right ()
        Nothing ->
          return . Left $ concat ["Error initializing audio driver \"", name, "\"."]
    Nothing -> return $ Right ()

running
  :: ViewStateConstraint m r
  => Presenter.Input -> ViewStateWithContext m ()
running event = do
  ticks1 <- ticks
  response <- request event
  processPresenterResponse response
  --
  ticks2 <- ticks
  let diff = ticks2 - ticks1
  let fpsDelay = 16
  when (diff < fpsDelay) (delay (fpsDelay - diff))
  --
  event' <- txGuiEvent =<< pollEvent
  case event' of
    FileDrop p | isStyleFile p -> do
      (style, messages) <- readStyleFile p
      if null messages
      then useNewStyle style (lift updateWithNewStyle >> running (Presenter.RebuildUI style))
      else do
        mapM_ (notify "Error reading new style file") messages
        running Presenter.Render
    _ -> running event'

processPresenterResponse
  :: ViewStateConstraint m r
  => Presenter.Output -> ViewStateWithContext m ()
processPresenterResponse = \case
    Presenter.Display layout windowTitle -> lift do
      updateHighDPIFlag
      setWindowTitle windowTitle
      displayLayout layout
    Alert summary details ->
      alert summary details
    Notify summary details ->
      notify summary details
    Prompt summary details options -> do
      selection <- prompt summary details options
      response <- request (PromptResponse selection)
      processPresenterResponse response
    UseRepresentation waveformRepresentation -> do
      lift (setWaveformRepresentation waveformRepresentation)
      response <- request Presenter.Render
      processPresenterResponse response
    Quit -> do
      lift destroyContext
      Font.quit
      Image.quit
      quit

isStyleFile :: FilePath -> Bool
isStyleFile = isSuffixOf "style.yaml" . map toLower . takeFileName
