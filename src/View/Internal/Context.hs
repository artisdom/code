{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE Rank2Types #-}

module View.Internal.Context
 ( Context(..)
 , WithContext
 , makeContext
 , destroyContext
 , runWithContext
 , setWaveformRepresentation
 , getWaveformRepresentation
 , setWindowTitle
 , readHighDPIFlag
 , updateHighDPIFlag
 , updatePathTexture
 , getTextures
 , getTexture
 , updateWithNewStyle
 , displayTEXT
 , widthOfTEXT
 ) where

import Control.Monad.Reader
import Control.Monad.State.Strict
import Data.List
import qualified Data.Map as Map
import qualified Data.Text as Text
import Pipes.Core
import Pipes.Lift
import qualified Data.StateVar as SV
import SDL hiding (get)
import qualified SDL

import Config
import Display
import qualified UI
import View.Internal.Assets
import View.Internal.SDLShim as Shim


data Context
  = Context
    { assetsOf :: !Assets
    , highDPIOf :: !Bool
    , pathOf :: !(Maybe FilePath)
    , rendererOf :: !Renderer
    , waveformRepresentationOf :: !WaveformRepresentation
    , windowOf :: !Window
    }

type WithContext = StateT Context


makeContext
  :: (MonadIO m, MonadReader r m, HasStyle r, HasProgramName r)
  => m Context
makeContext = do
  programName <- getNameOfProgram
  let windowProperties = defaultWindow { windowResizable = True, windowHighDPI = True }
  window <- createWindow (Text.pack programName) windowProperties
  windowMinimumSize window $= V2 400 300
  renderer <- createRenderer window (-1) defaultRenderer
  rendererDrawBlendMode renderer $= BlendAlphaBlend
  assets <- buildAssets renderer
  useAssetsIconIn window assets
  return Context
    { assetsOf = assets
    , highDPIOf = False
    , pathOf = Nothing
    , rendererOf = renderer
    , waveformRepresentationOf = mempty
    , windowOf = window
    }

destroyContext :: (MonadIO m) => WithContext m ()
destroyContext = do
  context <- get
  destroyRenderer (rendererOf context)
  destroyWindow (windowOf context)
  destroyAssets (assetsOf context)

runWithContext
  :: (MonadIO m)
  => Context -> Proxy a' a b' b (WithContext m) () -> Proxy a' a b' b m ()
runWithContext = evalStateP

setWaveformRepresentation :: (Monad m) => WaveformRepresentation -> WithContext m ()
setWaveformRepresentation waveRep = do
  context <- get
  put (context { waveformRepresentationOf = waveRep })

getWaveformRepresentation :: (Monad m) => WithContext m WaveformRepresentation
getWaveformRepresentation = gets waveformRepresentationOf

setWindowTitle :: (MonadIO m) => String -> WithContext m ()
setWindowTitle title = do
  titleVar <- gets (windowTitle . windowOf)
  titleVar $= Text.pack title

readHighDPIFlag :: (MonadIO m) => WithContext m Bool
readHighDPIFlag = gets highDPIOf

updateHighDPIFlag :: (MonadIO m) => WithContext m ()
updateHighDPIFlag = do
  window <- gets windowOf
  renderer <- gets rendererOf
  ws <- SDL.get (windowSize window)
  vp <- SDL.get (rendererViewport renderer)
  case vp of
    Nothing -> modify' (\s -> s { highDPIOf = False })
    Just (SDL.Rectangle _ dims) -> modify' (\s -> s { highDPIOf = ws /= dims })

updatePathTexture
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => FilePath -> WithContext m ()
updatePathTexture filePath = do
  context <- get
  let oldPath = pathOf context
  when (oldPath /= Just filePath) $ do
    put context { pathOf = Just filePath }
    rebuildPathTexture filePath

rebuildPathTexture
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => FilePath -> WithContext m ()
rebuildPathTexture path = do
  style <- asks getStyle
  let definition =
        TEXTureDefinition
        { keyOf = LoadScreenFilePath
        , textOf = path
        , fontOf = LoadScreen
        , foregroundColorOf = loadScreenTextColorOf style
        , backgroundColorOf = Just $ loadScreenBackgroundColorOf style
        }
  context <- get
  assets' <- rebuildTEXTureAsset (rendererOf context) definition (assetsOf context)
  put context { assetsOf = assets' }

getTextures :: Monad m => WithContext m Textures
getTextures = do
  highDPI <- gets highDPIOf
  gets (getDPITextures highDPI . assetsOf)

getTexture :: Monad m => TextureName -> WithContext m Texture
getTexture name = (Map.! name) <$> getTextures

updateWithNewStyle
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => WithContext m ()
updateWithNewStyle = do
  context <- get
  assets <- rebuildAssets (rendererOf context) (assetsOf context)
  put context { assetsOf = assets }
  forM_ (pathOf context) rebuildPathTexture


--------------------------------
-- Text Helpers
--------------------------------

displayTEXT
  :: MonadIO m
  => Renderer -> UI.Coordinates -> TEXT -> WithContext m ()
displayTEXT renderer coords text = do
  textures <- getTextures
  displayTEXTures renderer coords textures text

widthOfTEXT :: MonadIO m => TEXT -> WithContext m Int
widthOfTEXT text = do
  textures <- getTextures
  widthOfTEXTures textures text

