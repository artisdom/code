{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}

module View.Internal.Layout where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.State.Strict
import SDL

import Config
import Display
import UI
import View.Internal.Assets
import View.Internal.Context
import View.Internal.Object
import View.Internal.SDLShim


displayLayout
  :: (MonadIO m, MonadReader r m, HasSettings r, HasStyle r)
  => UILayout Object -> WithContext m ()
displayLayout layout = do
  renderer <- gets rendererOf
  displayLayout' layout
  present renderer

displayLayout'
  :: (MonadIO m, MonadReader r m, HasSettings r, HasStyle r)
  => UILayout Object -> WithContext m ()
displayLayout' = \case
  Bar bounds pattern ->
    displayBar bounds pattern
  Component bounds@(UI.Rectangle _ (Dimensions w h)) component ->
    when (w > 0 && h > 0) do
      highDPI <- readHighDPIFlag
      (if highDPI then doubleRect bounds else bounds) `displayObject` component
  Invisible ->
    return ()
  Node l r -> do
    displayLayout' l
    displayLayout' r

displayBar
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => UI.Rectangle -> UI.BarPattern -> WithContext m ()
displayBar bar@(UI.Rectangle _ (Dimensions w h)) pattern = do
  when (w > 0 && h > 0) do
    highDPI <- readHighDPIFlag
    let bar' = if highDPI then doubleRect bar else bar
    renderer <- gets rendererOf
    case pattern of
      Grip -> do
        color <- asks (editScreenResizeBarBackgroundColorOf . getStyle)
        draw renderer (color, bar')
        texture <- getTexture EditScreenResizeBarPattern
        tilePattern renderer texture bar'
      NoPattern -> do
        color <- asks (editScreenResizeBarBackgroundColorOf . getStyle)
        draw renderer (color, bar')
