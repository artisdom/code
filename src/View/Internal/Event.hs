{-# LANGUAGE BlockArguments #-}

module View.Internal.Event
 ( toGuiDropEvent
 , txGuiEvent
 ) where

import Control.Monad
import Control.Monad.IO.Class
import Foreign.C.String
import SDL

import Presenter.Input as Presenter
import UI


toGuiDropEvent :: (MonadIO m) => Maybe FilePath -> m (Maybe SDL.Event)
toGuiDropEvent (Just path) = do
  cstr <- liftIO (newCString path)
  return (Just . Event 0 . DropEvent . DropEventData $ cstr)
toGuiDropEvent Nothing = return Nothing

txGuiEvent :: (MonadIO m) => Maybe SDL.Event -> m Presenter.Input
txGuiEvent event =
  case event of
    Nothing -> return Render
    Just e ->
      case eventPayload e of
        -- keyboard
        KeyboardEvent e ->
          case txKeyboardEvent e of
            Just k  -> return k
            Nothing -> return Render
        -- mouse
        MouseButtonEvent e -> return (txMouseButtonEvent e)
        MouseMotionEvent e -> return (txMouseMotionEvent e)
        MouseWheelEvent e -> return (txMouseWheelEvent e)
        -- window
        DropEvent e -> txDropEvent e
        WindowClosedEvent e -> return CloseRequest
        WindowSizeChangedEvent e -> return (txWindowSizeChangedEvent e)
        WindowShownEvent e -> return (ContentDisplaying True)
        WindowHiddenEvent e -> return (ContentDisplaying False)
        WindowExposedEvent e -> return (ContentDisplaying True)
        WindowMinimizedEvent e -> return (ContentDisplaying False)
        WindowMaximizedEvent e -> return (ContentDisplaying True)
        WindowRestoredEvent e -> return (ContentDisplaying True)
        WindowGainedMouseFocusEvent e -> return (Presenter.Interactivity UI.UnspecifiedInput)
        WindowLostMouseFocusEvent e -> return (Presenter.Interactivity (UI.Mouse UI.Lost))
        -- default
        _ -> return Render

txKeyboardEvent :: KeyboardEventData -> Maybe Presenter.Input
txKeyboardEvent e =
  let direction = if keyboardEventKeyMotion e == Pressed then UI.Down else UI.Up
      repeat = keyboardEventRepeat e
      scancode = keysymKeycode (keyboardEventKeysym e)
  in if repeat then Nothing else Just $ Presenter.Interactivity
    case scancode of
      KeycodeAudioNext -> UI.Keyboard (UI.Key direction MediaAudioNext)
      KeycodeAudioPrev -> UI.Keyboard (UI.Key direction MediaAudioPrev)
      KeycodeAudioPlay -> UI.Keyboard (UI.Key direction MediaAudioPlay)
      KeycodeAudioStop -> UI.Keyboard (UI.Key direction MediaAudioStop)
      KeycodeQ -> UI.Keyboard (UI.Key direction KeyQ)
      KeycodeS -> UI.Keyboard (UI.Key direction KeyS)
      KeycodeY -> UI.Keyboard (UI.Key direction KeyY)
      KeycodeZ -> UI.Keyboard (UI.Key direction KeyZ)
      KeycodeSpace -> UI.Keyboard (UI.Key direction KeySpace)
      KeycodeF4 -> UI.Keyboard (UI.Key direction KeyF4)
      KeycodeHome -> UI.Keyboard (UI.Key direction KeyHome)
      KeycodeEnd -> UI.Keyboard (UI.Key direction KeyEnd)
      KeycodePageUp -> UI.Keyboard (UI.Key direction KeyPageUp)
      KeycodePageDown -> UI.Keyboard (UI.Key direction KeyPageDown)
      KeycodeLAlt -> UI.Keyboard (UI.Key direction MetaAlt)
      KeycodeRAlt -> UI.Keyboard (UI.Key direction MetaAlt)
      KeycodeLGUI -> UI.Keyboard (UI.Key direction MetaCmd)
      KeycodeRGUI -> UI.Keyboard (UI.Key direction MetaCmd)
      KeycodeLCtrl -> UI.Keyboard (UI.Key direction MetaCtrl)
      KeycodeRCtrl -> UI.Keyboard (UI.Key direction MetaCtrl)
      KeycodeLShift -> UI.Keyboard (UI.Key direction MetaShift)
      KeycodeRShift -> UI.Keyboard (UI.Key direction MetaShift)
      _ -> UI.UnspecifiedInput

txMouseButtonEvent :: MouseButtonEventData -> Presenter.Input
txMouseButtonEvent e =
  let direction = if mouseButtonEventMotion e == Pressed then UI.Down else UI.Up
      button = mouseButtonEventButton e
      P (V2 x y) = mouseButtonEventPos e
      coords = Coordinates { xOf = fromIntegral x, yOf = fromIntegral y }
  in Presenter.Interactivity case button of
      ButtonLeft  -> UI.Mouse (UI.Button direction LeftButton coords)
      ButtonRight -> UI.Mouse (UI.Button direction RightButton coords)
      _ -> UI.UnspecifiedInput

txMouseMotionEvent :: MouseMotionEventData -> Presenter.Input
txMouseMotionEvent e =
  let P (V2 x y) = mouseMotionEventPos e
      V2 dx dy = mouseMotionEventRelMotion e
  in Presenter.Interactivity . UI.Mouse . UI.Position $ Movement
    { positionOf = Coordinates { xOf = fromIntegral x, yOf = fromIntegral y }
    , movementOf = Deltas { deltaXOf = fromIntegral dx, deltaYOf = fromIntegral dy }
    }

txMouseWheelEvent :: MouseWheelEventData -> Presenter.Input
txMouseWheelEvent e =
  let V2 x y = mouseWheelEventPos e
      coords = Deltas { deltaXOf = fromIntegral x, deltaYOf = fromIntegral y }
      invCoords = Deltas { deltaXOf = -1 * fromIntegral x, deltaYOf = -1 * fromIntegral y }
  in Presenter.Interactivity
    case mouseWheelEventDirection e of
      ScrollNormal  -> UI.Mouse (UI.Scroll { UI.amountOf = coords })
      ScrollFlipped -> UI.Mouse (UI.Scroll { UI.amountOf = invCoords })

txDropEvent :: (MonadIO m) => DropEventData -> m Presenter.Input
txDropEvent e = do
  let cString = dropEventFile e
  filePath <- liftIO (peekCString cString)
  return (FileDrop filePath)

txWindowSizeChangedEvent :: WindowSizeChangedEventData -> Presenter.Input
txWindowSizeChangedEvent e =
  let V2 width height = windowSizeChangedEventSize e
  in Presenter.Resize Dimensions
    { widthOf = fromIntegral width
    , heightOf = fromIntegral height
    }
