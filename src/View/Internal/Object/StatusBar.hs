module View.Internal.Object.StatusBar where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.Trans.State.Strict
import SDL

import Audio as A
import Config
import Display
import UI
import View.Internal.Assets
import View.Internal.Context
import View.Internal.SDLShim


displayStatusBar
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => UI.Rectangle -> StatusData -> WithContext m ()
displayStatusBar bounds@(UI.Rectangle (Coordinates x y) _) statusData = do
  highDPI <- readHighDPIFlag
  let dpiDiv = if highDPI then 1 else 2
  let px = x + (14 `div` dpiDiv)
  let py = y + (10 `div` dpiDiv)
  renderer <- gets rendererOf
  bgColor <- asks (editScreenStatusBarBackgroundColorOf . getStyle)
  draw renderer (bgColor, bounds)
  displayTEXT renderer (Coordinates px py)
    [ EditScreenStatusBarPlaybackStatusLabel
    , EditScreenStatusBarNameValueSeparator
    , if playbackStatusOf statusData
      then EditScreenStatusBarPlaying
      else EditScreenStatusBarStopped
    ]
  displayTEXT renderer (Coordinates (px+(250 `div` dpiDiv)) py) $
    EditScreenStatusBarFileStatusLabel :
    EditScreenStatusBarNameValueSeparator :
    case saveStatusOf statusData of
      Saved       -> [EditScreenStatusBarSaved]
      SaveErrored -> [EditScreenStatusBarSaveErrored]
      Saving p    ->
        map EditScreenStatusBarChar (show (floor (p*100))) ++
        [ EditScreenStatusBarChar '%'
        , EditScreenStatusBarChar ' '
        , EditScreenStatusBarSaved
        ]
      Unsaved     -> [EditScreenStatusBarUnsaved]
  displayTEXT renderer (Coordinates (px+(550 `div` dpiDiv)) py) $
    EditScreenStatusBarSampleRateLabel :
    EditScreenStatusBarNameValueSeparator :
    map EditScreenStatusBarChar (show (sampleRateValueOf statusData))
    ++ [EditScreenStatusBarHertz]
  displayTEXT renderer (Coordinates (px+(850 `div` dpiDiv)) py)
    [ EditScreenStatusBarSampleTypeLabel
    , EditScreenStatusBarNameValueSeparator
    , EditScreenStatusBarSampleType (sampleTypeValueOf statusData)
    ]
