{-# LANGUAGE BlockArguments #-}

module View.Internal.Object.Waveform
 ( displayWaveform
 ) where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.Trans.State.Strict
import Data.List
import qualified Data.Vector.Unboxed as VU
import Data.Word
import SDL

import Audio
import Config
import Display
import qualified UI
import View.Internal.Context
import View.Internal.SDLShim


data ClipState
  = ClipActive
  | ClipHover
  | ClipNeutral
  | ClipRemoving
  deriving (Eq)

data GapState
  = GapNeutral
  | GapRemoving
  deriving (Eq)


clipStateFor interactivity segment sampleOffset =
  let pair = (segment, sampleOffset)
  in case interactivity of
    Hovering p ->
      if inSegment p pair then ClipHover else ClipNeutral
    Activating p ->
      if inSegment p pair then ClipActive else ClipNeutral
    RemovingBetween p1 p2 ->
      if inSegment p1 pair || inSpan (p1, p2) pair
      then ClipRemoving else ClipNeutral
    _ -> ClipNeutral

inSegment :: SampleOffset -> (Segment, SampleOffset) -> Bool
inSegment pos (segment, start) = start <= pos && pos < start + lengthOf segment
{-# INLINE inSegment #-}

inSpan :: (SampleOffset, SampleOffset) -> (Segment, SampleOffset) -> Bool
inSpan (p1, p2) pair@(segment, start) = p1 < start && start <= p2
{-# INLINE inSpan #-}

-- This code was optimized in the following ways:
-- - The loops were modified so that they run in the IO monad instead of the
--   full monad stack, as the machinery for monads and lifting to IO appear to
--   be expensive when done a lot
-- - Calls to the GPU are expensive, so the render color is set only when a
--   color change has occurred (instead of setting it each loop iteration)
displayWaveform
  :: (MonadIO m, MonadReader r m, HasSettings r, HasStyle r)
  => UI.Rectangle -> WaveformData -> WithContext m ()
displayWaveform bounds waveformData = do
  renderer <- gets rendererOf
  style <- asks getStyle
  let gapBackgroundColorFor state =
        case state of
          GapNeutral  -> toV4 (editScreenGapNeutralColorOf style)
          GapRemoving -> toV4 (editScreenGapRemovingColorOf style)
  let clipBackgroundColorFor state =
        case state of
          ClipActive   -> toV4 (editScreenClipActiveBackgroundColorOf style)
          ClipHover    -> toV4 (editScreenClipHoverBackgroundColorOf style)
          ClipNeutral  -> toV4 (editScreenClipNeutralBackgroundColorOf style)
          ClipRemoving -> toV4 (editScreenClipRemovingBackgroundColorOf style)
  let clipForegroundColorFor state =
        case state of
          ClipActive   -> toV4 (editScreenClipActiveForegroundColorOf style)
          ClipHover    -> toV4 (editScreenClipHoverForegroundColorOf style)
          ClipNeutral  -> toV4 (editScreenClipNeutralForegroundColorOf style)
          ClipRemoving -> toV4 (editScreenClipRemovingForegroundColorOf style)
  -- drawing
  let WaveformData
        { interactivityOf = interactivity
        , segmentsOf = segments
        , viewSpanOf = span
        } = waveformData
  let (UI.Rectangle (UI.Coordinates _ y) (UI.Dimensions _ height)) = bounds
  -- dead space
  let color = editScreenDeadSpaceColorOf style
  draw renderer (color, bounds)
  -- draw gaps
  -- since the gaps are going to be covered by the clips, we can draw just two
  -- rectangles, max, to cover all the gaps:
  --  - one for the regular gap background
  --  - one for the highlighted gap background, if any
  liftIO do
    -- neutral
    rendererDrawColor renderer $= gapBackgroundColorFor GapNeutral
    let segment = InsertedGap (numSamplesIn segments)
    let neutralBounds = drawingBounds bounds segment 0 span y height
    draw renderer neutralBounds
    -- highlighted
    case interactivity of
      RemovingBetween p1 p2 -> do
        let initCursor = cursorAtFirstSample segments
        let p2Cursor = advanceCursorInBounds p2 initCursor segments
        let endCursor = moveToEndOfSegment p2Cursor segments
        let segment = InsertedGap $ numSamplesBetween initCursor endCursor segments - p1
        let removalBounds = drawingBounds bounds segment p1 span y height
        rendererDrawColor renderer $= gapBackgroundColorFor GapRemoving
        draw renderer removalBounds
      Truncating p -> do
        let initCursor = cursorAtFirstSample segments
        let midCursor = advanceCursorInBounds p initCursor segments
        let endCursor = moveToEndOfSegment midCursor segments
        let segment = InsertedGap $ numSamplesBetween initCursor endCursor segments - p
        let removalBounds = drawingBounds bounds segment p span y height
        rendererDrawColor renderer $= gapBackgroundColorFor GapRemoving
        draw renderer removalBounds
      _ -> return ()
  -- draw clip backgrounds
  let initClipState = ClipNeutral
  rendererDrawColor renderer $= clipBackgroundColorFor initClipState
  liftIO $ forEachWithAccum (0, initClipState) segments $
    \(sampleOffset, lastState) segment -> do
      let clipBounds = drawingBounds bounds segment sampleOffset span y height
      nextState <- case segment of
        Clip{} -> do
          let state = clipStateFor interactivity segment sampleOffset
          when (state /= lastState) $
            rendererDrawColor renderer $= clipBackgroundColorFor state
          draw renderer clipBounds
          return state
        _ -> return lastState
      return (sampleOffset + lengthOf segment, nextState)
  -- draw clip foregrounds
  waveformRepresentation <- getWaveformRepresentation
  -- the following values were moved out of the `renderClipContent` function
  -- to help optimize it (since they're the same in each invocation)
  let halfHeight = fromIntegral height / 2.0
  let scale sample = round (halfHeight * (1 - sample)) + y
  --
  rendererDrawColor renderer $= clipForegroundColorFor initClipState
  liftIO $ forEachWithAccum (0, initClipState) segments $
    \(sampleOffset, lastState) segment -> do
      let clipBounds = drawingBounds bounds segment sampleOffset span y height
      nextState <- case segment of
        Clip{} -> do
          let state = clipStateFor interactivity segment sampleOffset
          when (state /= lastState) $
            rendererDrawColor renderer $= clipForegroundColorFor state
          renderClipContent renderer waveformRepresentation segment clipBounds scale
          return state
        _ -> return lastState
      return (sampleOffset + lengthOf segment, nextState)

drawingBounds bounds segment sampleOffset span y height =
  let x = UI.toXIn bounds sampleOffset span
      width = max 1 $ UI.toXIn bounds (sampleOffset + lengthOf segment) span - x
      coords = UI.Coordinates { UI.xOf = x, UI.yOf = y }
      dims = UI.Dimensions { UI.widthOf = width, UI.heightOf = height }
  in UI.Rectangle coords dims
{-# INLINE drawingBounds #-}

renderClipContent
  :: MonadIO m
  => Renderer -> WaveformRepresentation -> Segment -> UI.Rectangle -> (Float -> Int)
  -> m ()
renderClipContent renderer waveformRepresentation clip bounds renderY =
  let width = UI.widthOf (UI.dimensionsOf bounds)
  in case clipRepresentationFor clip width waveformRepresentation of
    Samples samples -> do
      let renderX s = UI.toXIn bounds s (VU.length samples)
      draw renderer $ UI.Polyline $
        VU.imap (\k v -> UI.Coordinates (renderX k) (renderY v)) samples
    Extremes getMax getMin -> do
      forM_ [0..width-1] $ \dx ->
        let x = fromIntegral $ UI.xOf (UI.coordinatesOf bounds) + dx
            pMin = P (V2 x (fromIntegral (renderY (getMin dx))))
            pMax = P (V2 x (fromIntegral (renderY (getMax dx))))
        in drawLine renderer pMin pMax
