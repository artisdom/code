{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE Rank2Types #-}

module View.Internal.Object.ProgressBar
 ( displayLoadProgress
 ) where

import Control.Lens
import Control.Monad.Reader
import Control.Monad.State.Strict
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Unboxed as VU
import SDL hiding (origin, Rectangle)

import Config
import Display
import UI
import View.Internal.Assets hiding (backgroundColorOf)
import View.Internal.Context
import View.Internal.SDLShim as SDLShim


displayLoadProgress
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => Rectangle -> LoadProgressData -> WithContext m ()
displayLoadProgress bounds loadProgressData = do
  highDPI <- readHighDPIFlag
  let dpiDiv = if highDPI then 1 else 2
  renderer <- gets rendererOf
  let (LoadProgressData clipsProgress path fileProgress visualsProgress) = loadProgressData
  updatePathTexture path
  --
  bgColor <- asks (loadScreenBackgroundColorOf . getStyle)
  draw renderer (bgColor, bounds)
  --
  texture <- getTexture LoadScreenFileBarGraphic
  bar <- makeProgressBar highDPI texture fileProgress
  positioned <- positionInCenter bounds bar
  draw renderer positioned
  --
  let coords = SDLShim.positionOf positioned
  dims <- getDimensions bar
  let barSpacing = 10 `div` dpiDiv
  let textSpacing = 10 `div` dpiDiv
  --
  texture <- getTexture LoadScreenClipsBarGraphic
  bar <- makeProgressBar highDPI texture clipsProgress
  positioned <- positionInCenter bounds bar <&> moveBy (0, heightOf dims + barSpacing)
  draw renderer positioned
  --
  texture <- getTexture LoadScreenVisualsBarGraphic
  bar <- makeProgressBar highDPI texture visualsProgress
  positioned <- positionInCenter bounds bar <&> moveBy (0, 2 * (heightOf dims + barSpacing))
  draw renderer positioned
  --
  texture <- getTexture LoadScreenLoadingText
  Dimensions _ height <- getDimensions texture
  let positioned = moveBy (0, -3 * height - textSpacing) (Positioned coords texture)
  draw renderer positioned
  --
  texture <- getTexture LoadScreenFilePath
  Dimensions _ height <- getDimensions texture
  let positioned = moveBy (0, -2 * height - textSpacing) (Positioned coords texture)
  draw renderer positioned
  --
  let textureName =
        case (fileProgress, clipsProgress, visualsProgress) of
          (1,1,_) -> LoadScreenVisualsStatusText
          (1,_,_) -> LoadScreenClipsStatusText
          _       -> LoadScreenFileStatusText
  texture <- getTexture textureName
  let positioned = moveBy (0, -1 * height - textSpacing) (Positioned coords texture)
  draw renderer positioned


data ProgressBar
  = ProgressBar
    { backgroundColorOf :: !Color
    , barTextureOf :: !Texture
    , borderSizeOf :: !Int
    , borderColorOf :: !Color
    , paddingSizeOf :: !Int
    , progressOf :: !Float
    }

instance HasDimensions ProgressBar where
  getDimensions progressBar = do
    Dimensions width height <- getDimensions (barTextureOf progressBar)
    let borderContribution = 2 * borderSizeOf progressBar
    let paddingContribution = 2 * paddingSizeOf progressBar
    return Dimensions
      { widthOf = width + borderContribution + paddingContribution
      , heightOf = height + borderContribution + paddingContribution
      }


makeProgressBar
  :: (Monad m, MonadReader r m, HasStyle r)
  => Bool -> Texture -> Float -> m ProgressBar
makeProgressBar highDPI texture progress = do
  let dpiMul = if highDPI then 2 else 1
  style <- asks getStyle
  let backgroundColor =
        if | progress == 0 -> loadScreenProgressBarInactiveBackgroundColorOf style
           | progress == 1 -> loadScreenProgressBarCompleteBackgroundColorOf style
           | otherwise     -> loadScreenProgressBarActiveBackgroundColorOf style
  let borderColor =
        if | progress == 0 -> loadScreenProgressBarInactiveBorderColorOf style
           | progress == 1 -> loadScreenProgressBarCompleteBorderColorOf style
           | otherwise     -> loadScreenProgressBarActiveBorderColorOf style
  let padding = dpiMul *
        if | progress == 0 -> loadScreenProgressBarInactivePaddingSizeOf style
           | progress == 1 -> loadScreenProgressBarCompletePaddingSizeOf style
           | otherwise     -> loadScreenProgressBarActivePaddingSizeOf style
  return ProgressBar
    { backgroundColorOf = backgroundColor
    , barTextureOf = texture
    , borderSizeOf = 1
    , borderColorOf = borderColor
    , paddingSizeOf = fromIntegral padding
    , progressOf = progress
    }

instance Drawable (Positioned ProgressBar) where
  draw renderer (Positioned coords progressBar) = do
    let Coordinates x y = coords
    dims@(Dimensions width height) <- getDimensions progressBar
    let ProgressBar bgColor bar border borderColor padding progress = progressBar
    --
    let bgRect = UI.Rectangle coords dims
    draw renderer (bgColor, bgRect)
    --
    (Dimensions texWidth texHeight) <- getDimensions bar
    let texWidth' = ceiling $ fromIntegral texWidth * progress
    let subCoords = Coordinates 0 0
    let subDims = Dimensions texWidth' texHeight
    let barCoords = Coordinates (x + border + padding) (y + border + padding)
    let portion = Portion (UI.Rectangle subCoords subDims) bar
    draw renderer (Positioned barCoords portion)
    --
    let innerAdjustment = 2 * border + 2 * padding
    let leftOuterX = x
    let leftInnerX = leftOuterX + innerAdjustment
    let rightOuterX = x + width - 1
    let rightInnerX = rightOuterX - innerAdjustment
    let upperY = y
    let lowerY = upperY + height - 1
    if progress == 0
    then do
      draw renderer (borderColor, Polyline $ VU.fromList
        [ Coordinates leftInnerX upperY
        , Coordinates leftOuterX upperY
        , Coordinates leftOuterX lowerY
        , Coordinates leftInnerX lowerY
        ])
      draw renderer (borderColor, Polyline $ VU.fromList
        [ Coordinates rightInnerX upperY
        , Coordinates rightOuterX upperY
        , Coordinates rightOuterX lowerY
        , Coordinates rightInnerX lowerY
        ])
    else
      draw renderer (borderColor, rectToPoly bgRect)
