{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}

module View.Internal.Object.TimeAxis
 ( displayTimeAxis
 ) where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.Trans.State.Strict
import Text.Printf (printf)
import SDL

import Config
import Display
import UI
import View.Internal.Assets
import View.Internal.Context
import View.Internal.SDLShim


displayTimeAxis
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => UI.Rectangle -> TimeAxisData -> WithContext m ()
displayTimeAxis bounds timeAxisData = do
  highDPI <- readHighDPIFlag
  let dpiDiv = if highDPI then 1 else 2
  renderer <- gets rendererOf
  let b = beginningSampleOf timeAxisData
  let e = endingSampleOf timeAxisData
  let num = numSamplesInViewOf timeAxisData
  let rate = sampleRateOf timeAxisData
  style <- asks getStyle
  let bgColor = editScreenAxisBackgroundColorOf style
  let borderColor = editScreenAxisBorderColorOf style
  let axis = timeAxisLine b e num bounds
  draw renderer (bgColor, bounds)
  draw renderer (borderColor, axis)
  --
  let UI.Rectangle (Coordinates x y) (Dimensions w _) = bounds
  (textWidth, showMilli, labelInterval) <- calcLabelInterval bounds num rate
  let labelPositions = calcLabelPositions labelInterval b e
  let interstitialPositions = calcInterstitialPositions labelInterval b e
  --
  -- optimization: liftIO
  --
  liftIO $ forM_ interstitialPositions $ \i -> do
    let x = sampleToX bounds b num i
    let shortTickHeight = 6 `div` dpiDiv
    let tick = tickLine x y shortTickHeight
    draw renderer (borderColor, tick)
  textures <- getTextures
  liftIO $ forM_ labelPositions $ \i -> do
    let x = sampleToX bounds b num i
    let longTickHeight = 12 `div` dpiDiv
    let tick = tickLine x y longTickHeight
    let textCoords = Coordinates (x - textWidth `div` 2) (y+longTickHeight)
    let timeStamp = samplesToTimeStamp rate i
    draw renderer (borderColor, tick)
    displayTEXTures renderer textCoords textures (timeStampToTEXT showMilli timeStamp)

calcLabelInterval (UI.Rectangle _ (Dimensions w _)) num rate = do
  let spanTimeStamp = samplesToTimeStamp rate num
  textWidth <- widthOfTEXT (timeStampToTEXT False spanTimeStamp)
  let maxNumLabels = max 1 $ (w `div` textWidth) - 1
  let roughInterval = num `div` maxNumLabels
  let adjust = timeStampToSamples rate . adjustInterval . samplesToTimeStamp rate . (*2)
  if roughInterval > rate
  then return (textWidth, False, adjust roughInterval)
  else do
    textWidthMs <- widthOfTEXT (timeStampToTEXT True spanTimeStamp)
    let maxNumLabels = max 1 $ (w `div` textWidthMs) - 1
    let roughInterval = num `div` maxNumLabels
    let timeInterval = adjust roughInterval
    if timeInterval < rate
    then return (textWidthMs, True, timeInterval)
    else return (textWidth, False, timeInterval)

calcLabelPositions labelInterval beginningSample endingSample =
  -- start displaying before the view cursor position so we have a smooth
  -- scroll-in/out of the label text
  let firstLabelPosition = beginningSample - (beginningSample `mod` labelInterval)
  in  -- display after the end of the view so we have a smooth scroll-in/out
      -- of the label text, as well as a handy marker after the end of the
      -- audio when expanding the trailing gap
      takeWhile (< endingSample + labelInterval) -- adding the interval adds an extra label
      [ firstLabelPosition
      , firstLabelPosition + labelInterval
      ..]

{-# ANN module "HLint: ignore Use even" #-}
calcInterstitialPositions labelInterval beginningSample endingSample =
  let interstitialInterval =
        if | labelInterval `mod` 4 == 0 -> labelInterval `div` 4
           | labelInterval `mod` 2 == 0 -> labelInterval `div` 2
           | otherwise                  -> labelInterval
      firstInterstitialPosition =
        beginningSample + interstitialInterval - (beginningSample `mod` interstitialInterval)
  in  takeWhile (< endingSample)
      [ firstInterstitialPosition
      , firstInterstitialPosition + interstitialInterval
      ..]


data TimeStamp
  = TimeStamp
    { hoursOf :: !Int
    , minutesOf :: !Int
    , secondsOf :: !Int
    , millisecondsOf :: !Int
    }

defaultTimeStamp
  = TimeStamp
    { hoursOf = 0
    , minutesOf = 0
    , secondsOf = 0
    , millisecondsOf = 0
    }

samplesToTimeStamp :: Int -> Int -> TimeStamp
samplesToTimeStamp sampleRate samples =
  TimeStamp
  { hoursOf = hours
  , minutesOf = minutes
  , secondsOf = seconds
  , millisecondsOf = milliseconds
  }
  where
    milliseconds = samples * 1000 `div` sampleRate `mod` 1000
    totalSeconds = samples `div` sampleRate
    seconds = totalSeconds `mod` 60
    totalMinutes = totalSeconds `div` 60
    minutes = totalMinutes `mod` 60
    hours = totalMinutes `div` 60

timeStampToSamples :: Int -> TimeStamp -> Int
timeStampToSamples sampleRate (TimeStamp h m s l) =
  (h * 3600 + m * 60 + s) * sampleRate + (l * sampleRate `div` 1000)

adjustInterval :: TimeStamp -> TimeStamp
adjustInterval (TimeStamp 0 0 0 l) =
  defaultTimeStamp { millisecondsOf = nearestDivisionMs l }
adjustInterval (TimeStamp 0 0 s _) =
  defaultTimeStamp { secondsOf = nearestDivision s }
adjustInterval (TimeStamp 0 m _ _) =
  defaultTimeStamp { minutesOf = nearestDivision m }
adjustInterval (TimeStamp h _ _ _) =
  defaultTimeStamp { hoursOf = nearestDivision h }

nearestDivisionMs v
  | v < 5    = max 1 v
  | v < 10   = 10
  | v < 25   = 25
  | v < 50   = 50
  | v < 100  = 100
  | v < 250  = 250
  | v < 500  = 500
  | v < 1000 = 1000

nearestDivision v
  | v < 5  = v
  | v < 10 = 10
  | v < 15 = 15
  | v < 20 = 20
  | v < 30 = 30
  | v < 60 = 60

timeStampToTEXT showMilli (TimeStamp h m s l) =
  map AxisText $
  if showMilli
  then toTwoDigits h . (':':) . toTwoDigits m . (':':) . toTwoDigits s . ('.':) $ toThreeDigits l []
  else toTwoDigits h . (':':) . toTwoDigits m . (':':) $ toTwoDigits s []

toTwoDigits n = (numToDigit (n `div` 10 `mod` 10) :) . (numToDigit (mod n 10) :)

toThreeDigits n = (numToDigit (n `div` 100 `mod` 10) :) . toTwoDigits n

numToDigit = \case
  0 -> '0'
  1 -> '1'
  2 -> '2'
  3 -> '3'
  4 -> '4'
  5 -> '5'
  6 -> '6'
  7 -> '7'
  8 -> '8'
  9 -> '9'
  _ -> '?'

timeAxisLine beginning end total bounds@(UI.Rectangle (UI.Coordinates x y) _) =
  Line (Coordinates x y) (Coordinates (sampleToX bounds beginning total end) y)

tickLine x y h = Line (Coordinates x y) (Coordinates x (y+h))

sampleToX (UI.Rectangle (Coordinates x _) (Dimensions w _)) start num sample =
  (sample - start) * w `div` num + x
