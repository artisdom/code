module View.Internal.Dialog
 ( alert
 , notify
 , prompt
 ) where

import Control.Monad.IO.Class
import qualified Data.Text as Text
import Data.Word
import Foreign
import Foreign.C.String
import SDL
import SDL.Raw.Enum
import SDL.Raw.Types
import SDL.Raw.Video hiding (showSimpleMessageBox)

import UI


alert :: (MonadIO m) => String -> String -> m ()
alert title message = liftIO $
  showSimpleMessageBox Nothing Error (Text.pack title) (Text.pack message)

notify :: (MonadIO m) => String -> String -> m ()
notify title message = liftIO $
  showSimpleMessageBox Nothing Information (Text.pack title) (Text.pack message)

prompt :: (MonadIO m) => String -> String -> DialogOptions -> m (Maybe Int)
prompt title message options = liftIO $
  withCString title   $ \t ->
    withCString message $ \m ->
      withOptions options $ \i o ->
        alloca $ \pmbd ->
          alloca $ \pselection -> do
            let mbd =
                  MessageBoxData
                  { messageBoxDataFlags = SDL_MESSAGEBOX_WARNING
                  , messageBoxDataWindow = nullPtr
                  , messageBoxDataTitle = t
                  , messageBoxDataMessage = m
                  , messageBoxDataNumButtons = fromIntegral i
                  , messageBoxDataButtons = o
                  , messageBoxDataColorScheme = nullPtr
                  }
            poke pmbd mbd
            res <- showMessageBox pmbd pselection
            if res < 0          -- check for failure first
            then return Nothing -- failure is being coalesced with no selection
            else do
              selection <- fromIntegral <$> peek pselection
              if selection < 0  -- check for no selection
              then return Nothing
              else return (Just selection)

withOptions :: DialogOptions -> (Int -> Ptr MessageBoxButtonData -> IO b) -> IO b
withOptions options op =
  withCStrings (map textOf options) $
    flip withArrayLen op . zipWith toMessageBoxButtonData options

withCStrings = withCStringsAccum id

withCStringsAccum :: ([CString] -> [CString]) -> [String] -> ([CString] -> IO b) -> IO b
withCStringsAccum diffList []     op = op (diffList [])
withCStringsAccum diffList (s:ss) op =
  withCString s $ \cstr ->
    withCStringsAccum (diffList . (cstr:)) ss op

toMessageBoxButtonData option text =
  MessageBoxButtonData
  { messageBoxButtonDataFlags = toDataFlag (bindingOf option)
  , messageBoxButtonButtonID = fromIntegral (idOf option)
  , messageBoxButtonText = text
  }

toDataFlag :: DialogBinding -> Word32
toDataFlag binding =
  case binding of
    AcceptBinding -> SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT
    NoBinding     -> 0
    RejectBinding -> SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT
