{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

module View.Internal.Playback
 ( openDevice
 , closeDevice
 , startDevice
 , stopDevice
 , toggleDevice
 , isPlaying
 ) where

import Control.Concurrent
import Control.Monad.IO.Class
import Control.Monad.Reader
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Storable.Mutable as VSM
import Data.Word
import Foreign.C.Types
import SDL.Audio

import Audio
import Config


data PlaybackContext a
  = PlaybackContext
    { audioDeviceOf :: !(MVar AudioDevice)
    , audioSourceOf :: !(MVar a)
    , sampleCountOf :: !Int
    }


openDevice
  :: (AudioData a, HasSettings r, MonadIO m, MonadReader r m, PlaybackSource a)
  => MVar a -> m AudioDevice
openDevice ref = do
  deviceRef <- liftIO newEmptyMVar
  sampleCount <- getPlaybackBufferSampleCount
  let context = PlaybackContext
        { audioDeviceOf = deviceRef
        , audioSourceOf = ref
        , sampleCountOf = sampleCount
        }
  requestedSpec <- makeDeviceSpecFor context
  (device, actualSpec) <- openAudioDevice requestedSpec
  liftIO (putMVar deviceRef device)
  -- TODO compare actualSpec to requested spec
  -- error if unable to work with mismatch
  --liftIO . putStrLn . ("freq: " ++ ) . show . audioSpecFreq $ actualSpec
  --liftIO . putStrLn $ strSampleFromat actualSpec
  --liftIO . putStrLn . ("num chan: " ++) . show . audioSpecChannels $ actualSpec
  --case audioSpecFormat actualSpec of
  --  Unsigned8BitAudio -> liftIO (putStrLn "unsigned 8-bit")
  --  Signed16BitNativeAudio -> liftIO (putStrLn "signed 16-bit")
  --  Signed32BitNativeAudio -> liftIO (putStrLn "signed 32-bit")
  --  FloatingNativeAudio -> liftIO (putStrLn "float 32-bit")
  --  _ -> liftIO (putStrLn "unhandled sample format type")
  --liftIO . putStrLn . ("num samples: " ++) . show . audioSpecSamples $ actualSpec
  --liftIO . putStrLn . ("buff size (bytes): " ++) . show . audioSpecSize $ actualSpec
  --
  return device

makeDeviceSpecFor
  :: (AudioData a, MonadIO m, PlaybackSource a)
  => PlaybackContext a -> m OpenDeviceSpec
makeDeviceSpecFor context = do
  audioFile <- liftIO (readMVar (audioSourceOf context))
  case sampleTypeOf audioFile of
    Audio.Word8 -> makeDeviceSpecForSampleType context Unsigned8BitAudio
    Audio.Int16 -> makeDeviceSpecForSampleType context Signed16BitNativeAudio
    Audio.Int24 -> makeDeviceSpecForSampleType context FloatingNativeAudio -- special case
    Audio.Int32 -> makeDeviceSpecForSampleType context Signed32BitNativeAudio
    Audio.Float -> makeDeviceSpecForSampleType context FloatingNativeAudio

makeDeviceSpecForSampleType
  :: (AudioData a, MonadIO m, PlaybackSource a)
  => PlaybackContext a -> forall s. AudioFormat s -> m OpenDeviceSpec
makeDeviceSpecForSampleType context format = do
  audioFile <- liftIO (readMVar (audioSourceOf context))
  let sampleRate = (CInt . fromIntegral . sampleRateOf) audioFile
  return $
    OpenDeviceSpec
    { openDeviceFreq = Mandate sampleRate
    , openDeviceFormat = Mandate format
    , openDeviceChannels = Mandate Mono
    , openDeviceSamples = fromIntegral (sampleCountOf context)
    , openDeviceCallback = \sampleType vec ->
        -- even though 'Native' is requested, we might be given 'LE'
        case (sampleTypeOf audioFile, sampleType) of
          (Audio.Word8, Unsigned8BitAudio     ) -> fillWithContent Word8Content vec context
          (Audio.Int16, Signed16BitLEAudio    ) -> fillWithContent Int16Content vec context
          (Audio.Int16, Signed16BitNativeAudio) -> fillWithContent Int16Content vec context
          (Audio.Int24, FloatingLEAudio       ) -> fillWithContent FloatContent vec context
          (Audio.Int24, FloatingNativeAudio   ) -> fillWithContent FloatContent vec context
          (Audio.Int32, Signed32BitLEAudio    ) -> fillWithContent Int32Content vec context
          (Audio.Int32, Signed32BitNativeAudio) -> fillWithContent Int32Content vec context
          (Audio.Float, FloatingLEAudio       ) -> fillWithContent FloatContent vec context
          (Audio.Float, FloatingNativeAudio   ) -> fillWithContent FloatContent vec context
          _ -> return ()
    , openDeviceUsage = ForPlayback
    , openDeviceName = Nothing
    }

fillWithContent
  :: forall a s . (AudioData a, PlaybackSource a, VSM.Storable s)
  => ExpectedContent s
  -> VSM.IOVector s
  -> PlaybackContext a
  -> IO ()
fillWithContent contentType vec config = do
  let audioSource = audioSourceOf config
  audio <- takeMVar audioSource
  device <- readMVar (audioDeviceOf config)
  let sampleCount = sampleCountOf config
  when (playbackSuspended audio) (stopDevice device)
  let numChannels = VSM.length vec `div` sampleCount
  let clips = getSegmentsToPlay sampleCount audio
  let indices = sliceIndicesFor clips
  channelData <- VSM.new sampleCount
  forM_ (zip (toList clips) indices) $ \(clip, (start, num)) -> do
    let c = VSM.slice start num channelData
    fillWith audio clip contentType (VS.copy c)
  {-  if numChannels == 1
  then VSM.copy vec channelData
  else-}
  interleaveChannels channelData numChannels vec
  let audio' = recordSamplesPlayed sampleCount audio
  putMVar audioSource audio'

interleaveChannels
  :: VSM.Storable a
  => VSM.IOVector a -> Int -> VSM.IOVector a -> IO ()
interleaveChannels source numChannels target = do
  let tl = VSM.length target
  sequence_
    [VSM.read source (i `div` numChannels) >>= VSM.write target i
    | i <- [0..(tl-1)] ]

closeDevice :: (MonadIO m) => AudioDevice -> m ()
closeDevice = liftIO . closeAudioDevice

startDevice :: (MonadIO m) => AudioDevice -> m ()
startDevice = flip setAudioDevicePlaybackState Play

stopDevice :: (MonadIO m) => AudioDevice -> m ()
stopDevice = flip setAudioDevicePlaybackState Pause

toggleDevice :: (MonadIO m) => AudioDevice -> m ()
toggleDevice device = do
  status <- audioDeviceStatus device
  case status of
    Playing -> stopDevice device
    Paused -> startDevice device
    Stopped -> return () -- probably closing, so don't do anything

isPlaying :: (MonadIO m) => AudioDevice -> m Bool
isPlaying = fmap (== Playing) . audioDeviceStatus
