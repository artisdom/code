{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}

module View.Internal.Object where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.State.Strict
import Data.Functor
import Data.List
import Data.StateVar as SV

import Config
import Display
import UI
import View.Internal.Assets
import View.Internal.Context
import View.Internal.Object.ProgressBar
import View.Internal.Object.StatusBar
import View.Internal.Object.TimeAxis
import View.Internal.Object.Waveform
import View.Internal.SDLShim


displayObject
  :: (MonadIO m, MonadReader r m, HasSettings r, HasStyle r)
  => Rectangle -> Object -> WithContext m ()
displayObject bounds = \case
  Cursor cursorData ->
    displayCursor bounds cursorData
  Icon iconData ->
    displayIcon bounds iconData
  Instructions ->
    displayInstructions bounds
  LoadProgress loadProgressData ->
    displayLoadProgress bounds loadProgressData
  Status statusData ->
    displayStatusBar bounds statusData
  TimeAxis timeAxisData ->
    displayTimeAxis bounds timeAxisData
  ViewWindow viewWindowData ->
    displayViewWindow bounds viewWindowData
  Waveform waveformData ->
    displayWaveform bounds waveformData

displayCursor
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => Rectangle -> CursorData -> WithContext m ()
displayCursor bounds cursorData = do
  let viewStart = cursorViewStartOf cursorData
  let viewLen = cursorViewSpanOf cursorData
  let position = cursorPositionOf cursorData
  let height = heightOf (dimensionsOf bounds)
  let y = yOf (coordinatesOf bounds)
  let x = toXIn bounds (position - viewStart) viewLen
  let line = Line (Coordinates x y) (Coordinates x (y + height))
  color <- colorForCursor (cursorTypeOf cursorData)
  renderer <- gets rendererOf
  draw renderer (color, line)
  highDPI <- readHighDPIFlag
  -- double up when we're at a high DPI to make it more visible
  when highDPI do
    let line' = Line (Coordinates (x-1) y) (Coordinates (x-1) (y + height))
    draw renderer (color, line')

colorForCursor
  :: (MonadReader r m, HasStyle r)
  => CursorType -> WithContext m Color
colorForCursor cursorType = do
  style <- asks getStyle
  return $
    case cursorType of
      MarkingCursor ->
        editScreenMarkingCursorColorOf style
      PlaybackCursor PlaybackCursorActive ->
        editScreenPlaybackCursorActiveColorOf style
      PlaybackCursor PlaybackCursorIdle ->
        editScreenPlaybackCursorIdleColorOf style
      PrecisionCursor PrecisionCursorActivating ->
        editScreenPrecisionCursorActivatingColorOf style
      PrecisionCursor PrecisionCursorIdle ->
        editScreenPrecisionCursorIdleColorOf style
      PrecisionCursor PrecisionCursorModifying ->
        editScreenPrecisionCursorModifyingColorOf style

displayIcon
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => Rectangle -> IconData -> WithContext m ()
displayIcon (Rectangle c@(Coordinates x y) _) iconData = do
  let state = stateOf iconData
  texture <- case nameOf iconData of
    ConfigIcon -> getTexture (EditScreenConfigIcon state)
    RedoIcon -> getTexture (EditScreenRedoIcon state)
    SaveIcon -> getTexture (EditScreenSaveIcon state)
    UndoIcon -> getTexture (EditScreenUndoIcon state)
  let positioned = Positioned c texture
  renderer <- gets rendererOf
  draw renderer positioned
  return ()

displayInstructions
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => Rectangle -> WithContext m ()
displayInstructions bounds = do
  renderer <- gets rendererOf
  color <- asks (instructionsScreenBackgroundColorOf . getStyle)
  draw renderer (color, bounds)
  --
  texture <- getTexture InstructionsScreen
  placed <- positionInCenter bounds texture
  draw renderer placed

displayViewWindow
  :: (MonadIO m, MonadReader r m, HasStyle r)
  => Rectangle -> ViewWindowData -> WithContext m ()
displayViewWindow bounds viewWindowData = do
  renderer <- gets rendererOf
  style <- asks getStyle
  let state = viewWindowStateOf viewWindowData
  let viewPosition = windowStartOf viewWindowData
  let viewSpan = windowSpanOf viewWindowData
  let numSamples = fullSpanOf viewWindowData
  let x = toXIn bounds viewPosition numSamples
  let w = max 1 $ toXIn bounds (viewPosition + viewSpan) numSamples - x
  let Rectangle (Coordinates _ by) (Dimensions _ bh) = bounds
  let rect = Rectangle (Coordinates x by) (Dimensions w bh)
  let backgroundColor =
        case state of
          ViewWindowActive  -> editScreenViewWindowActiveBackgroundColorOf style
          ViewWindowHover   -> editScreenViewWindowHoverBackgroundColorOf style
          ViewWindowNeutral -> editScreenViewWindowNeutralBackgroundColorOf style
  draw renderer (backgroundColor, rect)
  let borderColor =
        case state of
          ViewWindowActive  -> editScreenViewWindowActiveBorderColorOf style
          ViewWindowHover   -> editScreenViewWindowHoverBorderColorOf style
          ViewWindowNeutral -> editScreenViewWindowNeutralBorderColorOf style
  draw renderer (borderColor, rectToPoly rect)
  -- double up when we're at a high DPI to make it more visible
  highDPI <- readHighDPIFlag
  when highDPI do
    let rect' = constrictBy 1 rect
    draw renderer (borderColor, rectToPoly rect')
