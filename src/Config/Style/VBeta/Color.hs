module Config.Style.VBeta.Color where

import Data.Aeson
import qualified Data.Text as Text

import Display


instance ToJSON Color where
  toJSON = String . Text.pack . show

instance FromJSON Color where
  parseJSON = withText "Color" $ \t ->
    let unpacked = Text.unpack t in
    case parseColor unpacked of
      Right c -> return c
      Left e -> fail ("Could not parse color: " ++ show e)
