{-# LANGUAGE TemplateHaskell #-}

module Config.Style.VBeta.EditScreen where

import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Text.Casing

import Config.Style.VBeta.Color
import Display


--------------------------------
-- Clip Styles
--------------------------------

data ClipStyle
  = ClipStyle
    { _clipBackgroundColor :: !Color
    , _clipForegroundColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 5
  , omitNothingFields = True
  }
  ''ClipStyle)
$(makeLenses ''ClipStyle)

defaultClipStyle =
  ClipStyle
  { _clipBackgroundColor = gray 34
  , _clipForegroundColor = Color 0 176 0 255
  }


data ClipStyles
  = ClipStyles
    { _clipActive :: !ClipStyle
    , _clipHover :: !ClipStyle
    , _clipNeutral :: !ClipStyle
    , _clipRemoving :: !ClipStyle
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 5
  , omitNothingFields = True
  }
  ''ClipStyles)
$(makeLenses ''ClipStyles)

defaultClipStyles =
  ClipStyles
  { _clipActive =
      defaultClipStyle
      { _clipBackgroundColor = gray 46
      , _clipForegroundColor = Color 0 255 0 255
      }
  , _clipHover =
      defaultClipStyle
      { _clipBackgroundColor = gray 46
      , _clipForegroundColor = Color 0 192 0 255
      }
  , _clipNeutral = defaultClipStyle
  , _clipRemoving =
      defaultClipStyle
      { _clipBackgroundColor = gray 46
      , _clipForegroundColor = gray 128
      }
  }


--------------------------------
-- Gap Styles
--------------------------------

data GapStyle
  = GapStyle
    { _gapBackgroundColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 4
  , omitNothingFields = True
  }
  ''GapStyle)
$(makeLenses ''GapStyle)

defaultGapStyle =
  GapStyle
  { _gapBackgroundColor = gray 64
  }


data GapStyles
  = GapStyles
    { _gapNeutral :: !GapStyle
    , _gapRemoving :: !GapStyle
    , _gapResizing :: !GapStyle
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 4
  , omitNothingFields = True
  }
  ''GapStyles)
$(makeLenses ''GapStyles)

defaultGapStyles =
  GapStyles
  { _gapNeutral = defaultGapStyle
  , _gapRemoving = defaultGapStyle { _gapBackgroundColor = gray 128 }
  , _gapResizing = defaultGapStyle { _gapBackgroundColor = Color 128 128 136 255 }
  }


--------------------------------
-- Cursor Styles
--------------------------------

data PlaybackCursorStyles
  = PlaybackCursorStyles
    { _playbackCursorActiveColor :: !Color
    , _playbackCursorIdleColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 15
  , omitNothingFields = True
  }
  ''PlaybackCursorStyles)
$(makeLenses ''PlaybackCursorStyles)

defaultPlaybackCursorStyles =
  PlaybackCursorStyles
  { _playbackCursorActiveColor = white
  , _playbackCursorIdleColor = gray 224
  }


data PrecisionCursorStyles
  = PrecisionCursorStyles
    { _precisionCursorActivatingColor :: !Color
    , _precisionCursorIdleColor :: !Color
    , _precisionCursorModifyingColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 16
  , omitNothingFields = True
  }
  ''PrecisionCursorStyles)
$(makeLenses ''PrecisionCursorStyles)

defaultPrecisionCursorStyles =
  PrecisionCursorStyles
  { _precisionCursorActivatingColor = orange
  , _precisionCursorIdleColor = magenta
  , _precisionCursorModifyingColor = purple
  }


--------------------------------
-- Axes and Units
--------------------------------

data AxisStyle
  = AxisStyle
    { _axisBackgroundColor :: !Color
    , _axisBorderColor :: !Color
    , _axisTextColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 5
  , omitNothingFields = True
  }
  ''AxisStyle)
$(makeLenses ''AxisStyle)

defaultAxisStyle =
  AxisStyle
  { _axisBackgroundColor = gray 64
  , _axisBorderColor = white
  , _axisTextColor = white
  }


--------------------------------
-- Resize Bar
--------------------------------

data ResizeBarStyle
  = ResizeBarStyle
    { _resizeBarBackgroundColor :: !Color
    , _resizeBarSize :: !Word
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 10
  , omitNothingFields = True
  }
  ''ResizeBarStyle)
$(makeLenses ''ResizeBarStyle)

defaultResizeBarStyle =
  ResizeBarStyle
  { _resizeBarBackgroundColor = gray 64
  , _resizeBarSize = 10
  }


--------------------------------
-- Status Bar
--------------------------------

data StatusBarStyle
  = StatusBarStyle
    { _statusBarBackgroundColor :: !Color
    , _statusBarTextColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 10
  , omitNothingFields = True
  }
  ''StatusBarStyle)
$(makeLenses ''StatusBarStyle)

defaultStatusBarStyle =
  StatusBarStyle
  { _statusBarBackgroundColor = gray 34
  , _statusBarTextColor = gray 170
  }


--------------------------------
-- View Window
--------------------------------

data ViewWindowStyle
  = ViewWindowStyle
    { _viewWindowBackgroundColor :: !Color
    , _viewWindowBorderColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 11
  , omitNothingFields = True
  }
  ''ViewWindowStyle)
$(makeLenses ''ViewWindowStyle)

defaultViewWindowStyle =
  ViewWindowStyle
  { _viewWindowBackgroundColor = Color 255 255 255 64
  , _viewWindowBorderColor = white
  }


data ViewWindowStyles
  = ViewWindowStyles
    { _viewWindowActive :: !ViewWindowStyle
    , _viewWindowHover :: !ViewWindowStyle
    , _viewWindowNeutral :: !ViewWindowStyle
    , _viewWindowZooming :: !ViewWindowStyle
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 11
  , omitNothingFields = True
  }
  ''ViewWindowStyles)
$(makeLenses ''ViewWindowStyles)

activeViewWindowBorderColor = Color 255 192 64 255

defaultViewWindowStyles =
  ViewWindowStyles
  { _viewWindowActive =
      ViewWindowStyle
      { _viewWindowBackgroundColor = invisible
      , _viewWindowBorderColor = activeViewWindowBorderColor
      }
  , _viewWindowHover =
      defaultViewWindowStyle
      { _viewWindowBorderColor = activeViewWindowBorderColor
      }
  , _viewWindowNeutral = defaultViewWindowStyle
  , _viewWindowZooming =
      ViewWindowStyle
      { _viewWindowBackgroundColor = invisible
      , _viewWindowBorderColor = activeViewWindowBorderColor
      }
  }


--------------------------------
-- Edit Screen Styles
--------------------------------

data EditScreenStyle
  = EditScreenStyle
    { _editScreenClipStyles :: !ClipStyles
    , _editScreenDeadSpaceColor :: !Color
    , _editScreenGapStyles :: !GapStyles
    , _editScreenMarkingCursorColor :: !Color
    , _editScreenNavigationBarHeight :: !Word
    , _editScreenPlaybackCursor :: !PlaybackCursorStyles
    , _editScreenPrecisionCursor :: !PrecisionCursorStyles
    , _editScreenAxis :: !AxisStyle
    , _editScreenResizeBar :: !ResizeBarStyle
    , _editScreenStatusBar :: !StatusBarStyle
    , _editScreenViewWindow :: !ViewWindowStyles
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 11
  , omitNothingFields = True
  }
  ''EditScreenStyle)
$(makeLenses ''EditScreenStyle)

defaultEditScreenStyle =
  EditScreenStyle
  { _editScreenClipStyles = defaultClipStyles
  , _editScreenDeadSpaceColor = black
  , _editScreenGapStyles = defaultGapStyles
  , _editScreenMarkingCursorColor = red
  , _editScreenNavigationBarHeight = 65
  , _editScreenPlaybackCursor = defaultPlaybackCursorStyles
  , _editScreenPrecisionCursor = defaultPrecisionCursorStyles
  , _editScreenAxis = defaultAxisStyle
  , _editScreenResizeBar = defaultResizeBarStyle
  , _editScreenStatusBar = defaultStatusBarStyle
  , _editScreenViewWindow = defaultViewWindowStyles
  }
