module Display.Object where

import qualified Data.Vector.Unboxed as VU

import Audio
import Display.Waveform
import UI


data Object
  = Cursor !CursorData
  | Icon !IconData
  | Instructions
  | LoadProgress !LoadProgressData
  | Status !StatusData
  | TimeAxis !TimeAxisData
  | ViewWindow !ViewWindowData
  | Waveform !WaveformData


--------------------------------
-- Cursors
--------------------------------

data CursorData
  = CursorData
    { cursorTypeOf :: !CursorType
    , cursorViewSpanOf :: !SampleCount
    , cursorViewStartOf :: !SampleIndex
    , cursorPositionOf :: !SampleIndex
    }

data CursorType
  = MarkingCursor
  | PlaybackCursor !PlaybackCursorState
  | PrecisionCursor !PrecisionCursorState

data PlaybackCursorState
  = PlaybackCursorIdle
  | PlaybackCursorActive

data PrecisionCursorState
  = PrecisionCursorActivating
  | PrecisionCursorIdle
  | PrecisionCursorModifying


--------------------------------
-- Load Progress
--------------------------------

data LoadProgressData
  = LoadProgressData
    { clipsProgressOf :: !Float
    , filePathOf :: !FilePath
    , fileProgressOf :: !Float
    , visualsProgressOf :: !Float
    }


--------------------------------
-- Status
--------------------------------

data StatusData
  = StatusData
    { playbackStatusOf :: !Bool
    , sampleRateValueOf :: !Int
    , sampleTypeValueOf :: !SampleType
    , saveStatusOf :: !SaveStatus
    }

data SaveStatus
  = Saved
  | SaveErrored
  | Saving !Float
  | Unsaved


--------------------------------
-- Time Axis
--------------------------------

data TimeAxisData
  = TimeAxisData
    { beginningSampleOf :: !SampleOffset
    , endingSampleOf :: !SampleOffset
    , numSamplesInViewOf :: !SampleOffset
    , sampleRateOf :: !Int
    }


--------------------------------
-- Toolbar
--------------------------------

data ButtonState
  = Active
  | Disabled
  | Enabled
  | Hover
  deriving (Eq, Ord)

data IconName
  = ConfigIcon
  | RedoIcon
  | SaveIcon
  | UndoIcon
  deriving (Eq, Ord)

data IconData
  = IconData
    { nameOf :: IconName
    , stateOf :: ButtonState
    }


--------------------------------
-- View Window
--------------------------------

data ViewWindowData
  = ViewWindowData
    { fullSpanOf :: !SampleCount
    , viewWindowStateOf :: !ViewWindowState
    , windowSpanOf :: !SampleCount
    , windowStartOf :: !SampleIndex
    }

data ViewWindowState
  = ViewWindowActive
  | ViewWindowHover
  | ViewWindowNeutral


--------------------------------
-- Waveform
--------------------------------

data WaveformData
  = WaveformData
    { interactivityOf :: !Interactivity
    , segmentsOf :: !Segments
    , viewSpanOf :: !SampleCount
    , viewStartOf :: !SampleIndex
    }

data Interactivity
  = NoInteractivity
  | Hovering !SampleOffset
  | Activating !SampleOffset
  | RemovingBetween !SampleOffset !SampleOffset
  | Truncating !SampleOffset
