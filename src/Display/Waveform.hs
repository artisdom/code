{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiWayIf #-}

module Display.Waveform
 ( WaveformRepresentation
 , makeWaveformRepresentation
 , ClipRepresentation(..)
 , clipRepresentationFor
 ) where

import Control.Monad
import Data.List as L
import Data.Map.Strict as MS
import Data.Maybe
import qualified Data.Vector.Unboxed as VU

import Audio


--------------------------------
-- clips data
--------------------------------

type Samples = VU.Vector Float

data ClipData
  = ClipData
    { clipOf    :: !Segment
    , maxTreeOf :: !ExtremeTree
    , minTreeOf :: !ExtremeTree
    , samplesOf :: !Samples
    }
  deriving (Eq)

type WaveformRepresentation = Map SampleIndex ClipData


makeWaveformRepresentation
  :: (AudioData a, Monad m)
  => a -> Segments -> Float -> (Float -> m ()) -> m WaveformRepresentation
makeWaveformRepresentation audio segments threshold report = do
  report 0.0
  makeWaveformRepresentation' audio segments threshold report 1 MS.empty

makeWaveformRepresentation' audio segments threshold report index accum =
  if index < numSegments segments
  then do
    let segment = segmentAt index segments
    -- bang pattern(s) to force the computation(s) to run in the thread
    let !clipData = makeClipData audio segment threshold
    let !accum' = MS.insert (startOf segment) clipData accum
    report (fromIntegral (startOf segment + lengthOf segment) /
            fromIntegral (numberOfSamplesIn audio))
    makeWaveformRepresentation' audio segments threshold report (index+2) accum'
  else return accum

makeClipData :: (AudioData a) => a -> Segment -> Float -> ClipData
makeClipData audio clip threshold = ClipData clip maxTree minTree samples
  where
    samples = makeDisplaySamples audio clip threshold
    (maxTree, minTree) = makeExtremes samples chunkSize

makeDisplaySamples :: AudioData a => a -> Segment -> Float -> VU.Vector Float
makeDisplaySamples audio clip threshold =
  VU.generate (lengthOf clip)
    (\i -> scaleForDisplay (dBFSAt (startOf clip + i) audio) threshold)


--------------------------------
-- extremes
--------------------------------

data ExtremeTree
  = Node
    { extremeOf :: !Float
    , indexOf :: !Int
    , leftOf :: !ExtremeTree
    , rightOf :: !ExtremeTree
    }
  | Null
  deriving (Eq)

type TreeCon = ExtremeTree -> ExtremeTree
type ConPair = (Float, TreeCon)


-- WARNING: this stuff gets tricky since we're dealing with lengths and indices

chunkSize = 32

-- get the indices, inclusively, of the extreme range to use for the clip
getExtremeIndices clip fullClip chunkSize =
  let beginningOffset = startOf clip - startOf fullClip
      startingIndex = beginningOffset `div` chunkSize
      endingIndex = (beginningOffset + lengthOf clip - 1) `div` chunkSize
  in (startingIndex, endingIndex)

makeExtremes vec chunkSize =
  ( makeExtremeTree (>) $ findExtremes vec VU.maximum chunkSize
  , makeExtremeTree (<) $ findExtremes vec VU.minimum chunkSize
  )

findExtremes :: VU.Vector Float -> (VU.Vector Float -> Float) -> Int -> VU.Vector Float
findExtremes vec minMax chunkSize =
  VU.generate (size+extra)
    (\i -> minMax (VU.slice (i*chunkSize) (if i < size then chunkSize else rem) vec))
  where
    rem = VU.length vec `mod` chunkSize
    size = VU.length vec `div` chunkSize
    extra = if rem == 0 then 0 else 1

makeExtremeTree :: (Float -> Float -> Bool) -> VU.Vector Float -> ExtremeTree
makeExtremeTree ord =
  L.foldl' (flip snd) Null .
  VU.ifoldl (\hasLeft i v -> addToCon (i, v) ord hasLeft []) []

addToCon :: (Int, Float) -> (Float -> Float -> Bool) -> [ConPair] -> [TreeCon] -> [ConPair]
addToCon pair@(index, extreme) ord hasLeft chainingRight =
  if L.null hasLeft || ord topHas extreme
  then newTop:hasLeft else addToCon pair ord hasLeft' chainingRight'
  where
    topHas = fst (head hasLeft)
    left = L.foldr ($) Null chainingRight
    newTop = (extreme, Node extreme index left)
    hasLeft' = tail hasLeft
    chainingRight' = snd (head hasLeft) : chainingRight

pickInRange :: Int -> Int -> ExtremeTree -> Float
pickInRange _ _ Null = 0.0
pickInRange min max (Node extreme index left right) =
  if | max < index -> pickInRange min max left
     | min > index -> pickInRange min max right
     | otherwise   -> extreme


--------------------------------
-- representation
--------------------------------

data ClipRepresentation
  = Extremes
    { getMax :: !(Int -> Float)
    , getMin :: !(Int -> Float)
    }
  | Samples Samples


clipRepresentationFor :: Segment -> Int -> WaveformRepresentation -> ClipRepresentation
clipRepresentationFor clip width clipsData =
  let (_, clipData) = fromJust (MS.lookupLE (startOf clip) clipsData)
      fullClip = clipOf clipData
      (startingIndex, endingIndex) = getExtremeIndices clip fullClip chunkSize
      indexCount = endingIndex+1-startingIndex
  in if width < indexCount `div` 2
  then
    let dx = fromIntegral indexCount / fromIntegral width
        -- this could have better accuracy, but it works for now
        sampleAtX tree x =
          let xf = fromIntegral x
              lowerIndex = startingIndex + round (dx * xf)
              upperIndex = startingIndex + round (dx * (xf+1))
          in pickInRange lowerIndex upperIndex tree
        getMax = sampleAtX (maxTreeOf clipData)
        getMin = sampleAtX (minTreeOf clipData)
    in Extremes getMax getMin
  else
    let start = startOf clip - startOf fullClip
    in Samples $ VU.slice start (lengthOf clip) (samplesOf clipData)
