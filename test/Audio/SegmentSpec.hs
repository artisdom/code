module Audio.SegmentSpec where

import Test.HUnit
import Test.QuickCheck

import Audio.Segment


startingCases = [0, 1, 1000]
lengthCases = [0, 1, 1000]
clips = [Clip s l | s <- startingCases, l <- lengthCases]
gaps = [Gap l | l <- lengthCases]

--
-- unit test isClip with clip and gap
-- unit test isGap with clip and gap
--
-- unit test offsetOfFirstSampleIn with Clip and Gap, 0 len and non-zero len
-- unit test offsetOfLastSampleIn with Clip and Gap, 0 len and non-zero len



instance Arbitrary Segment where
  arbitrary = do
    b <- arbitrary
    if b
    then Clip <$> arbitrary `suchThat` (>= 0) <*> arbitrary `suchThat` (>= 0)
    else Gap <$> arbitrary `suchThat` (>= 0)


prop_BeginningTrimmed :: Segment -> Int -> Bool
prop_BeginningTrimmed segment amount =
  let trimmed = trimBeginning amount segment
  in lengthOf segment - amount == lengthOf trimmed
  && (not (isClip segment) || startOf segment + amount == startOf trimmed)

prop_EndTrimmed :: Segment -> Int -> Bool
prop_EndTrimmed segment amount =
  let trimmed = trimEnd amount segment
  in lengthOf segment - amount == lengthOf trimmed
  && (not (isClip segment) || startOf segment == startOf trimmed)

prop_GapExpands :: Segment -> Int -> Bool
prop_GapExpands c@Clip{} amount =
  c == expandGap amount c
prop_GapExpands g@Gap{} amount =
  lengthOf g + amount == lengthOf (expandGap amount g)

testSegmentProperties = do
  quickCheck prop_BeginningTrimmed
  quickCheck prop_EndTrimmed
  quickCheck prop_GapExpands
