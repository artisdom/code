module AudioSpec where

import Audio.DecibelSpec
import Audio.SegmentSpec


testAudioProperties = do
  testDecibelProperties
  testSegmentProperties
