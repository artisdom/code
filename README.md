# osabe

**osabe** is the **O**pen-**S**ource **A**udio-**B**ook **E**ditor, a specialized WAV editor for cutting voice clips out of long-form narration audio such as audiobooks or podcasts.

This is the official code repository for this project. For end-user downloads and documentation, see [osabe.app](https://osabe.app/).

Currently I'm not accepting code contributions, but that may change in future.

## About

**osabe** is written in [Haskell](https://www.haskell.org/) and relies on the [SDL2 game development library](https://www.libsdl.org/download-2.0.php). It was created out of a frustration with the feature set of existing free/cheap wave editors in an attempt to ease the burden of editing audio for this fledgling voice artist; I also wanted to try to do a more-complex project in Haskell.

## Dev Space Configuration

This project uses [stack](https://docs.haskellstack.org/en/stable/README/) to manage the Haskell project, and **git** for version control. You'll also need to install the SDL2 library for the Haskell bindings to work -- instructions are provided for different OSs in the following sub-sections.

### Installing SDL2 on Windows

I found this Reddit post on [installing SDL2 using stack](https://www.reddit.com/r/haskellgamedev/comments/4jpthu/windows_sdl2_is_now_almost_painless_via_stack/) very helpful.

You'll need to run (on x64 machines):

    stack exec -- pacman -Syu
    stack exec -- pacman -S mingw-w64-x86_64-pkg-config mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image mingw-w64-x86_64-SDL2_ttf
    stack install sdl2 sdl2-ttf

The `sdl2-image` stack package is also required for **osabe**, but at the time of development this wasn't installing correctly on my local via stack so I had to reference the Git repo in `extra-deps` instead [as recommended by tjlaxs](https://github.com/haskell-game/sdl2-image/issues/9).

After you have the above set up, the project should build correctly but it won't run because you need the DLLs for these libraries as well.

Download the Windows runtime binaries for your processer (likely x64) from:

 - [SDL2](https://www.libsdl.org/download-2.0.php)
 - [SDL_image 2.0](https://www.libsdl.org/projects/SDL_image/)
 - [SDL_ttf 2.0](https://www.libsdl.org/projects/SDL_ttf/)

Then unzip them and copy the DLLs to the root directory of the project. After this the application will run.

### Installing SDL2 on OSX

SDL2 is easily installed on Mac OSX using [Homebrew](https://brew.sh/) (as documented in the [Haskell game dev SDL2 documentation](https://github.com/haskell-game/sdl2))

    brew install pkg-config sdl2 sdl2_image sdl2_ttf

### Installing SDL2 on Linux

The naming of the SDL2 packages vary by distro, but you will need the SDL2 library along with SDl2-image and SDL2-ttf, and possibly pkg-config as well. Below are a few examples:

On Arch:

    sudo pacman -S sdl2_image sdl2_ttf

On Ubuntu:

    sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev

## Development

Development of this project is done using a _code-then-compile-in-the-REPL_ approach, but there are a few caveats w.r.t. running the `main` function in the REPL:

 - it is _much_ slower when running through the REPL than when compiled
 - it cannot be run on OSX from the REPL because of an issue with the bindings; apparently there's a solution but I haven't yet bothered to investigate -- see the [Haskell SDL wiki](https://wiki.haskell.org/SDL) for more.

As a result of the above, I find it more convenient to run the program using `stack run`; I just check that it compiles using `stack repl` until I'm ready to run it.

### Playback on Windows

The application defaults to using the `directsound` audio driver when running on Windows.

If you want to change which driver is used for playback, you can change the `audio-driver` configuration setting or run the program using the `SDL_AUDIODRIVER` environment variable, like so:

    SDL_AUDIODRIVER=directsound stack run
