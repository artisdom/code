{-# LANGUAGE MultiWayIf #-}

module Main where

import Control.Monad.Except
import Control.Monad.Reader

import App
import Args
import Config
import GlobalContext


main :: IO ()
main = do
  res <- runExceptT readArgs
  case res of
    Left es -> mapM_ print es
    Right args ->
      if | helpFlagOf args -> printUsage
         | versionFlagOf args -> putStrLn "beta"
         | otherwise -> do
            -- TODO handle IO errors
            configFilePaths <- establishConfigFiles (configFileOf args) Nothing
            -- TODO setup logging and add logging to the rest
            let settingsFilePath = settingsFilePathOf configFilePaths
            case argsOf args of
              ["config", "get", name] -> getSetting name settingsFilePath
              ["config", "list"] -> listSettings
              ["config", "set", name] -> setSetting name "" settingsFilePath
              ["config", "set", name, value] -> setSetting name value settingsFilePath
              ["help"] -> printUsage
              fs -> do
                (config, messages) <- readConfigFiles configFilePaths
                let filePath = if null fs then Nothing else Just (head fs)
                let context = defaultGlobalContext { configOf = config }
                runApp context filePath messages
