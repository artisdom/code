module GlobalContext
 ( GlobalContext(..)
 , defaultGlobalContext
 ) where

import Config


data GlobalContext
  = GlobalContext
    { programNameOf :: String
    , configOf :: Config
    }

instance HasProgramName GlobalContext where
  getProgramName = programNameOf

instance HasSettings GlobalContext where
  getSettings = getSettings . configOf

instance HasStyle GlobalContext where
  getStyle = getStyle . configOf
  updateStyle s c = c { configOf = updateStyle s (configOf c) }


defaultGlobalContext
  = GlobalContext
    { programNameOf = "osabe"
    , configOf = defaultConfig
    }
