{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module App
 ( layers
 , runApp
 ) where

import Prelude hiding (read)
import Control.Concurrent
import qualified Control.Concurrent.Async as A
import Control.Concurrent.MVar
import Control.Monad.Fix
import Control.Monad.Reader
import Control.Monad.Trans.Unlift
import Data.IORef
import Pipes
import Pipes.Core
import Pipes.Lift
import SDL.Audio

import Audio
import Config
import Control.Concurrent.Container
import Control.Concurrent.Operation
import GlobalContext
import Model
import Presenter
import View


type App = ReaderT GlobalContext IO

instance AudioLoader App where
  readAudio path report = do
    UnliftBase runInBase <- askUnliftBase
    liftIO $ readAudioFile path (runInBase . report)
  saveAudio audioFile segments report = do
    UnliftBase runInBase <- askUnliftBase
    liftIO $ saveAudioFile audioFile segments (runInBase . report)

instance AudioPlayer App MVar State AudioDevice where
  openPlaybackDevice = openDevice
  closePlaybackDevice = closeDevice
  startPlaybackDevice = startDevice
  stopPlaybackDevice = stopDevice
  togglePlaybackDevice = toggleDevice
  deviceIsPlaying = isPlaying

instance Container App MVar where
  create = liftIO . create
  read = liftIO . read
  update a f = liftIO (update a f)

data IORunState r
  = IORunState
    { asyncOf :: A.Async r
    , progressOf :: IORef Float
    }

instance Operation App IORunState where
  start op = do
    ioRef <- liftIO (newIORef 0)
    let report = liftIO . writeIORef ioRef
    UnliftBase runInBase <- askUnliftBase
    -- libs we're using appear to need `forkOS` to be used, which `asyncBound` uses
    async <- liftIO (A.asyncBound (runInBase (op report)))
    return (IORunState async ioRef)
  cancel = liftIO . A.cancel . asyncOf
  poll (IORunState a r) = do
    status <- liftIO (A.poll a)
    case status of
      Nothing -> do
        p <- liftIO (readIORef r)
        return (Progressing p)
      Just (Left e) ->
        return (Errored e)
      Just (Right v) ->
        return (Completed v)


layers
  :: ( AudioLoader m
     , AudioPlayer m a State d
     , Container m a
     , HasProgramName r
     , HasSettings r
     , HasStyle r
     , MonadFix m
     , MonadIO m
     , MonadReader r m
     , Operation m o
     )
  => ViewArgs -> Effect m ()
layers = model >+> presenter >+> view

runApp :: GlobalContext -> Maybe FilePath -> [String] -> IO ()
runApp globalContext filePath messages =
  let args = ViewArgs filePath messages
  in runEffect (runReaderP globalContext (layers args))
